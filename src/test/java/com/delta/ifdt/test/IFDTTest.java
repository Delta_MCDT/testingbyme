package com.delta.ifdt.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.DeltaIFDTApplication;
import com.delta.ifdt.PrepopulatingDB;
import com.delta.ifdt.ToCheckgroundURL;
import com.delta.ifdt.service.UserLoginService;

@RunWith(MockitoJUnitRunner.class)
public class IFDTTest {
	
	@InjectMocks
	@Spy
	PrepopulatingDB prepopulatingDB;
	
	@InjectMocks
	@Spy
	ToCheckgroundURL toCheckgroundURL;
	
	@InjectMocks
	@Spy
	DeltaIFDTApplication deltaIFDTApplication;
	
	@Mock
	UserLoginService userLoginService;
	
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void prepopulatingDBTest(){
		prepopulatingDB.dbData();
	}


}
