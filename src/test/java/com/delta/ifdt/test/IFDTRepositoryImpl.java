package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.repositoryImpl.DashBoardDetailsRepositoryImpl;
import com.delta.ifdt.repositoryImpl.FileTransferStatusDetailsRepositoryImpl;
import com.delta.ifdt.repositoryImpl.FlightOpenCloseRepositoryImpl;
import com.delta.ifdt.repositoryImpl.UserLoginRepositoryImpl;
import com.delta.ifdt.repositoryImpl.UserManagementRepositoryImpl;
import com.delta.ifdt.repositoryImpl.UserRoleRepositoryImpl;

@RunWith(MockitoJUnitRunner.class)
public class IFDTRepositoryImpl {
	
	@InjectMocks
	@Spy
	DashBoardDetailsRepositoryImpl dashBoardDetailsRepositoryImpl;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsRepositoryImpl fileTransferStatusDetailsRepositoryImpl;
	
	@InjectMocks
	@Spy
	FlightOpenCloseRepositoryImpl flightOpenCloseRepositoryImpl;
	
	@InjectMocks
	@Spy
	UserLoginRepositoryImpl userLoginRepositoryImpl;
	
	@InjectMocks
	@Spy
	UserManagementRepositoryImpl userManagementRepositoryImpl;
	
	@InjectMocks
	@Spy
	UserRoleRepositoryImpl userRoleRepositoryImpl;
	
	@Mock
	DashBoardDetailsRepository dashBoardDetailsRepository;
	
	@Mock
	Criteria criteria;
	
	@Mock
	EntityManager entityManager;
	@Mock
	Session session;
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);		
	}


	@Test
	public void dashBoardDetailsRepositoryImplTest() {
		

		dashBoardDetailsRepositoryImpl.getDetailsOfDashBoard("", "");
		dashBoardDetailsRepositoryImpl.getFileGraphDetails(null,1,2,null,null);
		dashBoardDetailsRepositoryImpl.getGraphDetails("SLOG",1,2,"01/04/2019","12/05/2019");
		dashBoardDetailsRepositoryImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(),5,10);
		
		
		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
		
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		dashBoardDetailsRepositoryImpl.getDetailsOfDashBoard("", "");
			
		when(criteria.uniqueResult()).thenReturn(5l);
		dashBoardDetailsRepositoryImpl.getGraphDetails("SLOG",1,2,"01/04/2019","12/05/2019");
		
		dashBoardDetailsRepositoryImpl.getFileGraphDetails(null,1,2,null,null);
		dashBoardDetailsRepositoryImpl.getFailureReasonDetails(null,1,2,null,null);
		dashBoardDetailsRepositoryImpl.getStatisticsDetails(null,null,1,2);
		
		dashBoardDetailsRepositoryImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(),5,10);
		
		when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(null);
		dashBoardDetailsRepositoryImpl.getStatisticsDetails(null,null,1,2);
		dashBoardDetailsRepositoryImpl.getFailureReasonDetails(null,1,2,null,null);
	}
	
	@Test
	public void fileTransferStatusDetailsRepositoryImplTest() {

		fileTransferStatusDetailsRepositoryImpl.saveFileTransferDetails(null);
		fileTransferStatusDetailsRepositoryImpl.saveFileTransferStatusDetails(null);
		fileTransferStatusDetailsRepositoryImpl.saveChunkFileDetails(null);
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_manual();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_ItuCheck();
		fileTransferStatusDetailsRepositoryImpl.getChunkFileTransferDetails(1);
		fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetails(1);
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsReceivedStatus();
		fileTransferStatusDetailsRepositoryImpl.getFleTraferDetRestStatusCheck(null);
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus(null);
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatusRecived(null);
		fileTransferStatusDetailsRepositoryImpl.createRestTemplate(new HttpComponentsClientHttpRequestFactory());



		
		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_manual();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_ItuCheck();
		fileTransferStatusDetailsRepositoryImpl.getChunkFileTransferDetails(1);
		fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetails(1);
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsReceivedStatus();
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus(new FlightOpenCloseEntity());
		fileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatusRecived(new FlightOpenCloseEntity());
		
		fileTransferStatusDetailsRepositoryImpl.getFleTraferDetRestStatusCheck(null);
		fileTransferStatusDetailsRepositoryImpl.removeChunkFileTransferDetails_manual(null);
		fileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetailsChunks(null);
		fileTransferStatusDetailsRepositoryImpl.updateFileTransferDetailsChunks(1);
		
		when(entityManager.merge(Mockito.any())).thenThrow(new RuntimeException());
		fileTransferStatusDetailsRepositoryImpl.saveFileTransferStatusDetails(null);
		fileTransferStatusDetailsRepositoryImpl.saveChunkFileDetails(null);

	}
	
	@Test
	public void flightOpenCloseRepositoryImplTest() {

		flightOpenCloseRepositoryImpl.getLastFlightOpenDetails();
		flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper();
		flightOpenCloseRepositoryImpl.getFlightDetails_with_received_main_entity();
		flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper_manual();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatus();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusManual();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusOffLoad();
		
		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<FlightOpenCloseEntity>> any())).thenReturn(criteria);
		
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		flightOpenCloseRepositoryImpl.getLastFlightOpenDetails();
		flightOpenCloseRepositoryImpl.saveFlightOpenCloseDetails(null);
		flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper();
		flightOpenCloseRepositoryImpl.getFlightDetails_with_received_main_entity();
		flightOpenCloseRepositoryImpl.getFlightDetailsCloseAndReadyToTransper_manual();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatus();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusManual();
		flightOpenCloseRepositoryImpl.getFltDesClsRdyToItuTraStatusOffLoad();
		
		when(entityManager.merge(Mockito.any())).thenThrow(new RuntimeException());
		flightOpenCloseRepositoryImpl.saveFlightOpenCloseDetails(null);
		
	}

	@Test
	public void userLoginRepositoryImplTest() {
		userLoginRepositoryImpl.getUserDetails(null);
		userLoginRepositoryImpl.setLastLogin(null,null);
		userLoginRepositoryImpl.getUserDetailsBasedName(null);
		userLoginRepositoryImpl.changePassword(null,null,true);
		userLoginRepositoryImpl.getUserDetailsByEmailId(null);
		userLoginRepositoryImpl.getMenuList();
		
		
		
		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<UserManagementEntity>> any())).thenReturn(criteria);
		
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		userLoginRepositoryImpl.getUserDetails(null);
		userLoginRepositoryImpl.setLastLogin(null,null);		

		userLoginRepositoryImpl.getUserDetailsBasedName(null);
		userLoginRepositoryImpl.changePassword(null,null,true);
		userLoginRepositoryImpl.getUserDetailsByEmailId(null);
		userLoginRepositoryImpl.getMenuList();
		/*userLoginRepositoryImpl.getRoleData();
		userLoginRepositoryImpl.getUserData();
		userLoginRepositoryImpl.getHeaderData();*/
		
	}
	
	@Test
	public void userManagementRepositoryImplTest() {
		
		userManagementRepositoryImpl.createUser(null);
		userManagementRepositoryImpl.deleteUser(null);
		userManagementRepositoryImpl.getUserList();

		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		when(criteria.setResultTransformer(new AliasToBeanResultTransformer(UserManagementModel.class))).thenReturn(criteria);
		
		when(criteria.uniqueResult()).thenReturn(5l);
		userManagementRepositoryImpl.duplicateUser(null);
		UserManagementModel uM = new UserManagementModel();
		userManagementRepositoryImpl.duplicateUser(uM);
		uM.setId(5);
		userManagementRepositoryImpl.duplicateUser(uM);
		
		userManagementRepositoryImpl.createUser(null);
		userManagementRepositoryImpl.getUserList();
		userManagementRepositoryImpl.deleteUser(1);
	
	}
	
	@Test
	public void userRoleRepositoryImplTest()
	{
		
		userRoleRepositoryImpl.getUserRoleList();
		
		when(entityManager.unwrap(Mockito.<Class<Session>> any())).thenReturn(session);
		when(session.createCriteria(Mockito.<Class<FileTransferStatusDetailsEntity>> any())).thenReturn(criteria);
		
		when(criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)).thenReturn(criteria);
		userRoleRepositoryImpl.getUserRoleList();
	}
	
	
}
