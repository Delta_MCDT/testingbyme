package com.delta.ifdt.test;

import org.apache.commons.configuration.ConfigurationException;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.test.web.servlet.MockMvc;

import com.delta.ifdt.controllers.ConfigurationManagementController;
import com.delta.ifdt.controllers.DashBoardDetailsController;
import com.delta.ifdt.controllers.FileTransferStatusController;
import com.delta.ifdt.controllers.UserLoginController;
import com.delta.ifdt.controllers.UserManagementController;
import com.delta.ifdt.controllers.UserRoleController;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.dto.UserManagementDto;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.service.FileTransferStatusDetailsService;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.service.UserManagementService;
import com.delta.ifdt.service.UserRoleService;
import com.delta.ifdt.util.CommonUtil;

@RunWith(MockitoJUnitRunner.class)
public class IFDTControllerTest {
	
	@InjectMocks
	@Spy
	ConfigurationManagementController configurationManagementController;

	@InjectMocks
	@Spy
	DashBoardDetailsController dashBoardDetailsController;
	
	@InjectMocks
	@Spy
	FileTransferStatusController fileTransferStatusController;
	
	@InjectMocks
	@Spy
	UserLoginController userLoginController;
	
	@InjectMocks
	@Spy
	UserManagementController userManagementController;
	
	@InjectMocks
	@Spy
	UserRoleController userRoleController;
	
	@Mock
	DashBoardDetailsService dashBoardDetailsService;
	
	@Mock
	CommonUtil commonUtil;
	
	@Mock
	FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
	
	@Mock
	FileTransferStatusDetailsService fileTransferStatusDetailsService;
	
	@Mock
	Environment env;
	
	@Mock
	UserLoginService userLoginService;
	
	@Mock
	UserManagementService userManagementService;

	@Mock
	UserRoleService userRoleService;

	@Mock
	UserManagementDto userManagementDto;
	
	MockMvc mockMvc;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		//mockMvc = MockMvcBuilders.standaloneSetup(configurationManagementController).build();
		
	}
	
	@Test
	public void configurationManagementTest() {
		configurationManagementController.getConfigDetails( new JSONObject());
		JSONObject resultMap = new JSONObject();
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		configurationManagementController.getConfigDetails(resultMap);
		configurationManagementController.editConfigDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("type", "BiteFile_Src");
		resultMap.put("PropertiesPathDetails", "/var/deltaxray/mts/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "AxinomFile_Src");
		resultMap.put("PropertiesPathDetails", "/opt/mediaserver/data/reports/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "SystemLogFile_Src");
		resultMap.put("PropertiesPathDetails", "/var/log/deltaxray/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "BiteFile_Dest");
		resultMap.put("PropertiesPathDetails", "/deltapathsDestination/bite_logs/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "AxinomFile_Dest");
		resultMap.put("PropertiesPathDetails", "/deltapathsDestination/axinom_logs/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "SystemLog_Dest");
		resultMap.put("PropertiesPathDetails", "/deltapathsDestination/system_logs/");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "GroundCount");
		resultMap.put("PropertiesPathDetails", "3");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "TarFileSizelimit");
		resultMap.put("PropertiesPathDetails", "185");
		configurationManagementController.editConfigDetails(resultMap);
		resultMap.put("type", "ChunkSize");
		resultMap.put("PropertiesPathDetails", "185");
		configurationManagementController.editConfigDetails(resultMap);
	}
	
	
	@Test
	public void DashboardTest() {
		dashBoardDetailsController.manualOffload( new JSONObject());
		JSONObject resultMap = new JSONObject();
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("searchStatus", "load");
		dashBoardDetailsController.manualOffload(resultMap);
		resultMap.put("searchStatus", "search");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		dashBoardDetailsController.manualOffload(resultMap);
		dashBoardDetailsController.getGraphDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("type", "");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		JSONObject resultMap1 = new JSONObject();
		resultMap1.put("page", 1);
		resultMap1.put("count", 10);
		resultMap.put("pagination", resultMap1);	
		dashBoardDetailsController.getGraphDetails(resultMap);
		dashBoardDetailsController.getFileGraphDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("type", "");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		resultMap.put("pagination", resultMap1);
		dashBoardDetailsController.getFileGraphDetails(resultMap);
		dashBoardDetailsController.getFileGraphDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("type", "");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		resultMap.put("pagination", resultMap1);
		dashBoardDetailsController.getFileGraphDetails(resultMap);
		dashBoardDetailsController.getFailureReasonDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("type", "");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		resultMap.put("pagination", resultMap1);
		dashBoardDetailsController.getFailureReasonDetails(resultMap);
		dashBoardDetailsController.getStatisticsDetails( new JSONObject());
		resultMap.put("sessionId", "12345");
		resultMap.put("serviceToken", "78945");
		resultMap.put("searchStatus", "load");
		resultMap.put("fromDate","");
		resultMap.put("toDate", "");
		resultMap.put("pagination", resultMap1);
		dashBoardDetailsController.getStatisticsDetails(resultMap);
		resultMap.put("searchStatus", "search");
		dashBoardDetailsController.getStatisticsDetails(resultMap);
	} 
	

	@Test
	public void FileTransferTest() {
		fileTransferStatusController.manualOffload( new JSONObject());
		JSONObject resultMap = new JSONObject();
		resultMap.put("pendrivePath", "/home/user");
		fileTransferStatusController.manualOffload(resultMap);
		fileTransferStatusController.fileTransferDetails( "");

	} 
	
	@Test
	public void userManagementTest() {
		userLoginController.loginUser( new JSONObject(), null, null);
		JSONObject resultMap = new JSONObject();
		resultMap.put("username", "test");
		resultMap.put("serviceToken", "12345");
		resultMap.put("password", "12345");
		userLoginController.loginUser(resultMap,null,null);
		userLoginController.logoutUser( new JSONObject());
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		userLoginController.logoutUser(resultMap);
		userLoginController.changePassword( new JSONObject());
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		resultMap.put("userName", "delta");
		resultMap.put("currentPassword", "123454");
		resultMap.put("newPassword", "123454");
		userLoginController.changePassword(resultMap);
		userLoginController.forgotPassword( new JSONObject());
		resultMap.put("emailId", "12345");
		userLoginController.forgotPassword(resultMap);
		userManagementController.createNewUser( new JSONObject());
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		userManagementController.createNewUser(resultMap);
		userManagementController.userList( new JSONObject());
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		userManagementController.userList(resultMap);
		userManagementController.deleteUser( new JSONObject());
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		resultMap.put("userId", "1");
		userManagementController.deleteUser(resultMap);
	} 
	

	
	@Test
	public void userManagementUserRoleListTest() {
		userRoleController.getUserRoleList( new JSONObject());
		JSONObject resultMap = new JSONObject();
		resultMap.put("serviceToken", "12345");
		resultMap.put("sessionId", "123454");
		userRoleController.getUserRoleList(resultMap);
	}
}
