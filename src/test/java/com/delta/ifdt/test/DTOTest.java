package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.dto.UserManagementDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.UserManagementModel;

@RunWith(MockitoJUnitRunner.class)
public class DTOTest {
	
	@InjectMocks
	@Spy
	UserManagementDto userManagementDto;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
	
	@Mock
	UserManagementModel userManagementModel;
	
	@Mock
	FileTransferStatusDetailsModel fileTransferStatusDetailsModel;
	
	@Mock
	FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void fileTransferStatusDetailsDtoTest(){
		when(fileTransferStatusDetailsEntity.getId()).thenReturn(1);
		when(fileTransferStatusDetailsModel.getId()).thenReturn(0);
		when(fileTransferStatusDetailsModel.getOriginalFilename()).thenReturn(null);
		
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);

		when(fileTransferStatusDetailsModel.getId()).thenReturn(1);
		when(fileTransferStatusDetailsModel.getOriginalFilename()).thenReturn("");
		
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		
		when(fileTransferStatusDetailsModel.getId()).thenReturn(null);
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(fileTransferStatusDetailsModel);
		fileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity(null);
		
		fileTransferStatusDetailsDto.getStatisticsDetailsEntity(fileTransferStatusDetailsEntity);
		fileTransferStatusDetailsDto.getStatisticsDetailsEntity(null);
	}
	
	@Test
	public void userManagementDtoTest() {
		when(userManagementModel.getId()).thenReturn(0);
		userManagementDto.getUserDetails(userManagementModel);
		
		when(userManagementModel.getId()).thenReturn(1);
		userManagementDto.getUserDetails(userManagementModel);
		
		when(userManagementModel.getId()).thenReturn(null);
		userManagementDto.getUserDetails(userManagementModel);
		
		userManagementDto.getUserDetails(null);


	}

}
