package com.delta.ifdt.test;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.entities.MenuEntity;
import com.delta.ifdt.entities.SubMenuEntity;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.models.User;

@RunWith(MockitoJUnitRunner.class)
public class EntityTest {
	
	@InjectMocks
	@Spy
	FlightOpenCloseEntity flightOpenCloseEntity;
	
	@InjectMocks
	@Spy
	UserRoleEntity userRoleEntity;
	
	@InjectMocks
	@Spy
	SubMenuEntity subMenuEntity;
	
	@InjectMocks
	@Spy
	MenuEntity menuEntity;
	
	@InjectMocks
	@Spy
	ChunksDetailsEntity chunksDetailsEntity;
	
	@InjectMocks
	@Spy
	UserSessionPool userSessionPool;
	
	@InjectMocks
	@Spy
	UserManagementEntity userManagementEntity;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void fileTransferStatusDetailsEntityTest(){
		
		fileTransferStatusDetailsEntity.setChecksum("");
		fileTransferStatusDetailsEntity.getChecksum();
		fileTransferStatusDetailsEntity.setChunkSumCount(5);
		fileTransferStatusDetailsEntity.getChunkSumCount();
		fileTransferStatusDetailsEntity.setFailureReasonForOffLoad("");
		fileTransferStatusDetailsEntity.getFailureReasonForOffLoad();
		fileTransferStatusDetailsEntity.setFileType("");
		fileTransferStatusDetailsEntity.getFileType();
		fileTransferStatusDetailsEntity.setId(5);
		fileTransferStatusDetailsEntity.getId();
		fileTransferStatusDetailsEntity.setModeOfTransfer("");
		fileTransferStatusDetailsEntity.getModeOfTransfer();
		fileTransferStatusDetailsEntity.setOpenCloseDetilsId(5);
		fileTransferStatusDetailsEntity.getOpenCloseDetilsId();
		fileTransferStatusDetailsEntity.setOriginalFilename("");
		fileTransferStatusDetailsEntity.getOriginalFilename();
		fileTransferStatusDetailsEntity.setStatus("");
		fileTransferStatusDetailsEntity.getStatus();
		fileTransferStatusDetailsEntity.setTarFileDate(new Date());
		fileTransferStatusDetailsEntity.getTarFileDate();
		fileTransferStatusDetailsEntity.setTarFilename("");
		fileTransferStatusDetailsEntity.getTarFilename();
		fileTransferStatusDetailsEntity.setTarFilePath("");
		fileTransferStatusDetailsEntity.getTarFilePath();
		
		
	}
	
	@Test
	public void userManagementEntityTest(){
		
		
		userManagementEntity.setCreationDate("10122005");
		userManagementEntity.getCreationDate();
		userManagementEntity.setEmailId("abc@xyz.com");
		userManagementEntity.getEmailId();
		userManagementEntity.setFullName("");
		userManagementEntity.getFullName();
		userManagementEntity.getUserName();
		userManagementEntity.setUserName("");
		userManagementEntity.getPassword();
		userManagementEntity.setPassword("");
		userManagementEntity.getStatus();
		userManagementEntity.setStatus("");
		userManagementEntity.setRemarks("");
		userManagementEntity.setUserRoleEntity(userRoleEntity);
		userManagementEntity.getUserRoleEntity();
		userManagementEntity.getLastLoginDate();
		userManagementEntity.setLastLoginDate(null);
		userManagementEntity.getRemarks();
		userManagementEntity.setId(5);
		userManagementEntity.getId();
		
	}
	
	@Test
	public void userSessionPoolTest() {
		
		UserSessionPool.getInstance();
		userSessionPool.addUser(user());
		userSessionPool.getSessionUser("");
		userSessionPool.removeUser(user());
		userSessionPool.removeUser("");
		userSessionPool.isUserInSession(user());
		
	}
	
	private User user() {
		User user = new User();
		user.setServiceToken("ASDFGHJ");
		user.setUserName("testUser");
		user.setLastLoginTime("10052015");
		
		return user;
		
	}
	
	@Test
	public void flightOpenCloseEntityTest(){
		
		flightOpenCloseEntity.getAircraftType();
		flightOpenCloseEntity.setAircraftType("");
		flightOpenCloseEntity.getAirLineName();
		flightOpenCloseEntity.setAirLineName("");
		flightOpenCloseEntity.getArrivalAirport();
		flightOpenCloseEntity.setArrivalAirport("");
		flightOpenCloseEntity.getArrivalTime();
		flightOpenCloseEntity.setArrivalTime(new Date());
		flightOpenCloseEntity.getCloseOpenStatus();
		flightOpenCloseEntity.setCloseOpenStatus("");
		flightOpenCloseEntity.getDepartureAirport();
		flightOpenCloseEntity.setDepartureAirport("");
		flightOpenCloseEntity.getDepartureTime();
		flightOpenCloseEntity.setDepartureTime(new Date());
		flightOpenCloseEntity.getFlightCloseTime();
		flightOpenCloseEntity.setFlightCloseTime(new Date());
		flightOpenCloseEntity.getFlightNumber();
		flightOpenCloseEntity.setFlightNumber("");
		flightOpenCloseEntity.getFlightOpenTime();
		flightOpenCloseEntity.setFlightOpenTime(new Date());
		flightOpenCloseEntity.getId();
		flightOpenCloseEntity.setId(0);
		flightOpenCloseEntity.getItuFileStatus();
		flightOpenCloseEntity.setItuFileStatus("");
		flightOpenCloseEntity.getItuFolderName();
		flightOpenCloseEntity.setItuFolderName("");
		flightOpenCloseEntity.getNextFlightCloseTime();
		flightOpenCloseEntity.setNextFlightCloseTime(new Date());
		flightOpenCloseEntity.getOffloadGenTime();
		flightOpenCloseEntity.setOffloadGenTime(new Date());
		flightOpenCloseEntity.getTailNumber();
		flightOpenCloseEntity.setTailNumber("");
		flightOpenCloseEntity.getTransferStatus();
		flightOpenCloseEntity.setTransferStatus("");
		flightOpenCloseEntity.getWayOfTransfer();
		flightOpenCloseEntity.setWayOfTransfer("");
		
		
	}
	
	@Test
	public void userRoleEntityTest(){
		
		userRoleEntity.getId();
		userRoleEntity.setId(0);
		userRoleEntity.getRole();
		userRoleEntity.setRole("");
		
	}
	
	@Test
	public void subMenuEntityTest() {
		
		subMenuEntity.getId();
		subMenuEntity.setId(1);
		subMenuEntity.getRoute();
		subMenuEntity.setRoute("");
		subMenuEntity.getMenuEntity();
		subMenuEntity.setMenuEntity(new MenuEntity());
		subMenuEntity.getDisplayText();
		subMenuEntity.setDisplayText("");
		
	}
	
	@Test
	public void menuEntityTest() {
		menuEntity.getDisplayText();
		menuEntity.setDisplayText("");
		menuEntity.getId();
		menuEntity.setId(0);
		menuEntity.getRoute();
		menuEntity.setRoute("");
		menuEntity.getSubMenuItems();
		menuEntity.setSubMenuItems(new ArrayList());
		
	}
	
	@Test
	public void chunksDetailsEntityTest(){
		
		chunksDetailsEntity.getChunk_Checksum();
		chunksDetailsEntity.setChunk_Checksum("");
		chunksDetailsEntity.getChunkCount();
		chunksDetailsEntity.setChunkCount(5);
		chunksDetailsEntity.getChunkName();
		chunksDetailsEntity.setChunkName("");
		chunksDetailsEntity.getChunkTarFilePath();
		chunksDetailsEntity.setChunkTarFilePath("");
		chunksDetailsEntity.getChunkStatus();
		chunksDetailsEntity.setChunkStatus("");
		chunksDetailsEntity.getFileTransferStausDetailsEntityId();
		chunksDetailsEntity.setFileTransferStausDetailsEntityId(5);
		chunksDetailsEntity.getId();
		chunksDetailsEntity.setId(5);
	}
	

}
