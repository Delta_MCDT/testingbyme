package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import com.delta.ifdt.config.DBConfig;
import com.delta.ifdt.config.SQLiteDialect;
import com.delta.ifdt.config.SQLiteIdentityColumnSupport;

@RunWith(MockitoJUnitRunner.class)
public class IfdtConfigTest {
	
	@InjectMocks
	@Spy
	SQLiteDialect sQLiteDialect;
	
	@InjectMocks
	@Spy
	DBConfig dBConfig;
	
	@InjectMocks
	@Spy
	SQLiteIdentityColumnSupport sQLiteIdentityColumnSupport;
	
	@Mock
	Environment env;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void sQLiteDialectTest1() {
		sQLiteDialect.getIdentityColumnSupport();
		sQLiteDialect.supportsLimit();
		sQLiteDialect.getLimitString("",false);
		sQLiteDialect.supportsTemporaryTables();
		sQLiteDialect.getCreateTemporaryTableString();
		sQLiteDialect.dropTemporaryTableAfterUse();
		sQLiteDialect.supportsCurrentTimestampSelection();
		sQLiteDialect.isCurrentTimestampSelectStringCallable();
		sQLiteDialect.getCurrentTimestampSelectString();
		sQLiteDialect.supportsUnionAll();
		sQLiteDialect.hasAlterTable();
		sQLiteDialect.dropConstraints();
		sQLiteDialect.getAddColumnString();
		sQLiteDialect.getForUpdateString();
		sQLiteDialect.supportsOuterJoinForUpdate();		
		sQLiteDialect.supportsIfExistsBeforeTableName();
		sQLiteDialect.supportsCascadeDelete();
		/*sQLiteDialect.getDropForeignKeyString();
		sQLiteDialect.getAddForeignKeyConstraintString("","");
		sQLiteDialect.getAddPrimaryKeyConstraintString("");*/
	
	}
	
	@Test
	public void dBConfigTest() {
		when(env.getProperty("driverClassName")).thenReturn("org.sqlite.JDBC");
		dBConfig.dataSource();
		
	}
	
	@Test
	public void sQLiteIdentityColumnSupportTest() {
		sQLiteIdentityColumnSupport.supportsIdentityColumns();
		sQLiteIdentityColumnSupport.getIdentitySelectString("","",0);
		sQLiteIdentityColumnSupport.getIdentityColumnString(99);
		sQLiteIdentityColumnSupport.hasDataTypeInIdentityColumn();
	}
	

}
