package com.delta.ifdt.test.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.serviceImpls.DashBoardDetailsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class DashBoardDetailsServiceImplTest {
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	@Spy
	DashBoardDetailsServiceImpl  dashBoardDetailsServiceImpl;
	
	
	@Mock
	DashBoardDetailsRepository objDashBoardDetailsRepository;
	 
	@Test
	public void DashBoardServiceImpltest()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getDetailsOfDashBoard(Mockito.anyString(),Mockito.anyString())).thenReturn(new ArrayList<FileTransferStatusDetailsEntity>());
		dashBoardDetailsServiceImpl.getDashBoardDetails(new Date().toString(),new Date().toString());
		
		
		Mockito.when(objDashBoardDetailsRepository.getDetailsOfDashBoard(Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getDashBoardDetails(new Date().toString(),new Date().toString());
	}
	
	
	@Test
	public void getGraphDetails()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getGraphDetails("delta",1,2,"delta","delta");
		
		
		Mockito.when(objDashBoardDetailsRepository.getGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getGraphDetails("delta",1,2,"delta","delta");
	}
	
	
	@Test
	public void getFileGraphDetails()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta",1,2,"delta","delta");
		
		
		Mockito.when(objDashBoardDetailsRepository.getFileGraphDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getFileGraphDetails("delta",1,2,"delta","delta");
	}
	
	@Test
	public void getFailureReasonDetails()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getStatisticsDetails("delta","delta",2,1);
		
		
		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.anyString(),Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getStatisticsDetails("delta","delta",2,1);
	}
	
	@Test
	public void getStatisticsDetails()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getFailureReasonDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getFailureReasonDetails("delta",1,2,"delta","delta");
		
		
		Mockito.when(objDashBoardDetailsRepository.getFailureReasonDetails(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt(),Mockito.anyString(),Mockito.anyString())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getFailureReasonDetails("delta",1,2,"delta","delta");
	}
	@Test
	public void getStatisticsDetails_test()
	{
		
		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.any(FileTransferStatusDetailsModel.class),Mockito.anyInt(),Mockito.anyInt())).thenReturn(new HashMap<String, Object>());
		dashBoardDetailsServiceImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(),1,2);
		
		
		Mockito.when(objDashBoardDetailsRepository.getStatisticsDetails(Mockito.any(FileTransferStatusDetailsModel.class),Mockito.anyInt(),Mockito.anyInt())).thenThrow(new RuntimeException());
		dashBoardDetailsServiceImpl.getStatisticsDetails(new FileTransferStatusDetailsModel(),1,2);
	}

}
