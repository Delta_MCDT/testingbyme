package com.delta.ifdt.test.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.serviceImpls.FlightOpenCloseServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class FlightOpenCloseServiceImplTest {
	
	@InjectMocks
	@Spy
	FlightOpenCloseServiceImpl flightOpenCloseServiceImpl;
	
	@Mock
	FlightOpenCloseRepository objFlightOpenCloseRepository;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test	
	public void flightOpencloseTest()
	{
		
		flightOpenCloseServiceImpl.flightOpenCloseDetailsSave("FLIGHT_OPENED");
		Mockito.when(objFlightOpenCloseRepository.getLastFlightOpenDetails()).thenReturn(new FlightOpenCloseEntity());
		Mockito.when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
		flightOpenCloseServiceImpl.flightOpenCloseDetailsSave("FLIGHT_CLOSED");
		Mockito.when(objFlightOpenCloseRepository.getLastFlightOpenDetails()).thenThrow(new RuntimeException());
		flightOpenCloseServiceImpl.flightOpenCloseDetailsSave("FLIGHT_CLOSED");

		flightOpenCloseServiceImpl.flightOpenCloseDetailsSave("");
		
	}

}
