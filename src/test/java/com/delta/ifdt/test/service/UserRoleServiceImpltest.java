package com.delta.ifdt.test.service;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserRoleRepository;
import com.delta.ifdt.serviceImpls.UserRoleServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserRoleServiceImpltest {

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@InjectMocks
	@Spy
	UserRoleServiceImpl userRoleServiceImpl;
	
	@Mock
	UserRoleRepository userRoleRepository;	
	
	@Test
	public void getUserDetailsByUserName()
	{
		Mockito.when(userRoleRepository.getUserRoleList()).thenReturn(new ArrayList<>());
		userRoleServiceImpl.getUserRoleList();
		
		
		Mockito.when(userRoleRepository.getUserRoleList()).thenThrow(new RuntimeException());
		userRoleServiceImpl.getUserRoleList();
	}
	
}
