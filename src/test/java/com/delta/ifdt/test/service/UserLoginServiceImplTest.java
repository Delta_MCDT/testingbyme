package com.delta.ifdt.test.service;

import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserLoginRepository;
import com.delta.ifdt.serviceImpls.UserLoginServiceImpl;
import com.delta.ifdt.util.EmailUtil;
import com.delta.ifdt.util.PasswordCryption;

@RunWith(MockitoJUnitRunner.class)
public class UserLoginServiceImplTest {
	
	@InjectMocks
	@Spy
	UserLoginServiceImpl userLoginServiceImpl;
	
	@Mock
	UserLoginRepository userLoginRepository;
	
	@Mock
	PasswordCryption passwordCryption;
	
	@Mock
	EmailUtil emailUtil;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getUserDetailsByUserName()
	{
		Mockito.when(userLoginRepository.getUserDetails(Mockito.anyString())).thenReturn(new UserManagementModel());
		userLoginServiceImpl.getUserDetailsByUserName("ram");
		
		Mockito.when(userLoginRepository.getUserDetails(Mockito.anyString())).thenThrow(new RuntimeException());
		userLoginServiceImpl.getUserDetailsByUserName("hari");
		
	}
	
	
	@Test
	public void setLastLogin()
	{
		Mockito.when(userLoginRepository.setLastLogin(Mockito.anyString(),Mockito.any(Date.class))).thenReturn(true);
		userLoginServiceImpl.setLastLogin("hari",new Date());
		
		Mockito.when(userLoginRepository.setLastLogin(Mockito.anyString(),Mockito.any(Date.class))).thenThrow(new RuntimeException());
		userLoginServiceImpl.setLastLogin("hari",new Date());
		
	}
	
	@Test
	public void getpasswordTest()
	{
		Mockito.when(userLoginRepository.changePassword(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean())).thenReturn(true);
		userLoginServiceImpl.changePassword("siva", "sai", true);
		
		Mockito.when(userLoginRepository.changePassword(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean())).thenThrow(new RuntimeException());
		userLoginServiceImpl.changePassword("siva", "sai", true);
	}
	
	
	@Test
	public void validUserTest()
	{
		
		userLoginServiceImpl.validUser(new UserManagementModel(),"hari");
		
		
	}
	
	@Test
	public void getUserDetailsByEmailIdtest()
	{
		Mockito.when(userLoginRepository.getUserDetailsByEmailId(Mockito.anyString())).thenReturn(new UserManagementEntity());
		userLoginServiceImpl.getUserDetailsByEmailId("siva");
		
		Mockito.when(userLoginRepository.getUserDetailsByEmailId(Mockito.anyString())).thenThrow(new RuntimeException());
		userLoginServiceImpl.getUserDetailsByEmailId("siva");
	}
	
	
	@Test
	public void resetPassword()
	{
		
		userLoginServiceImpl.resetPassword("siva","laxman");
		userLoginServiceImpl.resetPassword(null,null);
		
		
	}
	
	@Test
	public void mailNewPassword() throws Exception
	{
		//when((emailUtil.sendEmail( Mockito.any(),null, null, Mockito.any(), Mockito.any().toString(), true))).thenReturn(true);
		userLoginServiceImpl.mailNewPassword("delta","delta","delta","delta@gmail.com");
	}
	
	
	@Test
	public void getMenuList()
	{
		
		Mockito.when(userLoginRepository.getMenuList()).thenReturn(new HashMap<String, Object>());
		userLoginServiceImpl.getMenuList();
		
		Mockito.when(userLoginRepository.getMenuList()).thenThrow(new RuntimeException());
		userLoginServiceImpl.getMenuList();
	}
	
	
	@Test
	public void executeDbData()
	{
		
		// Mockito.when(userLoginRepository.getMenuList()).thenReturn(new HashMap<String, Object>());
		userLoginServiceImpl.executeDbData();
	}
	

}
