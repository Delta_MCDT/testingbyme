package com.delta.ifdt.test.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserManagementRepository;
import com.delta.ifdt.serviceImpls.UserManagementServiceImpl;


@RunWith(MockitoJUnitRunner.class)

public class UsermanagementServiceImplTest {
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	@Spy
	UserManagementServiceImpl userManagementServiceImpl;
	
	
	@Mock
	UserManagementRepository userManagementRepository;
	
	@Test
	public void duplicateUser_test()
	{
		Mockito.when(userManagementRepository.duplicateUser(Mockito.any(UserManagementModel.class))).thenReturn(Mockito.anyBoolean());
		userManagementServiceImpl.duplicateUser(new UserManagementModel());
		
		Mockito.when(userManagementRepository.duplicateUser(Mockito.any(UserManagementModel.class))).thenThrow(new RuntimeException());
		userManagementServiceImpl.duplicateUser(new UserManagementModel());
		
	
	}
	
	@Test
	public void createUser_test()
	{
		UserManagementEntity userEntity = new UserManagementEntity();
		userEntity.setPassword("delta");
		userManagementServiceImpl.createUser(userEntity);
		userManagementServiceImpl.createUser(null);
	
	}
	
	@Test
	public void getUserList_test()
	{
		UserManagementModel userDetailsModel = new UserManagementModel();
		List lst = new ArrayList();
		lst.add(new UserManagementModel());
		Mockito.when(userManagementRepository.getUserList()).thenReturn(lst);
		userDetailsModel.setPassword("delta");
		userDetailsModel.setConfirmPassword("delta");
		
		userManagementServiceImpl.getUserList();
	
	}
	
	@Test
	public void deleteUser_test()
	{
		Mockito.when(userManagementRepository.deleteUser(Mockito.anyInt())).thenReturn(true);
		userManagementServiceImpl.deleteUser(1);
		
		Mockito.when(userManagementRepository.deleteUser(Mockito.anyInt())).thenThrow(new RuntimeException());
		userManagementServiceImpl.deleteUser(1);
	}
	
	
	

}
