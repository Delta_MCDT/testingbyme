package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.config.DBConfig;
import com.delta.ifdt.config.SQLiteDialect;
import com.delta.ifdt.config.SQLiteIdentityColumnSupport;
import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.ManualOffload;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.serviceImpls.FileTransferStatusDetailsServiceimpl;
import com.delta.ifdt.util.CommonUtil;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FileTransferStatusDetailsServiceimplTest {
	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsServiceimpl.class);
	@InjectMocks
	@Spy
	SQLiteDialect sQLiteDialect;
	
	@InjectMocks
	@Spy
	DBConfig dBConfig;
	
	@InjectMocks
	@Spy
	SQLiteIdentityColumnSupport sQLiteIdentityColumnSupport;
	
	@Mock
	Environment env;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsServiceimpl fileTransferStatusDetailsServiceimpl;
	@Mock
	RestTemplate restTemplate = new RestTemplate();
	@Mock
	FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;
	@Mock
	FlightOpenCloseRepository objFlightOpenCloseRepository;
	
	@Mock
	 CommonUtil commonUtil;

	 @Rule
	   public TemporaryFolder tempFolder = new TemporaryFolder();
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void receivedStatusOffloadQueryingTest()  {
		
		try {
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("https://10.9.41.240:3000/gas/v1/offload/status");
			List<String> finalOffloadedQueryList=new ArrayList<>();
			finalOffloadedQueryList.add("one.txt");
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			//when(fileTransferStatusDetailsServiceimpl.createRestTemplate(requestFactory)).thenReturn(new RestTemplate(requestFactory));
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("foo"));
			fileTransferStatusDetailsServiceimpl.receivedStatusOffloadQuerying(finalOffloadedQueryList);
			
		} catch (Exception e) {
			logger.error("Exception for receivedStatusOffloadQueryingTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
		
	}
	
	@Test
	public void restQueryForOffloadTest()  {
		try {
			
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("https://10.9.41.240:3000/gas/v1/offload/status");
			List<String> finalOffloadedQueryList=new ArrayList<>();
			finalOffloadedQueryList.add("one.txt");
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);

			ManualOffload manualOffload = new ManualOffload();
			manualOffload.setFiles(finalOffloadedQueryList.toArray(new String[0]));

		
			//when(fileTransferStatusDetailsServiceimpl.createRestTemplate(requestFactory)).thenReturn(new RestTemplate(requestFactory));
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("foo"));
			
			fileTransferStatusDetailsServiceimpl.restQueryForOffload(finalOffloadedQueryList);
			
		} catch (Exception e) {
			logger.error("Exception for restQueryForOffloadTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
		
	}
	@Test
	public void sysItuLogsUpdationInDBTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("185");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			//when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity=new FileTransferStatusDetailsEntity();
			objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
			fileTransferStatusDetailsServiceimpl.sysItuLogsUpdationInDB(objLocFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	@SuppressWarnings("deprecation")
	@Test
	public void sysItuLogsUpdationInDBTestChunks()  {
	
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(commonUtil.computeFileChecksum(Mockito.any(MessageDigest.class), Mockito.anyString())).thenReturn("gfgdshdfh");
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			//when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity=new FileTransferStatusDetailsEntity();
			objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.sysItuLogsUpdationInDB(objLocFileTransferStatusDetailsEntity);
		    Assert.assertNotNull(objFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for sysItuLogsUpdationInDBTestChunks() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void sysItuLogsUpdationInDBTest_Exception()  {
		
		try {
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("185");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("185");
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			//when(() -> CommonUtil.Chunking_System_logs(Matchers.anyString(), Matchers.a)).thenReturn(null);
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity=new FileTransferStatusDetailsEntity();
			//objLocFileTransferStatusDetailsEntity.setTarFilePath(test.toString());
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.sysItuLogsUpdationInDB(objLocFileTransferStatusDetailsEntity);
			Assert.assertNull(objFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for sysItuLogsUpdationInDBTest_Exception() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void fileTransferFromIFEServertoLocalPathTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath(objStausFlightOpenCloseEntity);
			Assert.assertNotNull(objFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void itufileTraFmIFESertoLocPthTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			;
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth(objStausFlightOpenCloseEntity);
			Assert.assertNotNull(objFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for itufileTraFmIFESertoLocPthTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getFileTransferDetailsTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath("hai");
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			
			
			List<FileTransferStatusDetailsEntity> objListFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.getFileTransferDetails();
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getFileTransferDetailsTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	

	@Test
	public void getStatusOfFileTransferDetailsTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath("hai");
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetails(objList);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getStatusOfFileTransferDetailsOldTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath("hai");
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(objList,objStausFlightOpenCloseEntity);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsOldTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	@Test
	public void getStatusOfFileTransferDetailsOldWithOutChunksTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			/*ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath("hai");
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);*/
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld(objList,objStausFlightOpenCloseEntity);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsOldWithOutChunksTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getStatusOfFileTransferDetailsNewFilesTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath("hai");
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(objList,objStausFlightOpenCloseEntity);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsNewFilesTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getStatusOfFileTransferDetailsNewFilesWithOutChunksTest()  {
		
		try {
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles(objList,objStausFlightOpenCloseEntity);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsNewFilesWithOutChunksTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getStatusOfFileTransferDetailsOld_manualTest()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld_manual(objList,objStausFlightOpenCloseEntity,testFdtdest.toString());
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsOld_manualTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	
	@Test
	public void fileTransferFromIFEServertoLocalPathBITETest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			
			boolean status=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathBITE(objStausFlightOpenCloseEntity,objFileTransferStatusDetailsEntity0);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathBITETest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void fileTransferFromIFEServertoLocalPathAXINOMTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity0.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			
			boolean status=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathAXINOM(objStausFlightOpenCloseEntity,objFileTransferStatusDetailsEntity0);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathAXINOMTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void fileTransferFromIFEServertoLocalPathSYSTEMTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			
			boolean status=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM(objStausFlightOpenCloseEntity,objFileTransferStatusDetailsEntity2);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathSYSTEMTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void fileTransferFromIFEServertoLocalPathITUTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(test.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(test.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath("hai");
			objFileTransferStatusDetailsEntity2.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			
			boolean status=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU(objStausFlightOpenCloseEntity,objFileTransferStatusDetailsEntity2);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathITUTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void fileTransferFromIFEServertoLocalPathManvalTest()  {
		
		try {
			File test=tempFolder.getRoot();
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, test.toString());
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			
			when(commonUtil.Chunking_System_logs(Mockito.anyString(), Mockito.anyLong())).thenReturn(objTotDetails);
			when(env.getProperty("BITE_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE")).thenReturn(testFdtdest.toString());
			
			when(env.getProperty("BITE_FILE_PATH_DESTINATION")).thenReturn(testFdtdest.toString());
			when(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());
			when(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());
			when(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).thenReturn(testFdtdest.toString());
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntity=fileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathManval(objStausFlightOpenCloseEntity);
			Assert.assertNotNull(objFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for fileTransferFromIFEServertoLocalPathManvalTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getStatusOfFileTransferDetailsNewFiles_manualTest()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("hai");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\"fileName\":\"BITE_DELTA0001_1559711897000.tar.gz\",\"currentChunk\":\"1\",\"message\":\"File Uploaded Successfully\",\"status\":\"2001\"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			
			
			fileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles_manual(objList,objStausFlightOpenCloseEntity,testFdtdest.toString());
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for sysItuLogsUpdationInDBTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void metadataJsonFileCreationAndTransferTest()  {
		
		try {
			//File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setArrivalTime(new Date());
			
			
			boolean status=	fileTransferStatusDetailsServiceimpl.metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,testFdtdest.toString());
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getStatusOfFileTransferDetailsNewFiles_manualTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void groundServerQueryTestOne()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("hai");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("hai");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("hai");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  }\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			
			fileTransferStatusDetailsServiceimpl.groundServerQuery(objList,objOpenList);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for groundServerQueryTestOne() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void groundServerQueryTestTwo()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  }\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			
			fileTransferStatusDetailsServiceimpl.groundServerQuery(objList,objOpenList);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for groundServerQueryTestTwo() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	
	@Test
	public void groundServerQueryTestThree()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": \"\"\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			
			fileTransferStatusDetailsServiceimpl.groundServerQuery(objList,objOpenList);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for groundServerQueryTestThree() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	
	@Test
	public void receivedStatusServerQueryingTest()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": \"\"\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			
			fileTransferStatusDetailsServiceimpl.receivedStatusServerQuerying(objList,objOpenList);
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for receivedStatusServerQueryingTest() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	
	@Test
	public void getFileTransferDetailsRestStatusTestOne()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			when(fileTransferStatusDetailsRepository.getFileTransferDetails_ItuCheck()).thenReturn(objList);
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": \"\"\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			when(objFlightOpenCloseRepository.getFlightDetails_with_received_main_entity()).thenReturn(objOpenList);
			when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper()).thenReturn(objOpenList);
			when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatus()).thenReturn(objOpenList);
			
			
			fileTransferStatusDetailsServiceimpl.flightTransferIfeToGround();
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getFileTransferDetailsRestStatusTestOne() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	@Test
	public void getFileTransferDetailsRestStatusTestTwo()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails()).thenReturn(new ArrayList<FileTransferStatusDetailsEntity>());
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			when(fileTransferStatusDetailsRepository.getFileTransferDetails_ItuCheck()).thenReturn(objList);
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": \"\"\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			when(objFlightOpenCloseRepository.getFlightDetails_with_received_main_entity()).thenReturn(objOpenList);
			when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper()).thenReturn(objOpenList);
			when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatus()).thenReturn(objOpenList);
			
			
			fileTransferStatusDetailsServiceimpl.flightTransferIfeToGround();
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getFileTransferDetailsRestStatusTestTwo() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
	
	
	@Test
	public void getmanualOffloadTransfer()  {
		
		try {
			File testFdtSource=tempFolder.newFile("testFdtSource.txt");
			File testFdtdest=tempFolder.newFolder("testFdtDest");
			List<FileTransferStatusDetailsEntity> objList=new ArrayList<>();
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity0=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity0.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity0.setFileType(Constants.BITE_LOG);
			objFileTransferStatusDetailsEntity0.setTarFilename("BITE_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity0.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity0.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity1=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity1.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity1.setFileType(Constants.AXINOM_LOG);
			objFileTransferStatusDetailsEntity1.setTarFilename("AXINOM_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity1.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity1.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity2=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity2.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity2.setFileType(Constants.SYSTEM_LOG);
			objFileTransferStatusDetailsEntity2.setTarFilename("SLOG_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity2.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity2.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity2.setId(1);
			objFileTransferStatusDetailsEntity2.setOpenCloseDetilsId(1);
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity3=new FileTransferStatusDetailsEntity();
			objFileTransferStatusDetailsEntity3.setTarFilePath(testFdtSource.toString());
			objFileTransferStatusDetailsEntity3.setFileType(Constants.ITU_LOG);
			objFileTransferStatusDetailsEntity3.setTarFilename("ITU_DELTA0001_1559711897000.tar.gz");
			objFileTransferStatusDetailsEntity3.setOriginalFilename("hai");
			objFileTransferStatusDetailsEntity3.setChunkSumCount(1);
			objFileTransferStatusDetailsEntity3.setId(1);
			objFileTransferStatusDetailsEntity3.setOpenCloseDetilsId(1);
			objFileTransferStatusDetailsEntity3.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
			objList.add(objFileTransferStatusDetailsEntity0);
			objList.add(objFileTransferStatusDetailsEntity1);
			objList.add(objFileTransferStatusDetailsEntity2);
			objList.add(objFileTransferStatusDetailsEntity3);
			
			List<ChunksDetailsEntity> objChunksList=new ArrayList<>();
			ChunksDetailsEntity objChunksDetailsEntity=new ChunksDetailsEntity();
			objChunksDetailsEntity.setChunkTarFilePath(testFdtSource.toString());
			objChunksDetailsEntity.setChunkName("hai");
			objChunksList.add(objChunksDetailsEntity);
			Map<String, Object> objTotDetails = new LinkedHashMap<>();

			Map<Integer, String> objPathDetails = new LinkedHashMap<>();
			objPathDetails.put(1, "hai");
			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", 1);
			when(env.getProperty("GROUND_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_IP")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("GROUND_SERVER_URL")).thenReturn("hai");
			when(env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("chunk_size_of_each_chunk")).thenReturn("0");
			when(env.getProperty("size_limit_of_tar_file")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_IP")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_PORT")).thenReturn("0");
			when(env.getProperty("OFFLOADED_SERVER_URL")).thenReturn("0");
			
			when(commonUtil.isIPandportreachable(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
			;
		
			
			when(fileTransferStatusDetailsRepository.saveFileTransferStatusDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(new FileTransferStatusDetailsEntity());
			when(fileTransferStatusDetailsRepository.saveChunkFileDetails(Mockito.any(ChunksDetailsEntity.class))).thenReturn(new ChunksDetailsEntity());
			
			
			when(fileTransferStatusDetailsRepository.getFileTransferDetails_manual()).thenReturn(objList);
			when(fileTransferStatusDetailsRepository.getFileTransferDetailsRestStatus(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(true);
			
			when(fileTransferStatusDetailsRepository.getChunkFileTransferDetails(Mockito.anyInt())).thenReturn(objChunksList);
			when(fileTransferStatusDetailsRepository.saveFileTransferDetails(Mockito.any(FileTransferStatusDetailsEntity.class))).thenReturn(true);
			when(objFlightOpenCloseRepository.saveFlightOpenCloseDetails(Mockito.any(FlightOpenCloseEntity.class))).thenReturn(new FlightOpenCloseEntity());
			when(fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(Mockito.anyInt())).thenReturn(true);
			when(fileTransferStatusDetailsRepository.getFileTransferDetails_ItuCheck()).thenReturn(objList);
			
			
			
			when(fileTransferStatusDetailsRepository.createRestTemplate(Mockito.any(HttpComponentsClientHttpRequestFactory.class))).thenReturn(restTemplate);
			when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(HttpEntity.class), Mockito.<Class<String>> any()))
			.thenReturn(ResponseEntity.ok("{\n" + 
					"  \"BITE_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": \"\"\n" + 
					"  },\n" + 
					"  \"AXINOM_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": false,\n" + 
					"    \"ExtractionStatus\": false\n" + 
					"  },\n" + 
					"  \"SLOG_DELTA0001_1559711897000.tar.gz\": {\n" + 
					"    \"ChecksumStatus\": true,\n" + 
					"    \"ExtractionStatus\": true\n" + 
					"  },\n" + 
					"  \"ITU_DELTA0001_1559711897000.tar.gz\": null\n" + 
					"}"));
			
			
			
			
			FlightOpenCloseEntity objStausFlightOpenCloseEntity=new FlightOpenCloseEntity();
			objStausFlightOpenCloseEntity.setDepartureTime(new Date());
			objStausFlightOpenCloseEntity.setItuFileStatus(Constants.READY_TO_TRANSMIT);
			objStausFlightOpenCloseEntity.setItuFolderName("");
			objStausFlightOpenCloseEntity.setId(1);
			
			List<FlightOpenCloseEntity> objOpenList=new ArrayList<>();
			objOpenList.add(objStausFlightOpenCloseEntity);
			
			when(objFlightOpenCloseRepository.getFltDesClsRdyToItuTraStatusManual()).thenReturn(objOpenList);
			when(objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper_manual()).thenReturn(objOpenList);
			
			
			
			fileTransferStatusDetailsServiceimpl.manualOffloadTransfer(testFdtdest.toString());
			//Assert.assertNotNull(objListFileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception for getmanualOffloadTransfer() in  FileTransferStatusDetailsServiceimplTest:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		
	}
}
