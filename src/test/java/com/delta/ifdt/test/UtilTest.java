package com.delta.ifdt.test;

import static org.mockito.Mockito.when;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.JavaMailSender;

import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.EmailUtil;
import com.delta.ifdt.util.FileFilterDateIntervalUtils;
import com.delta.ifdt.util.FileUtil;
import com.delta.ifdt.util.GlobalStatusMap;
import com.delta.ifdt.util.LoadPropertyFiles;
import com.delta.ifdt.util.PasswordCryption;

@RunWith(MockitoJUnitRunner.class)
public class UtilTest {
	
	@InjectMocks
	@Spy
	CommonUtil commonUtil;
	
	@InjectMocks
	@Spy
	FileUtil futil;
	
	@InjectMocks
	@Spy
	LoadPropertyFiles loadPropertyFiles;
	
	@InjectMocks
	@Spy
	DateUtil dateUtil;
	
	@InjectMocks
	@Spy
	GlobalStatusMap globalStatusMap;
	
	@InjectMocks
	@Spy
	PasswordCryption passwordCryption;
	
	@InjectMocks
	@Spy
	EmailUtil emailUtil;
	

	FileFilterDateIntervalUtils fileFilterDateIntervalUtils = new FileFilterDateIntervalUtils("","","mmddyyyy");
	
	@Mock
	File fileDir;
	
	@Mock
	JavaMailSender sender;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void FileFilterDateIntervalUtilsTest() {
		
		fileFilterDateIntervalUtils.accept(new File(""), "");
		
	}
	
	@Test
	public void commonUtilTest() throws Exception {
		
		CommonUtil.buildResponseJson("", "", "", "");
		
		CommonUtil.getHomeDirectory();
		
		CommonUtil.isValidObject(new ArrayList<>());
		CommonUtil.isValidObject(null);
		
	
		
		CommonUtil.toGetCurrentDir();
		
		CommonUtil.convertObjectToJson(new Object());
		CommonUtil.convertObjectToJson(null);
		
		CommonUtil.parseRequestDataToJson("{ }");
		
		CommonUtil.parseDataToJSON("{ }");
		
		
		CommonUtil.dateToString(new Date(),"mmddyyyy");
		CommonUtil.dateToString(new Date(),"abcd");
		
		
		CommonUtil.createZipFile("./src/test/resources/op/a.zip","./src/test/resources/test.txt");
		CommonUtil.createZipFile("","");
		
		CommonUtil.createZipFileOfDirectory("./src/test/resources/op/b.zip","./src/test/resources/testFolder/");
		CommonUtil.createZipFileOfDirectory(null,null);
		
		
		CommonUtil.getLeftSubStringWithLen("This is a test String",5);
		CommonUtil.getLeftSubStringWithLen(null,5);
		CommonUtil.getLeftSubStringWithLen("",5);
		CommonUtil.getLeftSubStringWithLen(" ",5);
		
		CommonUtil.decimalFormtter(0.0f);
		
		CommonUtil.createTarFile("","");
		CommonUtil.createTarFile("./src/test/resources/testFolder/","./src/test/resources/op/d.tar.gz");
		CommonUtil.createTarFile(null,null);
		
		
		
		/*CommonUtil.Chunking_System_logs("",5);
		CommonUtil.Chunking_System_logs("./src/test/resources/op/d.tar.gz",5);*/
		
		/*CommonUtil.computeFileChecksum(MessageDigest.getInstance("MD5"),"./src/test/resources/op/d.tar.gz");
		CommonUtil.computeFileChecksum(null,"./src/test/resources/op/d.tar.gz");*/
		
		CommonUtil.getFilesDateInterval("",new Date(),new Date(),"");
		CommonUtil.getFilesDateInterval(null,null,null,null);
		
		CommonUtil.writeDataToFile("File content for test","./src/test/resources/op/file_write.txt");
		CommonUtil.writeDataToFile(null,null);
		
		
	}
	
	@Test
	public void loadPropertyFilesTest() throws Exception {
		
		LoadPropertyFiles.getErrorCodeInstance();
		LoadPropertyFiles.getPropInstance();
		LoadPropertyFiles.init();
		LoadPropertyFiles.getInstance();
		
		
		loadPropertyFiles.getAppCodeProperty("");
		loadPropertyFiles.getAppCodeProperty(null);
		loadPropertyFiles.setAppCodeProperty("", "");
		loadPropertyFiles.getErrorCodeProperty("");
		loadPropertyFiles.getErrorCodeProperty(null);
		loadPropertyFiles.setConfigProperties("", "");
		loadPropertyFiles.getProperty(null);
		loadPropertyFiles.getProperty("hai");
		loadPropertyFiles.setConfigProperties("","");
		loadPropertyFiles.setConfigProperties(null,null);
		loadPropertyFiles.getUnAuthorizedAccessPropInstance();
		
	}
	
	@Test
	public void dateUtilTest() throws Exception {
		
		DateUtil.stringToDate("10121995","mmddyyyy");
		DateUtil.stringToDate("10121995"," ");
		DateUtil.stringToDate(""," ");
		DateUtil.stringToDate(null," ");
		
		DateUtil.dateToString(new Date(),null);
		DateUtil.dateToString(new Date(),"mmddyyyy");
		DateUtil.dateToString(null,"mmddyyyy");
		
		DateUtil.stringToDateEndTime("10121995","mmddyyyy");
		DateUtil.stringToDateEndTime("","mmddyyyy");
		DateUtil.stringToDateEndTime(null,"mdy");
		DateUtil.stringToDateEndTime("10121995",null);
		
		DateUtil.getDateStringInFormat("10121995","mmddyyyy","ddmmyyyy");
		DateUtil.getDateStringInFormat("","","");
		DateUtil.getDateStringInFormat(null,"abcd","ddmmyyyy");
		DateUtil.getDateStringInFormat("10121995","abcd","ddmmyyyy");
		
		DateUtil.getDate(new Date());
		
	}
	

	@Test
	public void fileUtilTest() throws Exception {

		FileUtil.deleteFileOrFolder("");
		FileUtil.deleteFileOrFolder("./src/test/resources/op/b.zip");
		FileUtil.deleteFileOrFolder("./src/test/resources/op/");
		FileUtil.deleteFileOrFolder(null);
		
		FileUtil.createDirectory("");
		FileUtil.createDirectory("./src/test/resources/op/t1/");
		
		FileUtil.deleteFileOrFolder("./src/test/resources/op/");
		
		
		FileUtil.copyFastFileToLocation("./src/test/resources/testFolder/test.txt", "./src/test/resources/testFolder/test_new.txt");
		FileUtil.copyFastFileToLocation("./src/test/resources/testFolder/", "./src/test/resources/op2");
		
		FileUtil.copyFastFileToLocationbetweenDatess("", "", new Date(), new Date(), "");
		
		FileUtil.copyFastFileToLocationbetweenDatessItu("", "", new Date(), new Date(), "");
		

	}
	
	@Test
	public void passwordCryptionTest() {
		
		PasswordCryption.setKey("abk67SDFGHJ");
		PasswordCryption.setKey("");
		
		PasswordCryption.encrypt("abk67SDFGHJ");
		PasswordCryption.encrypt(null);
		
		PasswordCryption.decrypt("abk67SDFGHJ");
		PasswordCryption.decrypt("");
		
		PasswordCryption.decryptPasswordUI("abk67SDFGHJ");
		PasswordCryption.decryptPasswordUI(null);
		
		PasswordCryption.encryptPasswordUI("abk67SDFGHJ");
		PasswordCryption.encryptPasswordUI(null);
		
	}
	
	@Test
	public void emailUtilTest() throws Exception {
		Session session = null;
		when(sender.createMimeMessage()).thenReturn(new MimeMessage(session));
		emailUtil.sendEmail(new String[] { "abc@xyz.com", "abc1@xyz.com" },
				new String[] { "abc2@xyz.com", "abc12@xyz.com" }, new String[] { "abc4@xyz.com", "abc15@xyz.com" }, "",
				"", false);
		
		
		
		when(sender.createMimeMessage()).thenReturn(new MimeMessage(session));
		emailUtil.sendEmail(new String[] { "abc@xyz.com", "abc1@xyz.com" },
				new String[] { "abc2@xyz.com", "abc12@xyz.com" }, new String[] { "abc4@xyz.com", "abc15@xyz.com" }, "",
				"",new File(""),"", false);
		
		
	}
	
	
	

}
