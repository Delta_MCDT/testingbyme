package com.delta.ifdt.test;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.FlightMetaDataModel;
import com.delta.ifdt.models.ManualOffload;
import com.delta.ifdt.models.PichartRestOffloadModel;
import com.delta.ifdt.models.PropertiesTemplateModel;
import com.delta.ifdt.models.StatusQueryModel;
import com.delta.ifdt.models.User;
import com.delta.ifdt.models.UserManagementModel;

@RunWith(MockitoJUnitRunner.class)
public class ModelTest {
	
	@InjectMocks
	@Spy
	ManualOffload manualOffload;
	
	@InjectMocks
	@Spy
	PropertiesTemplateModel propertiesTemplateModel;
	
	@InjectMocks
	@Spy
	StatusQueryModel statusQueryModel;
	
	@InjectMocks
	@Spy
	User user;
	
	@InjectMocks
	@Spy
	PichartRestOffloadModel pichartRestOffloadModel;
	
	@InjectMocks
	@Spy
	BarchartDataModel barchartDataModel;
	
	@InjectMocks
	@Spy
	FlightMetaDataModel flightMetaDataModel;
	
	@InjectMocks
	@Spy
	FileTransferStatusDetailsModel fileTransferStatusDetailsModel;
	
	@InjectMocks
	@Spy
	UserManagementModel userManagementModel;

	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void userManagementModelTest() {
		
		userManagementModel.setConfirmPassword("");
		userManagementModel.getConfirmPassword();
		userManagementModel.setCreationDate("");
		userManagementModel.getCreationDate();
		userManagementModel.setEmailId("");
		userManagementModel.getEmailId();
		userManagementModel.setFullName("");
		userManagementModel.getFullName();
		userManagementModel.setId(5);
		userManagementModel.getId();
		userManagementModel.setLastLoginDate("");
		userManagementModel.getLastLoginDate();
		userManagementModel.setPassword("");
		userManagementModel.getPassword();
		userManagementModel.getConfirmPassword();
		userManagementModel.setRemarks("");
		userManagementModel.getRemarks();
		userManagementModel.setUserName("");
		userManagementModel.getUserName();
		userManagementModel.setStatus("");
		userManagementModel.getStatus();
		userManagementModel.getRole();
		userManagementModel.setRole("");
		userManagementModel.setRoleId(5);
		userManagementModel.getRoleId();
		
	}
	
	@Test
	public void fileTransferStatusDetailsModelTest() {
		
		fileTransferStatusDetailsModel.setChecksum("");
		fileTransferStatusDetailsModel.getChecksum();
		fileTransferStatusDetailsModel.setFlightOpenCloseEntity(new FlightOpenCloseEntity());
		fileTransferStatusDetailsModel.getFlightOpenCloseEntity();
		fileTransferStatusDetailsModel.setFromDate("");
		fileTransferStatusDetailsModel.getFromDate();
		fileTransferStatusDetailsModel.setId(5);
		fileTransferStatusDetailsModel.getId();
		fileTransferStatusDetailsModel.setModeOfTransfer("");
		fileTransferStatusDetailsModel.getModeOfTransfer();
		fileTransferStatusDetailsModel.setOriginalFilename("");
		fileTransferStatusDetailsModel.getOriginalFilename();
		fileTransferStatusDetailsModel.setStatus("");
		fileTransferStatusDetailsModel.getStatus();
		fileTransferStatusDetailsModel.setTarFileDate("");
		fileTransferStatusDetailsModel.getTarFileDate();
		fileTransferStatusDetailsModel.setTarFilename("");
		fileTransferStatusDetailsModel.getTarFilename();
		fileTransferStatusDetailsModel.setTarFilePath("");
		fileTransferStatusDetailsModel.getTarFilePath();
		fileTransferStatusDetailsModel.setToDate("");
		fileTransferStatusDetailsModel.getToDate();
		
		
	}
	
	@Test
	public void flightMetaDataModelTest() {
		flightMetaDataModel.setAircraftsubtype("");
		flightMetaDataModel.getAircraftsubtype();
		flightMetaDataModel.setAircrafttype("");
		flightMetaDataModel.getAircrafttype();
		flightMetaDataModel.setAirline("");
		flightMetaDataModel.getAirline();
		flightMetaDataModel.setArrivalairport("");
		flightMetaDataModel.getArrivalairport();
		flightMetaDataModel.setArrivaltime("");
		flightMetaDataModel.getArrivaltime();
		flightMetaDataModel.setTailnumber("");
		flightMetaDataModel.getTailnumber();
		flightMetaDataModel.setDepartureairport("");
		flightMetaDataModel.getDepartureairport();
		flightMetaDataModel.setDeparturetime("");
		flightMetaDataModel.getDeparturetime();
		flightMetaDataModel.setDeparturetime("");
		flightMetaDataModel.setFlightnumber("");
		flightMetaDataModel.getFlightnumber();
		
		
	}
	
	@Test
	public void barchartDataModelTest() {
		
		barchartDataModel.setBackgroundColor("");
		barchartDataModel.getBackgroundColor();
		barchartDataModel.setData(new ArrayList());
		barchartDataModel.getData();
		barchartDataModel.setHoverBackgroundColor("");
		barchartDataModel.getHoverBackgroundColor();
		barchartDataModel.setLabel("");
		barchartDataModel.getLabel();
		barchartDataModel.setPercData(new ArrayList());
		barchartDataModel.getPercData();
	}
	
	@Test
	public void pichartRestOffloadModelTest() {
		
		pichartRestOffloadModel.setManvalTransferDatacount(56);
		pichartRestOffloadModel.getManvalTransferDatacount();
		pichartRestOffloadModel.setPercentageoffloadData(55.5f);
		pichartRestOffloadModel.getPercentageoffloadData();
		pichartRestOffloadModel.setPercentagerestData(65.5f);
		pichartRestOffloadModel.getPercentagerestData();
		pichartRestOffloadModel.setRestTransferDatacount(56);
		pichartRestOffloadModel.getRestTransferDatacount();
		pichartRestOffloadModel.setTotTransferDatacount(56);
		pichartRestOffloadModel.getTotTransferDatacount();
	}
	
	@Test
	public void userTest() {
		user.setLastLoginTime("");
		user.getLastLoginTime();
		user.setRole("");
		user.getRole();
		user.setRoleId(5);
		user.getRoleId();
		user.setServiceToken("");
		user.getServiceToken();
		user.setUserName("");
		user.getUserName();
		user.getTokenKey();
		
	}
	
	@Test
	public void statusQueryModelTest(){
		
		statusQueryModel.getChecksumStatus();
		statusQueryModel.setChecksumStatus("");
		statusQueryModel.getExtractionStatus();
		statusQueryModel.setExtractionStatus("");
	}
	
	@Test
	public void propertiesTemplateModelTest() {
		
		propertiesTemplateModel.getLabel();
		propertiesTemplateModel.setLabel("");
		propertiesTemplateModel.getValue();
		propertiesTemplateModel.setValue("");
		
	}
	
	@Test
	public void manualOffloadTest() {
		manualOffload.getFiles();
		manualOffload.setFiles(new String[] {});
		
	}
	
}
