package com.delta.ifdt.dto;


import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.util.CommonUtil;



@Component
public class FileTransferStatusDetailsDto {
	
	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsDto.class);
	
	
	/**
	 * This method gets FileTransferStatusDetailsEntity
	 * 
	 * @param fileTransferStatusDetailsModel,sessionId
	 * @return FileTransferStatusDetailsEntity
	 */
	public FileTransferStatusDetailsEntity getFileTransferStatusDetailsEntity(FileTransferStatusDetailsModel fileTransferStatusDetailsModel)
	{
		logger.info("method invoked==>FileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity");
		FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = null;
		try
		{
			if(fileTransferStatusDetailsModel != null)
			{
				fileTransferStatusDetailsEntity  = new FileTransferStatusDetailsEntity();
				if(fileTransferStatusDetailsModel.getId() != null  && fileTransferStatusDetailsModel.getId() != 0)
				{
					fileTransferStatusDetailsEntity.setId(fileTransferStatusDetailsModel.getId());
				}
				
				fileTransferStatusDetailsEntity.setTarFileDate(new Date());
				fileTransferStatusDetailsEntity.setOriginalFilename(fileTransferStatusDetailsModel.getOriginalFilename());
				fileTransferStatusDetailsEntity.setStatus(fileTransferStatusDetailsModel.getStatus());
				fileTransferStatusDetailsEntity.setTarFilename(fileTransferStatusDetailsModel.getTarFilename());
				fileTransferStatusDetailsEntity.setChecksum(fileTransferStatusDetailsModel.getChecksum());
				fileTransferStatusDetailsEntity.setTarFilePath(fileTransferStatusDetailsModel.getTarFilePath());
								
				
				
			}
		}catch (Exception e) {
			logger.error("Excpetion in  FileTransferStatusDetailsDto.getFileTransferStatusDetailsEntity() : " + ExceptionUtils.getFullStackTrace(e));
			
		}
		
		return fileTransferStatusDetailsEntity;
		
	}


	public FileTransferStatusDetailsModel getStatisticsDetailsEntity(FileTransferStatusDetailsEntity objEntity) {
		FileTransferStatusDetailsModel objModel = null;
		try {
			if (objEntity != null) {
				objModel = new FileTransferStatusDetailsModel();
				objModel.setId(objEntity.getId());
				objModel.setTarFilename(objEntity.getTarFilename());
				objModel.setModeOfTransfer(objEntity.getModeOfTransfer());
				objModel.setOriginalFilename(objEntity.getOriginalFilename());
				objModel.setTarFileDate(CommonUtil.dateToString(objEntity.getTarFileDate(),Constants.YYYY_MM_DD_HH_MM_SS));
			}
		} catch (Exception e) {
			logger.error("Excpetion in FileTransferStatusDetailsDto.getStatisticsDetailsEntity() : " + ExceptionUtils.getFullStackTrace(e));
			
		}
		return objModel;
	}

}
