package com.delta.ifdt.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.models.UserManagementModel;

@Component
public class UserManagementDto {

	static final Logger logger = LoggerFactory.getLogger(UserManagementDto.class);

	/**
	 * This method will set the UserManagementEntity with values from
	 * UserManagementModel.
	 * 
	 * @param userDetails
	 * @return userManagementEntity
	 */
	public UserManagementEntity getUserDetails(UserManagementModel userDetails) {
		UserManagementEntity userManagementEntity = null;
		try {
			if (userDetails != null) {
				userManagementEntity = new UserManagementEntity();
				if (userDetails.getId() != null && userDetails.getId() != 0) {
					userManagementEntity.setId(userDetails.getId());
				}
				SimpleDateFormat creationDateGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				creationDateGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
				userManagementEntity.setUserName(userDetails.getUserName());
				userManagementEntity.setFullName(userDetails.getFullName());
				userManagementEntity.setEmailId(userDetails.getEmailId());
				userManagementEntity.setPassword(userDetails.getPassword());
				userManagementEntity.setStatus(userDetails.getStatus());
				userManagementEntity.setRemarks(userDetails.getRemarks());
				userManagementEntity.setCreationDate(creationDateGmt.format(new Date()));
				UserRoleEntity userRoleEntityobj = new UserRoleEntity();
				userRoleEntityobj.setId(userDetails.getRoleId());
				userManagementEntity.setUserRoleEntity(userRoleEntityobj);
			}
		} catch (Exception e) {
			logger.error("Excpetion in UserManagementDto.getUserDetails(): " + ExceptionUtils.getFullStackTrace(e));
			
		}
		return userManagementEntity;
	}
}
