package com.delta.ifdt.interceptor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class GlobalInitializerListener implements ApplicationListener<ContextRefreshedEvent> {
	static final Logger logger = LoggerFactory.getLogger(GlobalInitializerListener.class);

	public static HashMap<String, String> faultCodeMap = new HashMap<>();
	public static List<String> tailNumberList = new ArrayList<>();
	public static HashMap<String, List<String>> unAuthorizedAccessUrlMap = new HashMap<>();

	@Value("${sessionTimeOut}")
	public String sessionTime;

	public static long MAX_INACTIVE_SESSION_TIMEOUT = 3600000; // Default
																// value.. 1
																// Hour = 60 min
																// * 60 sec *
																// 1000
																// Milliseconds
	String msg = "------====== SETTING SESSION TIMEOUT TO A DEFAULT VALUE. PLEASE SET A VALID TIMEOUT VALUE. =====------";

	BufferedReader bufferedReader = null;

	/**
	 * This method will load fault codes
	 * 
	 * @param arg0
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws Exception
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {

		if (arg0.getApplicationContext().getParent() == null) {
			logger.info("..................ApplicationListener Initialization of Fault codes Started ............");
			String filePath = "/errorcode.properties";
			String line = null;

			try {
				
				logger.info(
						"..................ApplicationListener Initialization of RCT properties Started ............");
				

				try {
					int intVal = Integer.parseInt(sessionTime);

					if (intVal > 0) {
						MAX_INACTIVE_SESSION_TIMEOUT = intVal * 60000;
					} else {
						logger.warn(msg);
					}
				} catch (NumberFormatException e) {
					logger.error("Failed to Load Configured Session Timeout::: " + e);
					logger.warn(msg);
					
				}

				
				Resource resource = new ClassPathResource(filePath);
				InputStream resourceInputStream = resource.getInputStream();
				bufferedReader = new BufferedReader(new InputStreamReader(resourceInputStream));
				while ((line = bufferedReader.readLine()) != null) {
					if (!line.trim().isEmpty() && !line.trim().startsWith("#") && !line.equals("")) {
						String[] errorFaultCodes = line.split("=");
						faultCodeMap.put(errorFaultCodes[0].trim(), errorFaultCodes[1].trim());
					}
				}
				

			} catch (FileNotFoundException e) {
				logger.error("Failed to load the error.properties file", e);
			} catch (IOException e) {
				logger.error("Failed to load fault codes ", e);
			} catch (Exception e) {
				logger.error("Exception in onApplicationEvent()  GlobalInitializerListener:: ", e);
			}
			logger.info("..................ApplicationListener Initialization of Fault codes End............");
		}
	}

	
}
