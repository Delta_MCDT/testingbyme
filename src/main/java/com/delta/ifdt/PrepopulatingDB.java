package com.delta.ifdt;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.delta.ifdt.service.UserLoginService;

@Component
public class PrepopulatingDB {
	
	@Autowired
	UserLoginService userLoginService;
	
	@PostConstruct
	public void dbData()
	{
		userLoginService.executeDbData();
	}

}
