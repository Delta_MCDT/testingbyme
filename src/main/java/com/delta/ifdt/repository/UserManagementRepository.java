package com.delta.ifdt.repository;

import java.util.List;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;

public interface UserManagementRepository {

	boolean duplicateUser(UserManagementModel userDetails);

	boolean createUser(UserManagementEntity userEntity);

	List<UserManagementModel> getUserList();

	boolean deleteUser(Integer valueOf);

}
