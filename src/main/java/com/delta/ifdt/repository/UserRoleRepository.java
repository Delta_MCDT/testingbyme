package com.delta.ifdt.repository;

import java.util.List;

import com.delta.ifdt.entities.UserRoleEntity;

public interface UserRoleRepository {

	List<UserRoleEntity> getUserRoleList();

}
