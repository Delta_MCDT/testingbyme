package com.delta.ifdt.repository;

import java.util.List;
import java.util.Map;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;

public interface DashBoardDetailsRepository {
	
	
	List<FileTransferStatusDetailsEntity> getDetailsOfDashBoard(String fromDate, String toDate);

	Map<String, Object> getGraphDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getFileGraphDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page, int count);

	Map<String, Object> getFailureReasonDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel, int page,
			int count);
	
	
	

}
