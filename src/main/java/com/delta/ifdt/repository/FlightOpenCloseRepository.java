package com.delta.ifdt.repository;

import java.util.List;

import com.delta.ifdt.entities.FlightOpenCloseEntity;

public interface FlightOpenCloseRepository {

	FlightOpenCloseEntity getLastFlightOpenDetails();

	FlightOpenCloseEntity saveFlightOpenCloseDetails(FlightOpenCloseEntity objFlightOpenCloseEntity);
	List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper();
	List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper_manual();
	List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatus();
	List<FlightOpenCloseEntity> getFlightDetails_with_received_main_entity();
	List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusOffLoad();
	List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusManual();
	

}
