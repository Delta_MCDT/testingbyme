package com.delta.ifdt.repository;

import java.util.Date;
import java.util.Map;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;

public interface UserLoginRepository {


	boolean setLastLogin(String userName, Date loginDate);

	boolean changePassword(String userName, String newPassword, boolean b);

	UserManagementEntity getUserDetailsBasedName(String userName);

	UserManagementModel getUserDetails(String userName);

	UserManagementEntity getUserDetailsByEmailId(String emailId);

	Map<String, Object> getMenuList();

	void getRoleData();

	void getUserData();

	void getHeaderData();

}
