package com.delta.ifdt.repository;

import java.util.List;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;


public interface FileTransferStatusDetailsRepository {
	
    boolean saveFileTransferDetails(FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity);
	List<FileTransferStatusDetailsEntity> getFileTransferDetails();
	List<FileTransferStatusDetailsEntity> getFileTransferDetails_manual();
	boolean deleteFileTransferDetails(Integer  chunkeDetailsId);
	FileTransferStatusDetailsEntity saveFileTransferStatusDetails(FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity);
	ChunksDetailsEntity saveChunkFileDetails(ChunksDetailsEntity objChunksDetailsEntity);
	List<ChunksDetailsEntity> getChunkFileTransferDetails(int parentFileId);
	List<FileTransferStatusDetailsEntity> getFileTransferDetailsOffLoadStatus();
	boolean getFileTransferDetailsRestStatus(FlightOpenCloseEntity objLocFlightOpenCloseEntity);
	List<ChunksDetailsEntity> removeChunkFileTransferDetails_manual(Integer id);
	List<FileTransferStatusDetailsEntity> getFileTransferDetailsReceivedStatus();
	boolean deleteFileTransferDetailsChunks(Integer parentId);
	boolean getFleTraferDetRestStatusCheck(FlightOpenCloseEntity objLocFlightOpenCloseEntity);
	boolean getFileTransferDetailsRestStatusRecived(FlightOpenCloseEntity objLocFlightOpenCloseEntity);
	List<FileTransferStatusDetailsEntity> getFileTransferDetails_ItuCheck();
	boolean updateFileTransferDetailsChunks(Integer parentId); 
	RestTemplate createRestTemplate(HttpComponentsClientHttpRequestFactory requestFactory);
	
}