package com.delta.ifdt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


import com.delta.ifdt.models.ManualOffload;


public class ToCheckgroundURL {
	static final Logger logger = LoggerFactory.getLogger(ToCheckgroundURL.class);
	public static void main(String args[]) {
		
		boolean status = true;
		try {
			HttpHeaders headers = new HttpHeaders();
			// headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			// headers.setContentType(new MediaType("application","json"));

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			String data="bbs";
			 logger.info("DashBoardDetailsController.manualOffload() metadatajson manualoffload " , data);

			/*
			 * body.add("file", new FileSystemResource(
			 * "/home/user/Documents/deltapathsDestination/var/deltaxray/mts/BITE_SA2887_201905201834.tar.gz"
			 * )); body.add("chunk_checksum", "c1a6c5fa1490aaea79f539fc0850c1cd");
			 * body.add("file_checksum", "c1a6c5fa1490aaea79f539fc0850c1cd");
			 * body.add("current_part",1); body.add("total_parts",1);
			 * body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);
			 */

			System.out.println(body);
			String[] files = new String[] {"BITE_DA156_1558265388000.tar.gz", "BITE_DA156_1558265388001.tar.gz"};
			ManualOffload manualOffload = new ManualOffload();
			manualOffload.setFiles(files);
			

			/*
			 * String[] a= new String[2]; a[0] = "\"BITE_DA156_1558265388000.tar.gz\""; a[1]
			 * = "\"BITE_DA156_1558265388100.tar.gz\"";
			 */

			/*
			 * ManualOffloadStatusFilesModel objManualOffloadStatusFilesModel=new
			 * ManualOffloadStatusFilesModel();
			 * objManualOffloadStatusFilesModel.setFiles(a);
			 */
//			List<String> objList = new ArrayList<>();
//			objList.add("BITE_DA156_1558265388000.tar.gz");
//			objList.add("BITE_DA156_1558265388001.tar.gz");
//			body.add("files", objList);

			HttpEntity<ManualOffload> requestEntity = new HttpEntity<>(manualOffload, headers);
			// String serverUrl = "https://10.9.41.240:3000/gas/v1/logs";

			String serverUrl = "https://10.9.41.240:3000/gas/v1/offload/status";			
			// String serverUrl = "http://localhost:8086/bbbs";

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);

			RestTemplate restTemplate = new RestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.exchange(serverUrl, HttpMethod.POST, requestEntity,
					String.class);
			System.out.println("######### 	Response: " + response.getBody());
			int statusCode = response.getStatusCodeValue();
			System.out.println("Status Code: " + statusCode);

			if (200 == statusCode) {
				
			}
		} catch (Exception e) {
			logger.error("Exception  main() in  ToCheckgroundURL:"
					+ ExceptionUtils.getFullStackTrace(e));
			status = false;

		}

		System.out.println(status);

	}

	
}
