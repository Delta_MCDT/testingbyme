package com.delta.ifdt.repositoryImpl;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.MenuEntity;
import com.delta.ifdt.entities.SubMenuEntity;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserLoginRepository;
import com.delta.ifdt.util.PasswordCryption;

@Repository
@Transactional
public class UserLoginRepositoryImpl implements UserLoginRepository{
	
	static final Logger logger = LoggerFactory.getLogger(UserLoginRepositoryImpl.class);
	
	@PersistenceContext
	EntityManager entityManager;

	
	/**
	 * This method will return User Details
	 * 
	 * @param userName
	 * @return UserManagementModel
	 */
	@SuppressWarnings("deprecation")
	@Override
	@Transactional()
	public UserManagementModel getUserDetails(String userName) {
		UserManagementModel userList = null;
		try {
			Criteria criteriaTotList = entityManager.unwrap(Session.class).createCriteria(UserManagementEntity.class);
			criteriaTotList.createAlias("userRoleEntity", "userRoleEntity");
			criteriaTotList.add(Restrictions.eq(Constants.USER_NAME, userName));
			Projection projection = Projections.projectionList().add(Projections.property(Constants.USER_NAME), Constants.USER_NAME)
					.add(Projections.property("fullName"), "fullName").add(Projections.property("remarks"), "remarks")
					.add(Projections.property("status"), "status").add(Projections.property(Constants.EMAIL_ID), Constants.EMAIL_ID)
					.add(Projections.property("password"), "password").add(Projections.property("id"), "id")
					.add(Projections.property("userRoleEntity.id"), "roleId").add(Projections.property("userRoleEntity.role"), "role");
					
			criteriaTotList.setProjection(projection);
			criteriaTotList.addOrder(Order.desc("creationDate"));
			userList = (UserManagementModel) criteriaTotList.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)
					.setResultTransformer(new AliasToBeanResultTransformer(UserManagementModel.class)).uniqueResult();
		} catch (Exception e) {
			logger.error(
					"Exception in UserLoginRepositoryImpl.getUserDetails(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return userList;
	}

	
	
	/**
	 * This method will set last login
	 * 
	 * @param userName
	 * @param loginDate
	 * @return boolean
	 */
	@Override
	@Transactional()
	public boolean setLastLogin(String userName, Date loginDate) {
		boolean status = false;
		try {
			UserManagementEntity user = getUserDetailsBasedName(userName);
			user.setLastLoginDate(loginDate);
			entityManager.merge(user);
			status = true;
		} catch (Exception e) {
			logger.error(
					"Exception in UserLoginRepositoryImpl.setLastLogin(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;
	}
	
	
	
	/**
	 * This method will return user Details based name
	 * 
	 * @param userName
	 * @return UserManagementEntity
	 */
	@Override
	@Transactional()
	public UserManagementEntity getUserDetailsBasedName(String userName) {
		UserManagementEntity user = null;
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<UserManagementEntity> query = builder.createQuery(UserManagementEntity.class);
			Root<UserManagementEntity> root = query.from(UserManagementEntity.class);
			query.select(root);
			query.where(builder.equal(root.get(Constants.USER_NAME), userName));
			TypedQuery<UserManagementEntity> queryResult = entityManager.createQuery(query);
			user =  queryResult.getSingleResult();
		} catch (Exception e) {
			logger.error("Exception in UserLoginRepositoryImpl.getUserDetailsBasedName: "
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return user;
	}

	
	/**
	 * This method is used to change the password
	 * 
	 * @param userName
	 * @param newPassword
	 * @param b
	 * @return boolean
	 */
	@Override
	@Transactional()
	public boolean changePassword(String userName, String newPassword, boolean b) {
		boolean status = false;
		try {
			UserManagementEntity user = getUserDetailsBasedName(userName);
			if (b) {
				user.setPassword(PasswordCryption.encrypt(PasswordCryption.decryptPasswordUI(newPassword)));
			} else {
				user.setPassword(PasswordCryption.encrypt(newPassword));
			}
			entityManager.merge(user);
			status = true;
		} catch (Exception e) {
			logger.error("Exception UserLoginRepositoryImpl.changePassword(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;
	}

	
	/**
	 * This method will return User Details By EmailId
	 * 
	 * @param emailId
	 * @param b
	 * @return UserManagementEntity
	 */
	@Override
	@Transactional()
	public UserManagementEntity getUserDetailsByEmailId(String emailId) {
		UserManagementEntity user = null;
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<UserManagementEntity> query = builder.createQuery(UserManagementEntity.class);
			Root<UserManagementEntity> root = query.from(UserManagementEntity.class);
			query.select(root);
			query.where(builder.equal(root.get(Constants.EMAIL_ID), emailId));
			TypedQuery<UserManagementEntity> queryResult = entityManager.createQuery(query);
			user =  queryResult.getSingleResult();
		} catch (Exception e) {
			logger.error("Exception in UserActionRepositoryImpl.getUserDetailsByEmailId(): "
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return user;
	}

	@Override
	public Map<String, Object> getMenuList() {
		Map<String, Object> objMap = new HashMap<String, Object>();
		List<MenuEntity> objList=null;
		List<MenuEntity> objSubList=null;
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(MenuEntity.class);
			//dont remove
		/*	Criteria subCriteria = entityManager.unwrap(Session.class)
					.createCriteria(SubMenuEntity.class);
			objSubList = subCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			if(objSubList.size()>0) 
			{
			criteria.createAlias("subMenuItems", "subMenuItems");
			criteria.addOrder(Order.asc("subMenuItems.id"));
			}*/
			objList=criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			objMap.put("test", objList);
		}catch (Exception e) {
			logger.error("Exception in UserActionRepositoryImpl.getMenuList(): "
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objMap;
	}


	@Override
	public void getRoleData() {
		String hql1 = "select count(*) FROM user_roles where role='Admin'";
		Integer value = (Integer) entityManager.createNativeQuery(hql1).getSingleResult();
		if(value.intValue()<=0) {
			String hql = "INSERT INTO user_roles(id,role)VALUES('1','Admin')";
			entityManager.createNativeQuery(hql).executeUpdate();
		}
		entityManager.flush();
		entityManager.clear();
	
	}



	@Override
	public void getUserData() {
		String hql1 = "select count(*) FROM user_details where user_name='delta'";
		Integer value = (Integer) entityManager.createNativeQuery(hql1).getSingleResult();
		if(value.intValue()<=0) {
			String hql = "INSERT INTO user_details(id,creation_date,email_id,full_name,last_login_date,password,remarks,status,user_name,role_id)VALUES(1,\"2019-05-16 15:04:33\",\"supriya.maddukuri@ltts.com\",\"supriya\",\"1558680302089\",\"QzFCSyotl2AmtoTozdlKnw==\",\"\",\"Active\",\"delta\",\"1\")";
			entityManager.createNativeQuery(hql).executeUpdate();
		}
		entityManager.flush();
		entityManager.clear();
	}



	@Override
	public void getHeaderData() {
		String hql1 = "select count(*) FROM main_menu_details";
		Integer value = (Integer) entityManager.createNativeQuery(hql1).getSingleResult();
		if(value.intValue()<4)
		{
		String hql = "INSERT INTO main_menu_details(id,dispaly_text,route_name)\n" + 
				"VALUES(1,\"DASHBOARD\",\"/dashboard\"),\n" + 
				"(2,\"USER MANAGEMENT\",\"/usermanagement\"),\n" + 
				"(3,\"STATISTICS\",\"/statistics\"),\n" + 
				"(4,\"CONFIGURATIONS\",\"/configuration\")";
		entityManager.createNativeQuery(hql).executeUpdate();
		}
		entityManager.flush();
		entityManager.clear();
	}

}
