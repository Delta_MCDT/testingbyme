package com.delta.ifdt.repositoryImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FlightOpenCloseRepository;

@Repository
@Transactional
public class FlightOpenCloseRepositoryImpl  implements FlightOpenCloseRepository{
	
	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;

	
	/**
	 * This method will return Last Flight Open Details
	 * 
	 * @param 
	 * @return FlightOpenCloseEntity
	 */
	@Override
	public FlightOpenCloseEntity getLastFlightOpenDetails() {

		FlightOpenCloseEntity objFlightOpenCloseEntity=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getLastFlightOpenDetails()");
		
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			
		
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_OPEN));
			criteria.addOrder(Order.desc("id"));
			criteria.setMaxResults(1);

			
			objFlightOpenCloseEntity = (FlightOpenCloseEntity)criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
			
		} catch (Exception e) {
			logger.error("Exception  getLastFlightOpenDetails() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntity;
	}

	
	/**
	 * This method will save Flight OpenClose Details
	 * 
	 * @param objFlightOpenCloseEntity
	 * @return FlightOpenCloseEntity
	 */
	@Override
	public FlightOpenCloseEntity saveFlightOpenCloseDetails(FlightOpenCloseEntity objFlightOpenCloseEntity) {

		logger.info(" FileTransferStatusDetailsRepositoryImpl.saveFlightOpenCloseDetails()");
		try {
			objFlightOpenCloseEntity=entityManager.merge(objFlightOpenCloseEntity);
		} catch (Exception e) {
			logger.error("Exception  saveFlightOpenCloseDetails() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return objFlightOpenCloseEntity;
	}
	
	
	
	/**
	 * This method will return FlightDetailsCloseAndReadyToTransper
	 * 
	 * @param 
	 * @return List<FlightOpenCloseEntity>
	 */
	@Override
	public List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFlightDetailsCloseAndReadyToTransper()");
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_CLOSE));
			//criteria.add(Restrictions.eq("transferStatus", Constants.READY_TO_TRANSMIT));
			Criterion criteria1 = Restrictions.eq("transferStatus", Constants.READY_TO_TRANSMIT);
		    Criterion criteria2 = Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT);
		    criteria.add(Restrictions.or(criteria1, criteria2));
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFlightDetailsCloseAndReadyToTransper() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}
   
	
	@Override
	public List<FlightOpenCloseEntity> getFlightDetails_with_received_main_entity() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFlightDetailsCloseAndReadyToTransper()");
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_CLOSE));
			Criterion criteria1 = Restrictions.eq("transferStatus", Constants.RECEIVED);
		    Criterion criteria2 = Restrictions.eq("ituFileStatus", Constants.RECEIVED);
		    criteria.add(Restrictions.or(criteria1, criteria2));
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFlightDetailsCloseAndReadyToTransper() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}
	
	
	/**
	 * This method will return FlightDetailsCloseAndReadyToTransper_manual 
	 * 
	 * @param 
	 * @return List<FlightOpenCloseEntity>
	 */
	@Override
	public List<FlightOpenCloseEntity> getFlightDetailsCloseAndReadyToTransper_manual() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_CLOSE));
			Criterion criteria1 = Restrictions.eq("transferStatus", Constants.READY_TO_TRANSMIT);
		    Criterion criteria2 = Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT);
		    criteria.add(Restrictions.or(criteria1, criteria2));
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFlightDetailsCloseAndReadyToTransper() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}
	
	@Override
	public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatus() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFltDesClsRdyToItuTraStatus()");
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_CLOSE));
			//Criterion criteria1 = Restrictions.eq("transferStatus", Constants.READY_TO_TRANSMIT);
			criteria.add(Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT));
			 criteria.add(Restrictions.eq("transferStatus",Constants.RECEIVED));
		   /* Criterion criteria2 = Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT);
		    criteria.add(Restrictions.or(criteria1, criteria2));*/
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFltDesClsRdyToItuTraStatus() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}
	@Override
	public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusManual() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFltDesClsRdyToItuTraStatus()");
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq(Constants.CLOSE_OPEN_STATUS, Constants.FLIGHT_CLOSE));
		    criteria.add(Restrictions.eq("ituFileStatus", Constants.READY_TO_TRANSMIT));
		    criteria.add(Restrictions.eq("transferStatus",Constants.RECEIVED));
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFltDesClsRdyToItuTraStatus() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}
	@Override
	public List<FlightOpenCloseEntity> getFltDesClsRdyToItuTraStatusOffLoad() {

		List<FlightOpenCloseEntity> objFlightOpenCloseEntityList=null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFltDesClsRdyToItuTraStatus()");
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FlightOpenCloseEntity.class);
			criteria.add(Restrictions.eq("ituFileStatus",Constants.READY_TO_TRANSMIT));
			criteria.add(Restrictions.eq("transferStatus",Constants.RECEIVED));
			criteria.addOrder(Order.asc("id"));

			
			objFlightOpenCloseEntityList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getFltDesClsRdyToItuTraStatus() in  FlightOpenCloseRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objFlightOpenCloseEntityList;
	}

	
	
}
