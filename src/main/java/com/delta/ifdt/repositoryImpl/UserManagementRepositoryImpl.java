package com.delta.ifdt.repositoryImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserManagementRepository;




@Repository
@Transactional
public class UserManagementRepositoryImpl implements UserManagementRepository {

	static final Logger logger = LoggerFactory.getLogger(UserManagementRepositoryImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * This method will a check duplicateUser
	 * 
	 * @param userDetails
	 * @return boolean
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean duplicateUser(UserManagementModel userDetails) {
		boolean status = false;
		try {

			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(UserManagementEntity.class);
			Conjunction conjunction = Restrictions.conjunction();
			if (userDetails.getId() != null && userDetails.getId() != 0) {
				conjunction.add(Restrictions.ne("id", userDetails.getId()));
				Criterion userName = Restrictions.eq(Constants.USER_NAME, userDetails.getUserName());
				Criterion emailId = Restrictions.eq(Constants.EMAIL_ID, userDetails.getEmailId());
				LogicalExpression orExp = Restrictions.or(userName, emailId);
				conjunction.add(orExp);
			} else {
				Criterion userName = Restrictions.eq(Constants.USER_NAME, userDetails.getUserName());
				Criterion emailId = Restrictions.eq(Constants.EMAIL_ID, userDetails.getEmailId());
				LogicalExpression orExp = Restrictions.or(userName, emailId);
				conjunction.add(orExp);
			}
			criteria.add(conjunction);
			criteria.setProjection(Projections.rowCount());
			Long duplicatecount = (Long) criteria.uniqueResult();
			if (duplicatecount.intValue() > 0) {
				status = true;
			}
		} catch (Exception e) {
			
			logger.error(
					"Exception in UserDetailsRepositoryImpl.duplicateUser(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;
	}

	/**
	 * This method will createUser
	 * 
	 * @param userEntity
	 * @return boolean
	 */
	@Override
	public boolean createUser(UserManagementEntity userEntity) {
		boolean status = false;
		try {
			entityManager.merge(userEntity);
			status = true;
		} catch (Exception e) {
			logger.error(
					"Exception in UserManagementRepositoryImpl.createUser(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;
	}

	/**
	 * This method will getUserList
	 * 
	 * @return List<UserManagementModel>
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<UserManagementModel> getUserList() {
		List<UserManagementModel> userList = null;
		try {
			Criteria criteriaTotList = entityManager.unwrap(Session.class).createCriteria(UserManagementEntity.class);
			criteriaTotList.createAlias("userRoleEntity", "userRoleEntity");
			Projection projection = Projections.projectionList().add(Projections.property(Constants.USER_NAME), Constants.USER_NAME)
					.add(Projections.property("fullName"), "fullName").add(Projections.property("remarks"), "remarks")
					.add(Projections.property("status"), "status").add(Projections.property(Constants.EMAIL_ID), Constants.EMAIL_ID)
					.add(Projections.property("password"), "password").add(Projections.property("id"), "id")
					.add(Projections.property("userRoleEntity.id"), "roleId").add(Projections.property("userRoleEntity.role"), "role")
					.add(Projections.property(Constants.CREATION_DATE),Constants.CREATION_DATE);
			criteriaTotList.setProjection(projection);
			criteriaTotList.addOrder(Order.desc(Constants.CREATION_DATE));
			userList = criteriaTotList.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY)
					.setResultTransformer(new AliasToBeanResultTransformer(UserManagementModel.class)).list();
		} catch (Exception e) {
			logger.error(
					"Exception in UserDetailsRepositoryImpl.duplicateUser(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return userList;
	}

	/**
	 * This method will deleteUser
	 * 
	 * @param valueOf
	 * @return boolean
	 */
	@Override
	public boolean deleteUser(Integer valueOf) {
		boolean status = false;
		try {
			entityManager.remove(getUserById(valueOf));
			status = true;
		} catch (Exception e) {
			
			logger.error("Exception in UserDetailsRepositoryImpl.deleteUser(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;
	}

	private UserManagementEntity getUserById(Integer valueOf) {
		return entityManager.find(UserManagementEntity.class, valueOf);
	}

}
