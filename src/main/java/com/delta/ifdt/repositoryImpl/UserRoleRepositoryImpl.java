package com.delta.ifdt.repositoryImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.repository.UserRoleRepository;

@Repository
@Transactional
public class UserRoleRepositoryImpl implements UserRoleRepository {

	static final Logger logger = LoggerFactory.getLogger(UserRoleRepositoryImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * This method will getUserRoleList
	 * 
	 * @return List<UserRoleEntity>
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<UserRoleEntity> getUserRoleList() {
		List<UserRoleEntity> roleList = null;
		try {
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(UserRoleEntity.class);
			roleList = criteria.setResultTransformer(org.hibernate.Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.info("Exception in UserDetailsRepositoryImpl.getRoleList(): " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return roleList;
	}

}
