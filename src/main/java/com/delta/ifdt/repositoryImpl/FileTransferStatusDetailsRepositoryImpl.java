package com.delta.ifdt.repositoryImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;

@Repository
@Transactional
public class FileTransferStatusDetailsRepositoryImpl implements FileTransferStatusDetailsRepository {

	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;

	
	/**
	 * This method save FileTransferDetails
	 * 
	 * @param fileTransferStatusDetailsEntity
	 * @return boolean
	 */
	@Override
	@Transactional()
	public boolean saveFileTransferDetails(FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {
		logger.info(" FileTransferStatusDetailsRepositoryImpl.saveFileTransferDetails() start");

		boolean status = false;
		try {
            Thread.sleep(5000);
			entityManager.merge(fileTransferStatusDetailsEntity);
			status = true;
			logger.info("status saveFileTransferDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ status);
		} catch (Exception e) {
			logger.info("status inside catch () in  FileTransferStatusDetailsRepositoryImpl:"
					+ status);
			logger.error("Exception  saveFileTransferDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			logger.info("status inside Finally () in  FileTransferStatusDetailsRepositoryImpl:"
					+ status);
			entityManager.flush();
			entityManager.clear();
			logger.info("status inside after Finally () in  FileTransferStatusDetailsRepositoryImpl:"
					+ status);
		}
		logger.info(" FileTransferStatusDetailsRepositoryImpl.saveFileTransferDetails() Finished with status :" +status);
		return status;

	}

	
	
	/**
	 * This method save FileTransferStatusDetails
	 * 
	 * @param fileTransferStatusDetailsEntityfileTransferStatusDetailsEntity
	 * @return boolean
	 */
	@Override
	@Transactional()
	public FileTransferStatusDetailsEntity saveFileTransferStatusDetails(
			FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {

		logger.info(" FileTransferStatusDetailsRepositoryImpl.saveFileTransferStatusDetails()");
		try {
			 Thread.sleep(5000);
			fileTransferStatusDetailsEntity = entityManager.merge(fileTransferStatusDetailsEntity);
		} catch (Exception e) {
			logger.error("Exception  saveFileTransferStatusDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return fileTransferStatusDetailsEntity;

	}

	
	
	/**
	 * This method save ChunkFileDetails
	 * 
	 * @param objChunksDetailsEntity
	 * @return ChunksDetailsEntity
	 */
	@Override
	@Transactional()
	public ChunksDetailsEntity saveChunkFileDetails(ChunksDetailsEntity objChunksDetailsEntity) {
		logger.info(" FileTransferStatusDetailsRepositoryImpl.saveChunkFileDetails()");
		
		try {
			 Thread.sleep(5000);
			objChunksDetailsEntity = entityManager.merge(objChunksDetailsEntity);
			
		} catch (Exception e) {
			logger.error("Exception  saveChunkFileDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objChunksDetailsEntity;

	}

	
	
	
	/**
	 * This method gets FileTransferDetails
	 * 
	 * @param
	 * @return List
	 */
	@Override
	@Transactional()
	public List<FileTransferStatusDetailsEntity> getFileTransferDetails() {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetails()");

		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}

	
	
	
	/**
	 * This method gets FileTransferDetails_manual
	 * 
	 * @param
	 * @return FileTransferStatusDetailsEntity
	 */
	@Override
	public List<FileTransferStatusDetailsEntity> getFileTransferDetails_manual() {
		List<FileTransferStatusDetailsEntity> objList = null;

		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_manual()");
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetails_manual() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}

	
	/**
	 * This method gets FileTransferDetails_manual
	 * 
	 * @param
	 * @return FileTransferStatusDetailsEntity
	 */
	@Override
	public List<FileTransferStatusDetailsEntity> getFileTransferDetails_ItuCheck() {
		List<FileTransferStatusDetailsEntity> objList = null;

		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetails_manual()");
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
			criteria.add(Restrictions.eq("fileType", Constants.ITU_LOG));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetails_ItuCheck() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}
	
	/**
	 * This method gets ChunkFileTransferDetails
	 * 
	 * @param parentFileId
	 * @return List
	 */
	@Override
	public List<ChunksDetailsEntity> getChunkFileTransferDetails(int parentFileId) {
		List<ChunksDetailsEntity> objList = null;

		logger.info(" FileTransferStatusDetailsRepositoryImpl.getChunkFileTransferDetails() chunk parent ID"
				+ parentFileId);
		try {

			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ChunksDetailsEntity.class);
			criteria.add(Restrictions.eq("chunkStatus", Constants.READY_TO_TRANSMIT));
			criteria.add(Restrictions.eq("fileTransferStausDetailsEntityId", parentFileId));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getChunkFileTransferDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}

	
	
	/**
	 * This method will delete FileTransferDetails
	 * 
	 * @param reqID
	 * @return boolean
	 */
	@Override
	public boolean deleteFileTransferDetails(Integer chunkDetailsId) {
		logger.info(
				" FileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetails() chunkDetailsId" + chunkDetailsId);
		boolean status = false;
		try {

			entityManager.remove(getFileTransferStatusDetailsEntity(chunkDetailsId));
			status = true;
		}

		catch (Exception e) {
			logger.error("Exception  deleteFileTransferDetails() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}

	
	
	/**
	 * This method will return FileTransferStatusDetailsEntity
	 * 
	 * @param id
	 * @return FileTransferStatusDetailsEntity
	 */

	private ChunksDetailsEntity getFileTransferStatusDetailsEntity(Integer id) {
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferStatusDetailsEntity() ");
		ChunksDetailsEntity objEntity = null;
		try {
			objEntity = entityManager.find(ChunksDetailsEntity.class, id);
		} catch (Exception e) {
			
			logger.error("Exception  getFileTransferStatusDetailsEntity() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
			
		}

		return objEntity;

	}

	/**
	 * This method gets getFileTransferDetailsOffLoadStatus
	 * 
	 * @param
	 * @return List
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FileTransferStatusDetailsEntity> getFileTransferDetailsOffLoadStatus() {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus() ");

		try {

			@SuppressWarnings("deprecation")
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
			criteria.add(Restrictions.eq("modeOfTransfer", "OFFLOAD"));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetailsOffLoadStatus() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FileTransferStatusDetailsEntity> getFileTransferDetailsReceivedStatus() {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsOffLoadStatus() ");

		try {

			@SuppressWarnings("deprecation")
			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.RECEIVED));
			
			

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetailsOffLoadStatus() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}
	
	

	/**
	 * This method gets getFileTransferDetailsRestStatus
	 * 
	 * @param
	 * @return List
	 */
	@Override
	public boolean getFileTransferDetailsRestStatus(FlightOpenCloseEntity objLocFlightOpenCloseEntity) {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus() ");

		boolean status = false;
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.ACKNOWLEDGED));
			criteria.add(Restrictions.eq("openCloseDetilsId", objLocFlightOpenCloseEntity.getId()));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			if (objList == null || objList.size() == 4) {
				status = true;

			}
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetailsRestStatus() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}
	
	
	/**
	 * This method gets getFileTransferDetailsRestStatus
	 * 
	 * @param
	 * @return List
	 */
	@Override
	public boolean getFileTransferDetailsRestStatusRecived(FlightOpenCloseEntity objLocFlightOpenCloseEntity) {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFileTransferDetailsRestStatus() ");

		boolean status = false;
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.READY_TO_TRANSMIT));
			criteria.add(Restrictions.eq("openCloseDetilsId", objLocFlightOpenCloseEntity.getId()));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			if (objList == null || objList.size() == 0) {
				status = true;

			}
		} catch (Exception e) {
			logger.error("Exception  getFileTransferDetailsRestStatus() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}

	/**
	 * This method gets getFileTransferDetailsRestStatus
	 * 
	 * @param
	 * @return List
	 */
	@Override
	public boolean getFleTraferDetRestStatusCheck(FlightOpenCloseEntity objLocFlightOpenCloseEntity) {
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.getFleTraferDetRestStatusCheck() ");

		boolean status = false;
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq(Constants.STATUS, Constants.ACKNOWLEDGED));
			criteria.add(Restrictions.eq("openCloseDetilsId", objLocFlightOpenCloseEntity.getId()));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			if (objList != null && objList.size() == 4) {
				status = true;

			}
		} catch (Exception e) {
			logger.error("Exception  getFleTraferDetRestStatusCheck() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}
	
	/**
	 * This method will remove ChunkFileTransferDetails_manual
	 * 
	 * @param parentFileId
	 * @return ChunksDetailsEntity
	 */
	@Override
	public List<ChunksDetailsEntity> removeChunkFileTransferDetails_manual(Integer parentFileId) {

		List<ChunksDetailsEntity> objList = null;
		logger.info(" FileTransferStatusDetailsRepositoryImpl.removeChunkFileTransferDetails_manual() ");
		try {

			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(ChunksDetailsEntity.class);
			
			criteria.add(Restrictions.eq("fileTransferStausDetailsEntityId", parentFileId));

			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		} catch (Exception e) {
			logger.error("Exception  removeChunkFileTransferDetails_manual() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}

		return objList;
	}
	/**
	 * This method will delete FileTransferDetails
	 * 
	 * @param reqID
	 * @return boolean
	 */
	@Override
	public boolean deleteFileTransferDetailsChunks(Integer parentId) {
		logger.info(
				" FileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetailsChunks() parentId" + parentId);
		boolean status = false;
		try {

			entityManager.createQuery("DELETE FROM ChunksDetailsEntity c where c.fileTransferStausDetailsEntityId="+parentId).executeUpdate();
			status = true;
		}

		catch (Exception e) {
			logger.error("Exception  deleteFileTransferDetailsChunks() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}
	/**
	 * This method will updateFileTransferDetailsChunks
	 * 
	 * @param reqID
	 * @return boolean
	 */
	@Override
	public boolean updateFileTransferDetailsChunks(Integer parentId) {
		logger.info(
				" FileTransferStatusDetailsRepositoryImpl.deleteFileTransferDetailsChunks() parentId" + parentId);
		boolean status = false;
		try {

			entityManager.createQuery("update ChunksDetailsEntity c set c.chunkStatus='"+Constants.ACKNOWLEDGED+"' where c.fileTransferStausDetailsEntityId="+parentId).executeUpdate();
			status = true;
		}

		catch (Exception e) {
			logger.error("Exception  updateFileTransferDetailsChunks() in  FileTransferStatusDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return status;

	}
	@Override
	public RestTemplate createRestTemplate(HttpComponentsClientHttpRequestFactory requestFactory) {
	    return new RestTemplate(requestFactory);
	}

}