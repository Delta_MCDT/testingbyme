package com.delta.ifdt.repositoryImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.util.DateUtil;


@Repository
@Transactional
public class DashBoardDetailsRepositoryImpl implements DashBoardDetailsRepository {
	
	static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsRepositoryImpl.class);
	@PersistenceContext
	private EntityManager entityManager;

	
	
	
	/**
	 * This method will return Dash Board Details 
	 * 
	 * @param barchartDataModel
	 * @return FileTransferStatusDetailsEntity
	 */
	@Override
	public List<FileTransferStatusDetailsEntity> getDetailsOfDashBoard(String fromDate, String toDate) {
		
		List<FileTransferStatusDetailsEntity> objList = null;
		logger.info("DashBoardDetailsRepositoryImpl.getDetailsOfDashBoard() start");
		try {

			Criteria criteria = entityManager.unwrap(Session.class)
					.createCriteria(FileTransferStatusDetailsEntity.class);
			Conjunction conjunction = Restrictions.conjunction();
			Criterion eventstartDate =Restrictions.ge("tarFileDate",
					DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
			Criterion eventEndDate =Restrictions.le("tarFileDate",
					DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
			conjunction.add(eventstartDate);
			conjunction.add(eventEndDate);
			criteria.add(conjunction);
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			
		} catch (Exception e) {
			logger.error("Exception  getDetailsOfDashBoard() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objList;

	}
	
	
	/**
	 * This method will getGraphDetails
	 * 
	 * @param type
	 * @return Map<String, Object>
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public Map<String, Object> getGraphDetails(String type,int page,int count, String fromDate, String toDate) {
		List<FileTransferStatusDetailsEntity> objList =null;
		Map<String, Object> objMap = new HashMap<>();
		double result = 0;
		int pagecount = 0;
		try {
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq("modeOfTransfer",type));
			criteria.add(Restrictions.eq("status",Constants.ACKNOWLEDGED));
			if(!"".equals(fromDate) && (!"".equals(toDate)))
			{
				Conjunction conjunction = Restrictions.conjunction();
				Criterion eventstartDate =Restrictions.ge("tarFileDate",
						DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
				Criterion eventEndDate =Restrictions.le("tarFileDate",
						DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
				conjunction.add(eventstartDate);
				conjunction.add(eventEndDate);
				criteria.add(conjunction);
			}
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteriaCount.setProjection(Projections.rowCount());
			Long totCount = (Long) criteriaCount.uniqueResult();
			double size = totCount;
			result = Math.ceil(size / count);
			pagecount = (int) result;
			objMap.put(Constants.page_count, pagecount);
			objMap.put("graphDetails",objList);
		} catch (Exception e) {
			logger.error("Exception  getGraphDetails() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objMap;
	}


	
	/**
	 * This method will return File Graph Details
	 * 
	 * @param type
	 * @param page
	 * @param count
	 * @return Map<String, Object>
	 */
	@Override
	public Map<String, Object> getFileGraphDetails(String type,int page,int count, String fromDate, String toDate) {
		List<FileTransferStatusDetailsEntity> objList =null;
		Map<String, Object> objMap = new HashMap<>();
		double result = 0;
		int pagecount = 0;
		try {
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq("fileType",type));
			criteria.add(Restrictions.eq("status",Constants.ACKNOWLEDGED));
			if(!"".equals(fromDate) && (!"".equals(toDate)))
			{
				Conjunction conjunction = Restrictions.conjunction();
				Criterion eventstartDate =Restrictions.ge("tarFileDate",
						DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
				Criterion eventEndDate =Restrictions.le("tarFileDate",
						DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
				conjunction.add(eventstartDate);
				conjunction.add(eventEndDate);
				criteria.add(conjunction);
			}
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteriaCount.setProjection(Projections.rowCount());
			Long totCount = (Long) criteriaCount.uniqueResult();
			double size = totCount;
			result = Math.ceil(size / count);
			pagecount = (int) result;
			objMap.put(Constants.page_count, pagecount);
			objMap.put("fileGraphDetails",objList);
		} catch (Exception e) {
			logger.error("Exception  getFileGraphDetails() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objMap;
	}


	/**
	 * This method will return Statistics Details
	 * 
	 * @param page
	 * @param count
	 * @return Map<String, Object>
	 */
	@Override
	public Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page,int count) {
		List<FileTransferStatusDetailsEntity> objList = null;
		Map<String, Object> mapDetails =new HashMap<>();
		double result = 0;
		int pagecount = 0;
		try {
			
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			if(!"".equals(fromDate) && (!"".equals(toDate)))
			{
			Conjunction conjunction = Restrictions.conjunction();
			Criterion eventstartDate =Restrictions.ge("tarFileDate",
					DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
			Criterion eventEndDate =Restrictions.le("tarFileDate",
					DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
			conjunction.add(eventstartDate);
			conjunction.add(eventEndDate);
			criteria.add(conjunction);
			}
			criteria.setFirstResult((page - 1) * count);
			criteria.setMaxResults(count);
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteriaCount.setProjection(Projections.rowCount());
			Long totCount = (Long) criteriaCount.uniqueResult();
			double size = totCount;
			result = Math.ceil(size / count);
			pagecount = (int) result;
			mapDetails.put("statisticDetails", objList);
			mapDetails.put(Constants.page_count, pagecount);
		} catch (Exception e) {
			logger.error("Exception  getStatisticsDetails() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return mapDetails;
			
	}


	
	/**
	 * This method will return Failure Reason Details
	 * 
	 * @param type
	 * @param page
	 * @param count
	 * @return Map<String, Object>
	 */
	@Override
	public Map<String, Object> getFailureReasonDetails(String type,int page,int count, String fromDate, String toDate) {
		List<FileTransferStatusDetailsEntity> objList =null;
		Map<String, Object> objMap = new HashMap<>();
		double result = 0;
		int pagecount = 0;
		try {
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.add(Restrictions.eq("failureReasonForOffLoad",type));
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			if(!"".equals(fromDate) && (!"".equals(toDate)))
			{
				Conjunction conjunction = Restrictions.conjunction();
				Criterion eventstartDate =Restrictions.ge("tarFileDate",
						DateUtil.stringToDate(fromDate, Constants.MM_DD_YYYY));
				Criterion eventEndDate =Restrictions.le("tarFileDate",
						DateUtil.stringToDateEndTime(toDate, Constants.MM_DD_YYYY));
				conjunction.add(eventstartDate);
				conjunction.add(eventEndDate);
				criteria.add(conjunction);
			}
			Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteriaCount.setProjection(Projections.rowCount());
			Long totCount = (Long) criteriaCount.uniqueResult();
			double size = totCount;
			result = Math.ceil(size / count);
			pagecount = (int) result;
			objMap.put(Constants.page_count, pagecount);
			objMap.put("failureReasonDetails",objList);
		} catch (Exception e) {
			logger.error("Exception  getFailureReasonDetails() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return objMap;
	}


	
	//need to check the time stamp
	
	/**
	 * This method will return Statistics Details
	 * 
	 * @param fileTransferStatusDetailsModel
	 * @return Map<String, Object>
	 */
	@Override
	public Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel,
			int page, int count) {
		List<FileTransferStatusDetailsEntity> objList = null;
		Map<String, Object> mapDetails =new HashMap<>();
		double result = 0;
		int pagecount = 0;
		try {			
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteria.setFirstResult((page - 1) * count);
			criteria.setMaxResults(count);
			objList = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			Criteria criteriaCount = entityManager.unwrap(Session.class).createCriteria(FileTransferStatusDetailsEntity.class);
			criteriaCount.setProjection(Projections.rowCount());
			Long totCount = (Long) criteriaCount.uniqueResult();
			double size = totCount;
			result = Math.ceil(size / count);
			pagecount = (int) result;
			mapDetails.put("statisticDetails", objList);
			mapDetails.put(Constants.page_count, pagecount);
		} catch (Exception e) {
			logger.error("Exception  getStatisticsDetails() in  DashBoardDetailsRepositoryImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return mapDetails;
	}

}
