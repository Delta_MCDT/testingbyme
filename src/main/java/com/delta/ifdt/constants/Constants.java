package com.delta.ifdt.constants;

public class Constants {

	public static final String ACTIVE = "Active";
	public static final String FAIL = "FAILED";
	public static final String SUCCESS = "SUCCESS";
	public static final String GMT = "GMT";
	public static final String LOAD = "load";
	public static final String SEARCH = "search";

	public static final String YYYY_MM_DD_HH_MM = "yyyyMMddHHmm";
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String DD_MM_YYYY = "dd-MM-yyyy";
	public static final String MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DD_MM_YYYY_= "dd/MM/yyyy";
	public static final String MM_DD_YY = "MMddyy";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YYYY_MM_DD_SLASH = "yyyy/MM/dd";
	public static final String MM_DD_YYYY_HH_MM = "MM/dd/yyyy HH:mm";

	/* File names to fetch */
	public static final String BITE_LOG = "BITE";
	public static final String AXINOM_LOG = "AXINOM";
	public static final String SYSTEM_LOG = "SLOG";
	public static final String USAGE_LOG = "USAGE";
	public static final String ITU_LOG = "ITU";
	public static final String LOG_EXTENSTION = "log.gz";
	
	
	public static final String DO = "_DO_";
	public static final String FSS = "_FSS_";
	public static final String ISD = "_ISD_";

	/* File names to fetch */
	public static final String READY_TO_TRANSMIT = "READY_TO_TRANSMIT";
	public static final String WAITING_FOR_ACKNOWLEDGEMENT = "WAITING_FOR_ACKNOWLEDGEMENT";
	public static final String ACKNOWLEDGED = "ACKNOWLEDGED";
	public static final String DELETED = "DELETED";
	public static final String RECEIVED = "RECEIVED";
	public static final Integer CHECKSUM_COUNT = 1;

	public static final String COMMA = ",";

	// Flight open close details
	public static final String FLIGHT_OPEN = "FLIGHT_OPENED";
	public static final String FLIGHT_CLOSE = "FLIGHT_CLOSED";

	public static final String WAY_OF_TRANSPER_OFFLOAD = "OFFLOAD";
	public static final String WAY_OF_TRANSPER_REST = "REST";

	public static final String OFFLOAD_LOCAL_PATH = "/home/user/offload/";
	public static final String OFFLOAD_LOCAL_PENDRIVE_PATH = "/media/user/surya";
	public static final String OFFLOAD_TYPE_AUTO = "AUTO";
	public static final String OFFLOAD_TYPE_MANUAL = "MANUAL";
	public static final String METADATA_FILE = "metadata.json";

	public static final String EXTRACTION_FAILURE = "Extraction_failure";
	public static final String CHECKSUM_FAILURE = "Checksum_failure";
	public static final String FILE_NOT_RECEIVED = "File_Not_Received";

	public static final String FLIGHT_NUMBER = "1234";

	// Application Properties
	public static final String BITE_FILE_PATH_IFE = "BiteFile_Src";
	public static final String AXINOM_FILE_PATH_IFE = "AxinomFile_Src";
	public static final String SYSTEM_LOG_FILE_PATH_IFE = "SystemLogFile_Src";
	public static final String BITE_FILE_PATH_DESTINATION = "BiteFile_Dest";
	public static final String AXINOM_FILE_PATH_IFE_DESTINATION = "AxinomFile_Dest";
	public static final String SYSTEM_LOG_FILE_PATH_IFE_DESTINATION = "SystemLog_Dest";
	public static final String GROUND_SERVER_CONNECT_CHECK_COUNT = "GroundCount";
	public static final String SIZE_LIMIT_OF_TAR_FILE = "TarFileSizelimit";
	public static final String CHUNK_SIZE_OF_EACH_CHUNK = "ChunkSize";

	// controllers
	public static final String SESSION_ID = "sessionId";
	public static final String SERVICE_TOKEN = "serviceToken";
	public static final String BITEFILE_PATH_IFE = "BITE_FILE_PATH_IFE";
	public static final String AXINOMFILE_PATH_IFE = "AXINOM_FILE_PATH_IFE";
	public static final String SYSTEMLOG_FILE_PATH_IFE = "SYSTEM_LOG_FILE_PATH_IFE";
	public static final String BITEFILE_PATH_DESTINATION = "BITE_FILE_PATH_DESTINATION";
	public static final String AXINOMFILE_PATH_IFE_DESTINATION = "AXINOM_FILE_PATH_IFE_DESTINATION";
	public static final String SYSTEMLOG_FILE_PATH_IFE_DESTINATION = "SYSTEM_LOG_FILE_PATH_IFE_DESTINATION";
	public static final String GROUNDSERVER_CONNECT_CHECK_COUNT = "GROUND_SERVER_CONNECT_CHECK_COUNT";
	public static final String PROPERTIES_PATH_DETAILS = "PropertiesPathDetails";
	public static final String LABEL_DETAILS = "labelDetails";
	public static final String PARAMETER_TYPE = "parameterType";
	public static final String STATUS = "status";

	public static final String PAGINATION = "pagination";
	public static final String FILE_GRAPH_DETAILS_LIST = "fileGraphDetailsList";
	public static final String COUNT = "count";
	public static final String STATISTIC_DETAILS = "statisticDetails";
	public static final String PAGE_COUNT = "PageCount";
	public static final String page_count = "pagecount";
	public static final String USER_NAME = "userName";
	public static final String VALID_USER = "validUser";

	// repositoryImpl
	public static final String CLOSE_OPEN_STATUS = "closeOpenStatus";
	public static final String EMAIL_ID = "emailId";

	public static final String CREATION_DATE = "creationDate";

	public static final String SUCCESS_STATUS_CODE = "2001";

	public static final String UTF_8 = "UTF-8";
	
	
	// ServiceImpl
	public static final String TOTAL_PARTS = "total_parts";
	public static final String OOFLOAD_TYPE = "offload_type";
	public static final String FILE_CHECKSUM = "file_checksum";
	public static final String CURRENT_PART = "current_part";
	public static final String CHUNK_CHECKSUM = "chunk_checksum";

}
