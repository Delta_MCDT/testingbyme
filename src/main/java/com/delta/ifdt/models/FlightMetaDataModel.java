package com.delta.ifdt.models;

public class FlightMetaDataModel {
	
	private String airline;
	private String aircrafttype;
	private String aircraftsubtype;
	private String flightnumber;
	private String tailnumber;
	private String departureairport;
	private String arrivalairport;
	private String departuretime;
	private String arrivaltime;
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getAircrafttype() {
		return aircrafttype;
	}
	public void setAircrafttype(String aircrafttype) {
		this.aircrafttype = aircrafttype;
	}
	public String getAircraftsubtype() {
		return aircraftsubtype;
	}
	public void setAircraftsubtype(String aircraftsubtype) {
		this.aircraftsubtype = aircraftsubtype;
	}
	public String getFlightnumber() {
		return flightnumber;
	}
	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}
	public String getTailnumber() {
		return tailnumber;
	}
	public void setTailnumber(String tailnumber) {
		this.tailnumber = tailnumber;
	}
	public String getDepartureairport() {
		return departureairport;
	}
	public void setDepartureairport(String departureairport) {
		this.departureairport = departureairport;
	}
	public String getArrivalairport() {
		return arrivalairport;
	}
	public void setArrivalairport(String arrivalairport) {
		this.arrivalairport = arrivalairport;
	}
	public String getDeparturetime() {
		return departuretime;
	}
	public void setDeparturetime(String departuretime) {
		this.departuretime = departuretime;
	}
	public String getArrivaltime() {
		return arrivaltime;
	}
	public void setArrivaltime(String arrivaltime) {
		this.arrivaltime = arrivaltime;
	}
	
	
	
	
	
	

}
