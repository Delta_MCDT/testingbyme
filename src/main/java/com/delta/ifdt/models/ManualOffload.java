package com.delta.ifdt.models;

public class ManualOffload {
	private String[] files;

	public String[] getFiles() {
		return files;
	}

	public void setFiles(String[] files) {
		this.files = files;
	}	
}
