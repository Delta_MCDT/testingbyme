package com.delta.ifdt.models;

public class StatusQueryModel {
	
	private String ChecksumStatus;
	private String ExtractionStatus;
	public String getChecksumStatus() {
		return ChecksumStatus;
	}
	public void setChecksumStatus(String checksumStatus) {
		ChecksumStatus = checksumStatus;
	}
	public String getExtractionStatus() {
		return ExtractionStatus;
	}
	public void setExtractionStatus(String extractionStatus) {
		ExtractionStatus = extractionStatus;
	}
	
	

}
