package com.delta.ifdt.models;

public class PichartRestOffloadModel {
	
	private long restTransferDatacount;
	private long manvalTransferDatacount;
	private long totTransferDatacount;
	private float percentagerestData;
	private float percentageoffloadData;
	public long getRestTransferDatacount() {
		return restTransferDatacount;
	}
	public void setRestTransferDatacount(long restTransferDatacount) {
		this.restTransferDatacount = restTransferDatacount;
	}
	public long getManvalTransferDatacount() {
		return manvalTransferDatacount;
	}
	public void setManvalTransferDatacount(long manvalTransferDatacount) {
		this.manvalTransferDatacount = manvalTransferDatacount;
	}
	public long getTotTransferDatacount() {
		return totTransferDatacount;
	}
	public void setTotTransferDatacount(long totTransferDatacount) {
		this.totTransferDatacount = totTransferDatacount;
	}
	public float getPercentagerestData() {
		return percentagerestData;
	}
	public void setPercentagerestData(float percentagerestData) {
		this.percentagerestData = percentagerestData;
	}
	public float getPercentageoffloadData() {
		return percentageoffloadData;
	}
	public void setPercentageoffloadData(float percentageoffloadData) {
		this.percentageoffloadData = percentageoffloadData;
	}
	
	

}
