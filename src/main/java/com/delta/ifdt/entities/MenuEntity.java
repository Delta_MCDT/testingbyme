package com.delta.ifdt.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "MAIN_MENU_DETAILS")
public class MenuEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@OneToMany(targetEntity = SubMenuEntity.class, mappedBy = "menuEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<SubMenuEntity> subMenuItems;
	
	@Column(name = "ROUTE_NAME", nullable = false)
	private String route;
	
	@Column(name = "DISPALY_TEXT", nullable = false)
	private String displayText;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public List<SubMenuEntity> getSubMenuItems() {
		return subMenuItems;
	}

	public void setSubMenuItems(List<SubMenuEntity> subMenuItems) {
		this.subMenuItems = subMenuItems;
	}


	
}
