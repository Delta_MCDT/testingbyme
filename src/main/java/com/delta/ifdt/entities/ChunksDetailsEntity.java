package com.delta.ifdt.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "CHUNK_DETAILS")
	
public class ChunksDetailsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="FILE_TRANSFER_STATUS_ID")
	private Integer fileTransferStausDetailsEntityId;
	
	@Column(name = "CHUNK_TAR_FILE_PATH")
	private String chunkTarFilePath;
	
	@Column(name = "CHUNK_STATUS")
	private String chunkStatus;
	
	@Column(name = "CHUNK_COUNT")
	private Integer chunkCount;
	
	@Column(name = "CHUNK_NAME")
	private String  chunkName;
	
	@Column(name = "CHUNK_CHECKSUM")
	private String chunk_Checksum;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFileTransferStausDetailsEntityId() {
		return fileTransferStausDetailsEntityId;
	}

	public void setFileTransferStausDetailsEntityId(Integer fileTransferStausDetailsEntityId) {
		this.fileTransferStausDetailsEntityId = fileTransferStausDetailsEntityId;
	}

	public String getChunkTarFilePath() {
		return chunkTarFilePath;
	}

	public void setChunkTarFilePath(String chunkTarFilePath) {
		this.chunkTarFilePath = chunkTarFilePath;
	}

	public String getChunkStatus() {
		return chunkStatus;
	}

	public void setChunkStatus(String chunkStatus) {
		this.chunkStatus = chunkStatus;
	}

	public Integer getChunkCount() {
		return chunkCount;
	}

	public void setChunkCount(Integer chunkCount) {
		this.chunkCount = chunkCount;
	}

	public String getChunkName() {
		return chunkName;
	}

	public void setChunkName(String chunkName) {
		this.chunkName = chunkName;
	}

	public String getChunk_Checksum() {
		return chunk_Checksum;
	}

	public void setChunk_Checksum(String chunk_Checksum) {
		this.chunk_Checksum = chunk_Checksum;
	}

	
	


}