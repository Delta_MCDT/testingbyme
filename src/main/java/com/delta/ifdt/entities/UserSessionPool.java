package com.delta.ifdt.entities;

import java.util.HashMap;
import java.util.Map;

import com.delta.ifdt.models.User;

	public class UserSessionPool {

		private static UserSessionPool instance = null;


		private Map<String, User> sessionMap = new HashMap<>();

		private UserSessionPool() {
		}

		public static UserSessionPool getInstance() {
			if (instance == null) {
				synchronized (UserSessionPool.class) {
					instance = new UserSessionPool();
				}
			}
			return instance;
		}

		public void addUser(User user) {
			sessionMap.put(user.getTokenKey(), user);
		}

		public void removeUser(User user) {
			sessionMap.remove(user.getTokenKey());
		}

		public void removeUser(String sessionToken) {
			sessionMap.remove(sessionToken);
		}

		public boolean isUserInSession(User user) {
			return sessionMap.containsKey(user.getTokenKey());
		}

		public User getSessionUser(String key) {
			return sessionMap.get(key);
		}
	}


