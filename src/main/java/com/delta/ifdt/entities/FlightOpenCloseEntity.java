package com.delta.ifdt.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FLIGHT_OPEN_CLOSE_DETAILS")
public class FlightOpenCloseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	private Date   flightOpenTime;
	private Date   flightCloseTime;
	private String airLineName;
	private String aircraftType;
	private String flightNumber;
	private String tailNumber;
	private String departureAirport;
	private String arrivalAirport;
	private Date   departureTime;
	private Date   arrivalTime;
	private Date   offloadGenTime;
	private String transferStatus;
	private String closeOpenStatus;
	private String wayOfTransfer;
	
	private Date   nextFlightCloseTime;
	private String   ituFolderName;
	private String   ituFileStatus;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getFlightOpenTime() {
		return flightOpenTime;
	}
	public void setFlightOpenTime(Date flightOpenTime) {
		this.flightOpenTime = flightOpenTime;
	}
	public Date getFlightCloseTime() {
		return flightCloseTime;
	}
	public void setFlightCloseTime(Date flightCloseTime) {
		this.flightCloseTime = flightCloseTime;
	}
	public String getAirLineName() {
		return airLineName;
	}
	public void setAirLineName(String airLineName) {
		this.airLineName = airLineName;
	}
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getTailNumber() {
		return tailNumber;
	}
	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public Date getOffloadGenTime() {
		return offloadGenTime;
	}
	public void setOffloadGenTime(Date offloadGenTime) {
		this.offloadGenTime = offloadGenTime;
	}
	
	public String getCloseOpenStatus() {
		return closeOpenStatus;
	}
	public void setCloseOpenStatus(String closeOpenStatus) {
		this.closeOpenStatus = closeOpenStatus;
	}
	public String getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}
	public String getWayOfTransfer() {
		return wayOfTransfer;
	}
	public void setWayOfTransfer(String wayOfTransfer) {
		this.wayOfTransfer = wayOfTransfer;
	}
	public Date getNextFlightCloseTime() {
		return nextFlightCloseTime;
	}
	public void setNextFlightCloseTime(Date nextFlightCloseTime) {
		this.nextFlightCloseTime = nextFlightCloseTime;
	}
	public String getItuFolderName() {
		return ituFolderName;
	}
	public void setItuFolderName(String ituFolderName) {
		this.ituFolderName = ituFolderName;
	}
	public String getItuFileStatus() {
		return ituFileStatus;
	}
	public void setItuFileStatus(String ituFileStatus) {
		this.ituFileStatus = ituFileStatus;
	}

	
	
}
