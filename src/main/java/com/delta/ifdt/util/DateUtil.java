package com.delta.ifdt.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.delta.ifdt.constants.Constants;




@Component
public class DateUtil {

	static final  Logger logger = LoggerFactory.getLogger(DateUtil.class);

	/**
	 * This method converts String format of Date to Date format.
	 * 
	 * @param pStrDate
	 * @param dateFormat
	 * @return Date format of passed string date for valid date format of string
	 *         and dateformat else it returns null.
	 * @throws ParseException
	 */
	public static Date stringToDate(String dateString, String dateFormat) {
		Date date = null;
		try {
			if (dateString != null && !"".equals(dateString)) {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				date = sdf.parse(dateString);
			}
		} catch (Exception e) {
			logger.error("Exception stringToDate() in  DateUtil" + e.getStackTrace());
		}
		return date;
	}
	
	
	
	
	/**
	 * This method will convert String to DateEndTime
	 * 
	 * 
	 * @param dateString, dateFormat
	 * @return Date
	 */
	public static Date stringToDateEndTime(String dateString, String dateFormat) {
		Date date = null;
		try {
			if (dateString != null && !"".equals(dateString)) {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				date = sdf.parse(dateString);
				
				Calendar calendar = Calendar.getInstance();
			    calendar.setTime(date);
			    calendar.add(Calendar.HOUR, 23);
			    calendar.add(Calendar.MINUTE, 59);
			    calendar.add(Calendar.SECOND, 59);
			    date=calendar.getTime();
			}
		} catch (Exception e) {
			logger.error("Exception stringToDateEndTime() in  DateUtil" + e.getStackTrace());
		}
		return date;
	}


	
	
	/**
	 * This method converts Date to String format of Date.
	 * 
	 * @param date
	 * @param dateFormat
	 * @return string format of passed date for valid input of date and
	 *         dateformat else it returns an empty string.
	 */
	public static String dateToString(Date date, String dateFormat) {
		String dateString = null;
		try {
			if (date != null && !"".equals(date)) {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				dateString = sdf.format(date);
			}
		} catch (Exception e) {
			
			logger.error("Exception dateToString() in  DateUtil" + e.getStackTrace());
		}
		return dateString;
	}

	
	
	/**
	 * return data in string format
	 * 
	 * @param dateString
	 * @param dateStrFormat
	 * @param expectedDateStrFormat
	 * @return
	 */
	public static String getDateStringInFormat(String dateString, String dateStrFormat, String expectedDateStrFormat) {
		Date date = null;
		String dateStr = null;
		try {
			if (dateString != null && !"".equals(dateString)) {
				SimpleDateFormat sdf = new SimpleDateFormat(dateStrFormat);
				date = sdf.parse(dateString);
				dateStr = dateToString(date, expectedDateStrFormat);
			}
		} catch (Exception e) {
			logger.error("Exception getDateStringInFormat() in  DateUtil" + e.getStackTrace());
		}
		return dateStr;
	}
	
	/**
	 * This Method returns GMT time
	 * 
	 * @return Date
	 */
	public static Date getDate(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss zzz");

        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        return new Date(sdf.format(date));

       }      

}
