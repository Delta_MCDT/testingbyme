package com.delta.ifdt.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component
public class CommonUtil {

	static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);

	/**
	 * 
	 * return json object
	 * 
	 * @param jsonString
	 * @return
	 */
	public static JsonObject parseRequestDataToJson(String jsonString) {
		logger.info("CommonUtil.parseRequestDataToJson() start");
		JsonParser parser = new JsonParser();
		return parser.parse(jsonString.trim()).getAsJsonObject();
	}

	/**
	 * 
	 * return json object
	 * 
	 * @param jsonString
	 * @return
	 * @throws ParseException
	 */
	public static JSONObject parseDataToJSON(String jsonString) throws ParseException {
		logger.info("CommonUtil.parseDataToJSON() start");
		JSONParser parser = new JSONParser();

		return (JSONObject) parser.parse(jsonString);

	}

	/**
	 * Config to json object
	 * 
	 * @param config
	 * @return
	 */
	public static String convertObjectToJson(Object config) {
		String requestBean = null;
		logger.info("CommonUtil.convertObjectToJson() start");
		try {
			ObjectMapper mapper = new ObjectMapper();
			requestBean = mapper.writeValueAsString(config);
		} catch (Exception e) {
			logger.error("Exception for convertObjectToJson() in CommonUtil  :" + ExceptionUtils.getFullStackTrace(e));
		}

		return requestBean;
	}

	/**
	 * This method is to construct the response object to UI(Success or Error
	 * 
	 * 
	 * @param response
	 * @return JSONObject
	 */
	public static JSONObject buildResponseJson(String status, String response, String sessionId, String serviceToken) {

		Map<String, String> mapObject = new HashMap<>();
		JSONObject jsonObject = null;
		logger.info("CommonUtil.buildResponseJson() start");
		try {
			mapObject.put("ram", status);
			mapObject.put("status", status);
			mapObject.put("reason", response);
			mapObject.put("sessionId", sessionId);
			mapObject.put("serviceToken", serviceToken);
			jsonObject = new JSONObject(mapObject);

		} catch (Exception e) {

			logger.error("Exception for buildResponseJson() in CommonUtil  :" + ExceptionUtils.getFullStackTrace(e));
		}
		return jsonObject;
	}

	/**
	 * This method is to construct the response object to UI(Success or Error
	 * 
	 * @param response
	 * @return String
	 */
	public static String getHomeDirectory() {
		String homeDirectory = null;
		logger.info("CommonUtil.getHomeDirectory() start");
		try {
			homeDirectory = System.getProperty("user.home") + "/";
		} catch (Exception e) {
			logger.error("Exception homeDirectory() in  CommonUtil" + e.getStackTrace());
		}
		return homeDirectory;
	}

	/**
	 * This method is check object valid or not
	 * 
	 * @param list
	 * @return boolean
	 */
	public static boolean isValidObject(Object list) {
		boolean status = false;
		logger.info("CommonUtil.isValidObject() start");
		try {
			if (list != null) {
				status = true;
			}
		} catch (Exception e) {
			logger.error("Exception isValidObject() in  CommonUtil" + e.getStackTrace());
		}
		return status;
	}

	/**
	 * This method is used to convert date to String
	 * 
	 * 
	 * @param date,
	 *            dateFormat
	 * @return String
	 */
	public static String dateToString(Date date, String dateFormat) {
		String dateString = null;
		logger.info("CommonUtil.dateToString() start");
		try {
			if (date != null && !"".equals(date)) {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				dateString = sdf.format(date);
			}
		} catch (Exception e) {
			logger.error("Exception stringToDate ::::" + ExceptionUtils.getFullStackTrace(e));
		}
		return dateString;
	}

	public static String toGetCurrentDir() {
		String togetDir = null;
		logger.info("CommonUtil.toGetCurrentDir() start");
		try {

			togetDir = System.getProperty("user.dir");

		} catch (Exception e) {
			logger.error("Exception toGetCurrentDir ::::" + ExceptionUtils.getFullStackTrace(e));
		}
		return togetDir;
	}

	/**
	 * This method is used to create zip File
	 * 
	 * 
	 * @param zipFilePathBuilder,
	 *            filePath
	 * @return boolean
	 */
	public static boolean createZipFile(String zipFilePathBuilder, String filePath) {
		boolean status = false;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		FileInputStream fis = null;
		logger.info("CommonUtil.createZipFile() start");
		try {
			byte[] buffer = new byte[1024];
			fos = new FileOutputStream(zipFilePathBuilder);
			zos = new ZipOutputStream(fos);
			File dataFile = new File(filePath);
			fis = new FileInputStream(dataFile);
			zos.putNextEntry(new ZipEntry(dataFile.getName()));
			int length;
			while ((length = fis.read(buffer)) > 0) {
				zos.write(buffer, 0, length);
			}
			if (dataFile.exists()) {
				dataFile.delete();

			}
			zos.closeEntry();
			status = true;
		} catch (Exception e) {
			logger.error("Failed to create zip file", e);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				if (zos != null) {
					zos.close();
				}
				if (fos != null) {
					fos.close();
				}

			} catch (Exception e) {
				logger.error("Failed to Finally block to zip file", e);
			}
		}
		return status;
	}

	/**
	 * This method is used to create zip File Directory
	 * 
	 * 
	 * @param destinationZipFile,
	 *            srcFilePath
	 * @return boolean
	 */
	public static boolean createZipFileOfDirectory(String destinationZipFile, String srcFilePath) {

		boolean status = false;
		logger.info("CommonUtil.createZipFileOfDirectory() start");
		try {
			byte[] buffer = new byte[1024];
			FileOutputStream fos = new FileOutputStream(destinationZipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			File dir = new File(srcFilePath);
			File[] files = dir.listFiles();
			for (int i = 0; i < files.length; i++) {
				FileInputStream fis = new FileInputStream(files[i]);
				zos.putNextEntry(new ZipEntry(files[i].getName()));
				int length;
				while ((length = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}

				zos.closeEntry();
				fis.close();
			}
			zos.close();
			status = true;
		} catch (Exception e) {
			logger.error("Failed to create zip file", e);
		}
		return status;
	}

	/**
	 * This method will return Left String with given length
	 * 
	 * 
	 * @param input,
	 *            len
	 * @return String
	 */
	public static String getLeftSubStringWithLen(String input, int len) {
		String result = "";
		logger.info("CommonUtil.getLeftSubStringWithLen() start");
		try {
			if (CommonUtil.isValidObject(input) && input.length() > 0 && input.length() >= len) {
				result = input.substring(0, input.length() - len);
			}
		} catch (Exception e) {
			logger.error("Failed in getLeftSubStringWithLen()", e);
		}
		return result;
	}

	/**
	 * This method is used to decimal formatting
	 * 
	 * 
	 * @param num
	 * @return String
	 */
	public static String decimalFormtter(float num) {
		String res = String.valueOf(num);
		logger.info("CommonUtil.decimalFormtter() start");
		try {
			DecimalFormat decimalFormat = new DecimalFormat("#.#");
			float twoDigitsF = Float.valueOf(decimalFormat.format(num));
			res = String.valueOf(twoDigitsF);
		} catch (Exception e) {
			logger.error("Failed in decimalFormtter()", e);
		}
		return res;
	}

	/**
	 * This method will create tar file
	 * 
	 * 
	 * @param inputPath,
	 *            tarGzPath
	 * @return boolean
	 */
	public static boolean createTarFile(String inputPath, String tarGzPath) throws IOException {
		boolean status = false;
		TarArchiveOutputStream tarOut = null;
		GzipCompressorOutputStream gzipOut = null;
		BufferedOutputStream bufferOut = null;
		FileOutputStream fileOut = null;
		logger.info("CommonUtil.createTarFile() start");

		try {
			File file = new File(tarGzPath);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}

			fileOut = new FileOutputStream(new File(tarGzPath));
			bufferOut = new BufferedOutputStream(fileOut);
			gzipOut = new GzipCompressorOutputStream(bufferOut);
			tarOut = new TarArchiveOutputStream(gzipOut);
			tarOut.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);
			addFileToTarGz(tarOut, inputPath, "");
			status = true;
		} catch (Exception ex) {

			logger.error(" CommonUtil ::createTarFile():: ::" + ExceptionUtils.getFullStackTrace(ex));
		} finally {
			try {
				if (tarOut != null) {

					tarOut.close();
				}
				if (gzipOut != null) {
					gzipOut.close();
				}
				if (bufferOut != null) {
					bufferOut.close();
				}
				if (fileOut != null) {
					fileOut.close();
				}
			} catch (Exception ex) {
				logger.error(" CommonUtil ::createTarFile():: ::" + ExceptionUtils.getFullStackTrace(ex));
			}

		}

		return status;

	}

	/**
	 * This method will add file to TarGz
	 * 
	 * 
	 * @param tOut,
	 *            path, base
	 * @return boolean
	 */
	private static boolean addFileToTarGz(TarArchiveOutputStream tOut, String path, String base) {

		boolean statusTar = false;
		logger.info("CommonUtil.addFileToTarGz() start");
		try {

			File f = new File(path);
			String entryName = base + f.getName();
			TarArchiveEntry tarEntry = new TarArchiveEntry(f, entryName);
			tOut.putArchiveEntry(tarEntry);

			if (f.isFile()) {
				IOUtils.copy(new FileInputStream(f), tOut);
				tOut.closeArchiveEntry();
			} else {
				tOut.closeArchiveEntry();
				File[] children = f.listFiles();
				if (children != null) {
					for (File child : children) {
						addFileToTarGz(tOut, child.getAbsolutePath(), entryName + "/");
					}
				}
			}

			statusTar = true;
		} catch (Exception e) {
			statusTar = false;
			logger.error(" CommonUtil ::addFileToTarGz():: ::" + ExceptionUtils.getFullStackTrace(e));
		}
		return statusTar;
	}

	/**
	 * This method will chunks System logs
	 * 
	 * 
	 * @param System_log_tar,
	 *            chunk_size
	 * @return Map<String,Object>
	 */
	public  Map<String, Object> Chunking_System_logs(String systemLogTar, long chunkSize) {
		logger.info("CommonUtil.Chunking_System_logs() start");
		// Map<String,>
		Map<String, Object> objTotDetails = new LinkedHashMap<>();

		Map<Integer, String> objPathDetails = new LinkedHashMap<>();

		File objTarFile = new File(systemLogTar);
		long fileSize = (long) objTarFile.length();

		int nChunks = 0;
		int read = 0;
		long readLength = chunkSize;

		OutputStream filePart = null;
		byte[] byteChunkPart;
		StringBuilder fileAppender = new StringBuilder();
		fileAppender.append(systemLogTar);
		System.out.println("Processing the chunks.......");
		try (InputStream inputStream = new FileInputStream(objTarFile)) {

			while (fileSize > 0) {
				if (fileSize <= chunkSize) {
					readLength = fileSize;
				}
				String newFileName = null;
				byteChunkPart = new byte[(int) readLength];
				read = inputStream.read(byteChunkPart, 0, (int) readLength);
				fileSize -= read;
				assert (read == byteChunkPart.length);
				nChunks++;
				newFileName = fileAppender.toString() + ".part" + Integer.toString(nChunks);
				System.out.println(newFileName);
				filePart = new FileOutputStream(new File(newFileName));
				filePart.write(byteChunkPart);
				filePart.flush();
				filePart.close();
				byteChunkPart = null;
				filePart = null;

				objPathDetails.put(nChunks, newFileName);
				System.out.println(objPathDetails);
			}

			objTotDetails.put("pathDetails", objPathDetails);
			objTotDetails.put("totCount", nChunks);

		} catch (IOException e) {
			logger.error("Exception for Chunking_System_logs() in CommonUtil  :" + ExceptionUtils.getFullStackTrace(e));
		}
		System.out.println(objTotDetails);

		return objTotDetails;

	}

	/**
	 * This method will compute file checksum
	 * 
	 * 
	 * @param digest,
	 *            file
	 * @return String
	 */
	public  String computeFileChecksum(MessageDigest digest, String file) throws Exception {
		StringBuilder sb = new StringBuilder();

		// Get file input stream for reading the file content
		FileInputStream fis = new FileInputStream(file);
		logger.info("CommonUtil.computeFileChecksum() start");
		try {

			// Create byte array to read data in chunks
			byte[] byteArray = new byte[1024];
			int bytesCount = 0;

			// Read file data and update in message digest
			while ((bytesCount = fis.read(byteArray)) != -1) {
				digest.update(byteArray, 0, bytesCount);
			}

			// Getting the hash bytes value
			byte[] bytes = digest.digest();

			// Converting Decimal to Hexadecimal format
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (Exception e) {
			logger.error("Exception for computeFileChecksum() in CommonUtil  :" + ExceptionUtils.getFullStackTrace(e));
		} finally {
			fis.close();
		}
		return sb.toString();
	}

	/**
	 * This method will check IP and Port reachable or not
	 * 
	 * 
	 * @param IP,
	 *            port
	 * @return boolean
	 */
	public  boolean isIPandportreachable(String ip, Integer port) {

		Socket socket = null;
		boolean statusOfServer = false;
		logger.info("CommonUtil.isIPandportreachable() start");
		try {

			socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), 1000);
			statusOfServer = true;
		} catch (Exception e) {

			logger.error("CommonUtil.isIPandportreachable:" + ExceptionUtils.getMessage(e));
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (Exception e) {
					logger.error("CommonUtil.isIPandportreachable:" + ExceptionUtils.getMessage(e));
				}
			}
		}
		return statusOfServer;
	}

	/**
	 * This method will get Files DateInterval
	 * 
	 * 
	 * @param folderPath,
	 *            startTime, endTime, dateFormate
	 * @return File
	 */
	public static File[] getFilesDateInterval(String folderPath, Date startTime, Date endTime, String dateFormate) {

		File files[] = null;
		logger.info("CommonUtil.getFilesDateInterval() start");
		try {

			FileFilterDateIntervalUtils filter = new FileFilterDateIntervalUtils(
					DateUtil.dateToString(startTime, dateFormate), DateUtil.dateToString(endTime, dateFormate),
					dateFormate);
			File folder = new File(folderPath);
			files = folder.listFiles(filter);

		} catch (Exception e) {
			logger.error(" CommonUtil::getFilesDateInterval():: ::" + ExceptionUtils.getFullStackTrace(e));
		}

		return files;

	}

	/**
	 * This method will write data to the file
	 * 
	 * 
	 * @param data,
	 *            filePath
	 * @return
	 */
	public static void writeDataToFile(String data, String filePath) {

		BufferedWriter writer = null;
		FileOutputStream fileOutputStream = null;
		OutputStreamWriter outputStreamWriter = null;
		logger.info("CommonUtil.writeDataToFile() start");
		try {
			fileOutputStream = new FileOutputStream(filePath);
			outputStreamWriter = new OutputStreamWriter(fileOutputStream, "utf-8");
			writer = new BufferedWriter(outputStreamWriter);
			writer.write(data);

		} catch (Exception e) {
			logger.error(" CommonUtil::writeDataToFile():: ::" + ExceptionUtils.getFullStackTrace(e));
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
				if (outputStreamWriter != null) {
					outputStreamWriter.close();
				}
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}

			} catch (Exception ex) {
				logger.error("Failed to writeMd5DataToFile :" + ExceptionUtils.getMessage(ex));
			}
		}
	}

}