package com.delta.ifdt.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class PasswordCryption {
	
		static final Logger logger = LoggerFactory.getLogger(PasswordCryption.class);
		
		public static final String SERVER_SECRET_KEY = "Bar12345Bar12345"; // // 128
																			// bit
																			// key

		public static final String INIT_VECTOR = "RandomInitVector"; // 16 bytes IV

		private static SecretKeySpec secretKey = null;

		
		/**
		 * Instantiates a new common utility.
		 */

		/**
		 * This method will Instantiates a new common utility
		 * 
		 * @param myKey
		 * @return SecretKeySpec
		 */
		public static SecretKeySpec setKey(String myKey) {
			try {
				secretKey = new SecretKeySpec(myKey.getBytes(), "AES");
			} catch (Exception e) {
				logger.error("Error PasswordCryption.setKey(): " + e.toString());
			}
			return secretKey;
		}

		
		
		/**
		 * This method will Encrypt string
		 * 
		 * @param strToDecrypt
		 * @return String
		 */
		public static String encrypt(String strToEncrypt) {
			try {
				Cipher cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.ENCRYPT_MODE, setKey(SERVER_SECRET_KEY));
				return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
			} catch (Exception e) {
				logger.error("Error while encrypting: " + e.toString());
			}
			return null;
		}

		
		
		/**
		 * This method will Decrypt string
		 * 
		 * @param strToDecrypt
		 * @return String
		 */
		public static String decrypt(String strToDecrypt) {
			try {
				Cipher cipher = Cipher.getInstance("AES");
				cipher.init(Cipher.DECRYPT_MODE, setKey(SERVER_SECRET_KEY));
				return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt.getBytes(StandardCharsets.UTF_8))));
			} catch (Exception e) {
				logger.error("Error while decrypting: " + e.toString());
			}
			return null;
		}

		
		
		/**
		 * This method will decrypt password
		 * 
		 * @param password
		 * @return
		 */
		public static String decryptPasswordUI(String password) {
			
			try {
				logger.error(" decryptPassword 1");
				byte[] decoded = org.apache.tomcat.util.codec.binary.Base64.decodeBase64(password.getBytes());

				logger.error("Exception : decryptPassword 2");
				

				password = new String(decoded, StandardCharsets.UTF_8);

				logger.error("Exception : decryptPassword 3");
			} catch (Exception e) {
				logger.error("Exception : decryptPassword " + ExceptionUtils.getFullStackTrace(e));
			}
			return password;
		}

		public static String encryptPasswordUI(String password) {
			
			try {
				byte[] dencoded = org.apache.tomcat.util.codec.binary.Base64.encodeBase64(password.getBytes());
				password = new String(dencoded, StandardCharsets.UTF_8);
			} catch (Exception e) {
				logger.error("Exception : encryptPassword " + ExceptionUtils.getFullStackTrace(e));
			}
			return password;
		}

}
