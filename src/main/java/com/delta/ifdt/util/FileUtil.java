package com.delta.ifdt.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.delta.ifdt.constants.Constants;

public class FileUtil {

	private String dir = null;
	private List<String> fileList = new ArrayList<>();
	private String fileName = null;

	static final  Logger logger = LoggerFactory.getLogger(FileUtil.class);

	
	/**
	 * delete the file or folder
	 * 
	 * @param fileWithPath
	 * @return
	 */
	public static boolean deleteFileOrFolder(String fileWithPath) {

		boolean stat = false;
		try {
			File file = new File(fileWithPath);
			if (file.isDirectory()) {
				String[] files = file.list();
				for (String fle : files) {
					File currentFile = new File(file.getPath(), fle);
					if (currentFile.isDirectory()) {
						stat = deleteFileOrFolder(currentFile.toString());
					} else {
						stat = currentFile.delete();
					}
				}
				stat = file.delete();// delete folder once sub folder/files are
										// deleted
			} else {
				stat = file.delete();
			}
		} catch (Exception e) {
			logger.error("Handled - Exception in deleteFileOrFolder :" + ExceptionUtils.getMessage(e));
		}
		return stat;
	}

	

	
	
	/**
	 * create directory
	 * 
	 * @param dirPath
	 * @return
	 * @throws IOException
	 */
	public static boolean createDirectory(String dirPath) throws IOException {

		File destFolder = new File(dirPath);
		File parentDir = destFolder.getCanonicalFile();
		if (!parentDir.exists()) {
			File file = new File(dirPath);
			if (file.mkdirs()) {
				return true;
			}

		}
		return false;
	}
	
	
	

	
	
	
	
	
	
	/**
	 * copy file to given location
	 * 
	 * @param fileWithLocation
	 * @param newFileLocation
	 * @throws Exception
	 * @retrun void
	 */
	public static void copyFastFileToLocation(String fileWithLocation, String newFileLocation) throws Exception {

		try {
			File sourceLocation = new File(fileWithLocation);
			File targetLocation = new File(newFileLocation);
			copyFastDirectory(sourceLocation, targetLocation);
		} catch (Exception e) {
			logger.error("Failed to copyFastFileToLocation  ::" + ExceptionUtils.getFullStackTrace(e));
			throw e;
		}
	}

	
	
	/**
	 * This method will copy to directory
	 * 
	 *@param sourceLocation
	 * @param targetLocation
	 * @return void
	 */
	private static void copyFastDirectory(File sourceLocation, File targetLocation) throws Exception {
		
		try {
			if (sourceLocation.isDirectory()) {
				if (!targetLocation.exists()) {
					targetLocation.mkdir();
				}
				String[] children = sourceLocation.list();
				for (int i = 0; i < children.length; i++) {
					copyFastDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
				}
			} else {

				Files.copy(sourceLocation.toPath(), targetLocation.toPath());
				
			}
		} catch (Exception e) {
			logger.error("Failed to copyFastDirectory  ::" + ExceptionUtils.getFullStackTrace(e));

		} 
	}

	
	
	/**
	 * copy file to given location
	 * 
	 * @param fileWithLocation
	 * @param newFileLocation
	 * @retrun Exception
	 * @return void
	 */
	public static void copyFastFileToLocationbetweenDatess(String fileWithLocation, String newFileLocation,
			Date fromTime, Date toTime, String dateFormate) throws Exception {

		try {
			File sourceLocation = new File(fileWithLocation);
			File targetLocation = new File(newFileLocation);
			copyFastDirectorybetweenDates(sourceLocation, targetLocation, fromTime, toTime, dateFormate);
		} catch (Exception e) {
			logger.error("Failed to copyFastFileToLocation  ::" + ExceptionUtils.getFullStackTrace(e));
			throw e;
		}
	}

	
	
	/**
	 * Tis method will copy from source to destination
	 * 
	 * @param sourceLocation
	 * @param targetLocation
	 * @param fromTime
	 * @return void
	 */
	private static void copyFastDirectorybetweenDates(File sourceLocation, File targetLocation, Date fromTime,
			Date toTime, String dateFormate) throws Exception {

		try {
			if (sourceLocation.isDirectory()) {

				if (!targetLocation.exists()) {
					targetLocation.mkdir();
				}
				String[] children = sourceLocation.list();
				for (int i = 0; i < children.length; i++) {
					long modifiedDate = new File(sourceLocation, children[i]).lastModified();
					boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
					if (fileExibetDates) {

						copyFastDirectorybetweenDates(new File(sourceLocation, children[i]),
								new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
					} else {
						continue;
					}
				}
			} else {
				long modifiedDate = sourceLocation.lastModified();
				boolean fileExibetDates = fileExistInbetweenDates(fromTime, toTime, modifiedDate);
				if (fileExibetDates) {
					
					FileUtils.copyFile(sourceLocation,targetLocation);

				}

			}
		} catch (Exception e) {
			logger.error("Failed to copyFastDirectorybetweenDates  ::" + ExceptionUtils.getFullStackTrace(e));

		}
	}

	
	/**
	 * Tis method will check fileExistInbetweenDates
	 * 
	 * @param fromTime
	 * @param toTime
	 * @param modifiedtime
	 * @return boolean
	 */
	private static boolean fileExistInbetweenDates(Date fromTime, Date toTime, long modifiedtime) {
		boolean status = false;

		try {
			Date dateModified = new Date(modifiedtime);
			status = dateModified.after(fromTime) && dateModified.before(toTime);

		} catch (Exception e) {
			status = false;
			logger.error("Failed to fileExistInbetweenDates  ::" + ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	
	
	/**
	 * copy file to given location
	 * 
	 * @param fileWithLocation
	 * @param newFileLocation
	 * @retrun Exception
	 * @return void
	 */
	public static void copyFastFileToLocationbetweenDatessItu(String fileWithLocation, String newFileLocation,
			Date fromTime, Date toTime, String dateFormate) throws Exception {

		try {
			File sourceLocation = new File(fileWithLocation);
			File targetLocation = new File(newFileLocation);
			copyFastDirectorybetweenDatesItu(sourceLocation, targetLocation, fromTime, toTime, dateFormate);
		} catch (Exception e) {
			logger.error("Failed to copyFastFileToLocation  ::" + ExceptionUtils.getFullStackTrace(e));
			throw e;
		}
	}

	
	
	/**
	 * Tis method will copy from source to destination
	 * 
	 * @param sourceLocation
	 * @param targetLocation
	 * @param fromTime
	 * @return void
	 */
	private static void copyFastDirectorybetweenDatesItu(File sourceLocation, File targetLocation, Date fromTime,
			Date toTime, String dateFormate) throws Exception {

		try {
			if (sourceLocation.isDirectory()) {

				if (!targetLocation.exists()) {
					targetLocation.mkdir();
				}
				String[] children = sourceLocation.list(new FilenameFilter() {
					
					@Override
					public boolean accept(File dir, String name) {
						// TODO Auto-generated method stub
						return name.endsWith(Constants.LOG_EXTENSTION) && (name.contains(Constants.DO) || name.contains(Constants.FSS) || name.contains(Constants.ISD));
					}
				});
				for (int i = 0; i < children.length; i++) {
					long modifiedDate = new File(sourceLocation, children[i]).lastModified();
					boolean fileExibetDates = fileExistInbetweenDatesItu(fromTime, toTime, modifiedDate);
					if (fileExibetDates) {

						copyFastDirectorybetweenDates(new File(sourceLocation, children[i]),
								new File(targetLocation, children[i]), fromTime, toTime, dateFormate);
					} else {
						continue;
					}
				}
			} else {
				long modifiedDate = sourceLocation.lastModified();
				boolean fileExibetDates = fileExistInbetweenDatesItu(fromTime, toTime, modifiedDate);
				if (fileExibetDates) {
					
					FileUtils.copyFile(sourceLocation,targetLocation);

				}

			}
		} catch (Exception e) {
			logger.error("Failed to copyFastDirectorybetweenDatesItu  ::" + ExceptionUtils.getFullStackTrace(e));

		}
	}
	
	/**
	 * Tis method will check fileExistInbetweenDatesItu
	 * 
	 * @param fromTime
	 * @param toTime
	 * @param modifiedtime
	 * @return boolean
	 */
	private static boolean fileExistInbetweenDatesItu(Date fromTime, Date toTime, long modifiedtime) {
		boolean status = false;

		try {
			Date dateModified = new Date(modifiedtime);
			status = dateModified.after(fromTime) && dateModified.before(toTime);

		} catch (Exception e) {
			status = false;
			logger.error("Failed to fileExistInbetweenDatesItu  ::" + ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	
}