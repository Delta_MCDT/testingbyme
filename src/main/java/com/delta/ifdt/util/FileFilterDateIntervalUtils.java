package com.delta.ifdt.util;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileFilterDateIntervalUtils implements FilenameFilter { 
    String dateStart;
    String dateEnd;
    SimpleDateFormat sdf;

    
    
	/**
	 * This method is used to send mail
	 * 
	 * 
	 * @param toList
	 * @return boolean
	 */
public FileFilterDateIntervalUtils(String dateStart, String dateEnd,String dateFormate) {
   this.dateStart = dateStart;
   this.dateEnd = dateEnd;

   sdf = new SimpleDateFormat(dateFormate);
}


/**
 * This method is used to accept
 * 
 * 
 * @param dir, name
 * @return boolean
 */
public boolean accept(File dir, String name) {
   Date d = new Date(new File(dir, name).lastModified());
   String current = sdf.format(d);
   return (dateStart.compareTo(current) < 0 && (dateEnd.compareTo(current) >= 0));
}
}
