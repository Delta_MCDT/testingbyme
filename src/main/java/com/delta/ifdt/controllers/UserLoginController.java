package com.delta.ifdt.controllers;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.entities.UserSessionPool;
import com.delta.ifdt.exception.DeltaMcdtException;
import com.delta.ifdt.models.User;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.GlobalStatusMap;

@RestController
public class UserLoginController {

	static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);

	@Autowired
	UserLoginService userLoginService;

	/**
	 * This method will login a user
	 * 
	 * @param loginDetails
	 * @param request
	 * @param session
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/loginUser")
	public @ResponseBody JSONObject loginUser(@RequestBody JSONObject loginDetails, HttpServletRequest request,
			HttpSession session) {
		logger.info("UserLoginController.loginUser()");
		JSONObject resultMap = new JSONObject();
		User user = null;
		String serviceToken = null;
		try {
			user = new User();
			user.setUserName(loginDetails.get("username").toString());
			serviceToken = loginDetails.get(Constants.SERVICE_TOKEN).toString();
			UserManagementModel userEntity = userLoginService.getUserDetailsByUserName(user.getUserName());

			if (userLoginService.validUser(userEntity, loginDetails.get("password").toString())) {
				if (!Constants.ACTIVE.equals(userEntity.getStatus())) {
					resultMap.put(Constants.VALID_USER, false);
					resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
					return resultMap;
				}

				Date loginDate = new Date();
				userLoginService.setLastLogin(user.getUserName(), loginDate);
				user.setRole(userEntity.getRole());
				user.setServiceToken(serviceToken);
				resultMap.put(Constants.VALID_USER, true);
				resultMap.put(Constants.USER_NAME, userEntity.getUserName());
				resultMap.put("userRole", userEntity.getRole());
				resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
				Map<String,Object> menuList= userLoginService.getMenuList();
				resultMap.put("mainMenu", menuList.get("test"));
				if (userEntity.getLastLoginDate() != null) {
					user.setLastLoginTime(userEntity.getLastLoginDate());
				} else {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					user.setLastLoginTime((sdf.format(loginDate)));
				}
				resultMap.put(Constants.SESSION_ID, user.getTokenKey());
				UserSessionPool.getInstance().addUser(user);
				request.getSession().setAttribute(Constants.USER_NAME, user);
				

				return resultMap;
			} else {
				resultMap.put(Constants.VALID_USER, false);
				resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			}
		} catch (Exception e) {
			logger.info("Exception in UserLoginController.loginAction(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
			
		}
		return resultMap;
	}

	/**
	 * This method will logout the user
	 * 
	 * @param logoutDetails
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/logoutUser")
	public @ResponseBody JSONObject logoutUser(@RequestBody JSONObject logoutDetails) {
		logger.info("UserLoginController.logoutUser()");
		String sessionId = null;
		String serviceToken = null;
		JSONObject resultMap = new JSONObject();
		try {
			serviceToken = logoutDetails.get(Constants.SERVICE_TOKEN).toString();
			sessionId = logoutDetails.get(Constants.SESSION_ID).toString();
			UserSessionPool.getInstance().removeUser(sessionId);
			if (GlobalStatusMap.loginUsersDetails.containsKey(sessionId)) {
				GlobalStatusMap.loginUsersDetails.remove(sessionId);
			}
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			return resultMap;
		} catch (Exception e) {
			logger.error("Exception in UserLoginController.logoutUser(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			return resultMap;
		}
	}

	/**
	 * This method will allow the user to change password
	 * 
	 * @param changePasswordDetails
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/changePassword")
	public @ResponseBody JSONObject changePassword(@RequestBody JSONObject changePasswordDetails) {
		logger.info("UserLoginController.changePassword()");
		String sessionId = null;
		String serviceToken = null;
		JSONObject resultMap = new JSONObject();
		
		try {
			String userName = changePasswordDetails.get(Constants.USER_NAME).toString();
			String password = changePasswordDetails.get("currentPassword").toString();
			String newPassword = changePasswordDetails.get("newPassword").toString();
			sessionId = changePasswordDetails.get(Constants.SESSION_ID).toString();
			serviceToken = changePasswordDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);

			UserManagementModel userManagementModel = userLoginService.getUserDetailsByUserName(userName);
			if (userLoginService.validUser(userManagementModel, password)) {
				if (!userLoginService.changePassword(userName, newPassword, true)) {
					resultMap.put(Constants.STATUS, Constants.FAIL);
					return resultMap;
				}
			} else {
				resultMap.put(Constants.STATUS, Constants.FAIL);
				return resultMap;
			}
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
			return resultMap;
		} catch (Exception e) {
			logger.error("Exception in LoginActionController.changePassword(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
			return resultMap;
		}
	}

	/**
	 * This method will allow the user to reset the password
	 * 
	 * @param changePasswordDetails
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/forgotPassword")
	public @ResponseBody JSONObject forgotPassword(@RequestBody JSONObject forgotPasswordDetails) {
		logger.info("UserLoginController.forgotPassword()"+forgotPasswordDetails);
		JSONObject resultMap = new JSONObject();
		try {
			String emailId = forgotPasswordDetails.get("emailId").toString();
			UserManagementEntity userDetail = userLoginService.getUserDetailsByEmailId(emailId);
			if (userDetail == null) {
				resultMap.put(Constants.STATUS, Constants.FAIL);
				return resultMap;
			}
			String userName = userDetail.getUserName();
			String userFullName = userDetail.getFullName();
			String newPassword = randomString(8);
			if (newPassword != null) {
				try {
					if (userLoginService.mailNewPassword(userFullName, userName, newPassword, emailId)) {
						userLoginService.resetPassword(userName, newPassword);
						if (userLoginService.changePassword(userName, newPassword, false)) {
							resultMap.put(Constants.STATUS, Constants.SUCCESS);
							resultMap.put("reason", null);
						} else {
							resultMap.put(Constants.STATUS, Constants.FAIL);
							return resultMap;
						}
						return resultMap;
					} else {
						resultMap.put(Constants.STATUS, Constants.FAIL);
						return resultMap;
					}
				} catch (AuthenticationFailedException ae) {
					resultMap.put(Constants.STATUS, Constants.FAIL);
					return resultMap;
				} catch (MessagingException me) {
					resultMap.put(Constants.STATUS, Constants.FAIL);
					return resultMap;
				} catch (DeltaMcdtException e) {
					resultMap.put(Constants.STATUS, Constants.FAIL);
					resultMap.put("reason", e.getMessage());
					return resultMap;
				} catch (TimeoutException te) {
					logger.error("Exception Failed to send message:" + ExceptionUtils.getFullStackTrace(te));
					resultMap.put(Constants.STATUS, Constants.FAIL);
					return resultMap;
				}

			} else {
				resultMap.put(Constants.STATUS, Constants.FAIL);
				return resultMap;
			}
		} catch (Exception e) {
			logger.error("Exception in LoginActionController.forgotPassword(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
			return resultMap;
		}
	}

	private String randomString(int length) {
		String ranString = null;
		String desiredCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom secRandom = new SecureRandom();
		try {
			StringBuilder sb = new StringBuilder(length);
			for (int i = 0; i < length; i++) {
				sb.append(desiredCharacters.charAt(secRandom.nextInt(desiredCharacters.length())));
			}
			ranString = sb.toString();
		} catch (Exception e) {
			logger.error("Exception in LoginActionController.randomString(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return ranString;
	}

}
