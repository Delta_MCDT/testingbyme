package com.delta.ifdt.controllers;



import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.service.FileTransferStatusDetailsService;


@RestController
public class FileTransferStatusController {
	
	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusController.class);

	@Autowired
	FileTransferStatusDetailsDto fileTransferStatusDetailsDto;
	
	@Autowired
	FileTransferStatusDetailsService fileTransferStatusDetailsService;
	
	@Autowired
	Environment env;

	

	/**
	 * This method will save  the manual offload transfer details
	 * 
	 * @param PendriveDetails
	 * @return statusOfOffload
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/manualOffloadTransfer")
	public JSONObject manualOffload(@RequestBody JSONObject pendriveDetails) {
		JSONObject resultMap = new JSONObject();
		String pendrivePath = null; 
		logger.info("FileTransferStatusController.manualOffload"+pendriveDetails);
		
		try
		{
			
			pendrivePath = pendriveDetails.get("pendrivePath").toString();
			boolean status = fileTransferStatusDetailsService.manualOffloadTransfer(pendrivePath);
			resultMap.put(Constants.STATUS, status);
		}
		catch (Exception e) {
			resultMap.put(Constants.STATUS, false);
			logger.error("FileTransferStatusController.manualOffload check"+ ExceptionUtils.getFullStackTrace(e));
			
		}
		
		return resultMap;


}
	
	
	/**
	 * This method will save  the fileTransferDetails
	 * 
	 * @param auditListDetails
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/fileTransferDetails")
	public JSONObject fileTransferDetails(@RequestBody String auditListDetails) {

		JSONObject resultMap = new JSONObject();
		
		boolean status = true;
		logger.info("FileTransferStatusController.fileTransferDetails start: "+auditListDetails);
		
		try {
			
			 status=fileTransferStatusDetailsService.flightTransferIfeToGround();
			resultMap.put(Constants.STATUS, status);
			
		} catch (Exception e) {
			logger.error("FileTransferStatusController.fileTransferDetails check"+ ExceptionUtils.getFullStackTrace(e));
		}
		return resultMap;
	}

}