package com.delta.ifdt.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.UserManagementDto;
import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.service.UserManagementService;
import com.delta.ifdt.service.UserRoleService;
import com.delta.ifdt.util.CommonUtil;
import com.google.gson.Gson;

@RestController
public class UserManagementController {
	
	static final Logger logger = LoggerFactory.getLogger(UserManagementController.class);

	@Autowired
	UserManagementService userManagementService;

	@Autowired
	UserRoleService userRoleService;

	@Autowired
	UserManagementDto userManagementDto;

	/**
	 * This method will a new create user
	 * 
	 * @param createUserDetails
	 * @return JSONObject
	 */
	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	@PostMapping(value = "/createNewUser")
	public JSONObject createNewUser(@RequestBody JSONObject createUserDetails) {
		String sessionId = null;
		String serviceToken = null;
		JSONObject resultMap = new JSONObject();
		try {
			sessionId = createUserDetails.get(Constants.SESSION_ID).toString();
			serviceToken = createUserDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			UserManagementModel userDetails = new Gson().fromJson(
					createUserDetails.toJSONString((Map) createUserDetails.get("CreateUserDetails")),
					UserManagementModel.class);
			if (userManagementService.duplicateUser(userDetails)) {
				resultMap.put(Constants.STATUS, Constants.FAIL);
				return resultMap;
			}
			UserManagementEntity userEntity = userManagementDto.getUserDetails(userDetails);
			if (userEntity != null) {
				if (userManagementService.createUser(userEntity)) {
					resultMap.put(Constants.STATUS, Constants.SUCCESS);
				} else {
					resultMap.put(Constants.STATUS, Constants.FAIL);
				}
			}
		} catch (Exception e) {
			logger.info(
					"Exception in UserManagementController.createNewUser(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
		}
		return resultMap;
	}

	/**
	 * This method will getUserList
	 * 
	 * @param userListDetails
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/userList")
	public JSONObject userList(@RequestBody JSONObject userListDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		try {
			sessionId = userListDetails.get(Constants.SESSION_ID).toString();
			serviceToken = userListDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			List<UserRoleEntity> userRoleDetailsEntityList = userRoleService.getUserRoleList();
			List<UserManagementModel> userList = userManagementService.getUserList();
			if (!CommonUtil.isValidObject(userList)) {
				userList = new ArrayList<>();
			}
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
			resultMap.put("userList", userList);
			resultMap.put("userRoleList", userRoleDetailsEntityList);
		} catch (Exception e) {
			logger.info("Exception in UserManagementController.getUserList(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
		}
		return resultMap;
	}

	/**
	 * This method will getUserList
	 * 
	 * @param userListDetails
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/deleteUser")
	public JSONObject deleteUser(@RequestBody JSONObject deleteUserDetails) {
		String sessionId = null;
		String serviceToken = null;
		JSONObject resultMap = new JSONObject();
		String userId = null;
		try {
			sessionId = deleteUserDetails.get(Constants.SESSION_ID).toString();
			serviceToken = deleteUserDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			userId = deleteUserDetails.get("userId").toString();
			if (StringUtils.isNotEmpty(userId)) {
				if (userManagementService.deleteUser(Integer.valueOf(userId))) {
					resultMap.put(Constants.STATUS, Constants.SUCCESS);
				} else {
					resultMap.put(Constants.STATUS, Constants.FAIL);
				}
			}
		} catch (Exception e) {
			resultMap.put(Constants.STATUS, Constants.FAIL);
			logger.error("Exception in UserManagementController.deleteUser(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return resultMap;
	}

}
