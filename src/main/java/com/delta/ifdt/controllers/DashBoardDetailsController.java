package com.delta.ifdt.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.dto.FileTransferStatusDetailsDto;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.service.DashBoardDetailsService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;

@RestController
public class DashBoardDetailsController {

	static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsController.class);
	@Autowired
	DashBoardDetailsService dashBoardDetailsService;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	FileTransferStatusDetailsDto fileTransferStatusDetailsDto;

	@SuppressWarnings({ "unchecked"})
	@PostMapping(value = "/dashBoardDetails")
	public JSONObject manualOffload(@RequestBody JSONObject pendriveDetails) {
		logger.info("DashBoardDetailsController.manualOffload() metadatajson manualoffload" + pendriveDetails);
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		String searchStatus = null;
		String fromDate = null;
		String toDate = null;
		try {
			sessionId = pendriveDetails.get("sessionId").toString();
			serviceToken = pendriveDetails.get("serviceToken").toString();
			searchStatus = pendriveDetails.get("searchStatus").toString();
			resultMap.put("sessionId", sessionId);
			resultMap.put("serviceToken", serviceToken);

			sessionId = pendriveDetails.get(Constants.SESSION_ID).toString();
			serviceToken = pendriveDetails.get(Constants.SERVICE_TOKEN).toString();
			searchStatus = pendriveDetails.get("searchStatus").toString();
			
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			if (StringUtils.isNotEmpty(searchStatus) && Constants.LOAD.equals(searchStatus)) {
				Date endDate = new Date();
				toDate = DateUtil.dateToString(endDate, Constants.MM_DD_YYYY);
				Calendar c = Calendar.getInstance();
				c.setTime(endDate);
				Integer pastHistory = 30;
				c.add(Calendar.DATE, -pastHistory);
				Date sdate = c.getTime();
				fromDate = DateUtil.dateToString(sdate, Constants.MM_DD_YYYY);
			}
			if (StringUtils.isNotEmpty(searchStatus) && Constants.SEARCH.equals(searchStatus)) {
				fromDate = pendriveDetails.get("fromDate").toString();
				toDate = pendriveDetails.get("toDate").toString();

			}
			Map<String, Object> statusMapDetails = dashBoardDetailsService.getDashBoardDetails(fromDate,toDate);
			if (statusMapDetails != null && statusMapDetails.size() > 0) {
				resultMap.put("fileTransferDetails", statusMapDetails.get("fileTransferDetails"));
				resultMap.put("mcdtManualDetails", statusMapDetails.get("mcdtManualDetails"));
				resultMap.put("reasonsForFailure", statusMapDetails.get("reasonsForFailure"));
				resultMap.put(Constants.STATUS, Constants.SUCCESS);
			}
		} catch (Exception e) {
			logger.error("Exception for  manualOffload() in  DashBoardDetailsController: metadata.json"
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return resultMap;

	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getGraphDetails")
	public JSONObject getGraphDetails(@RequestBody JSONObject graphDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		String type = null;
		String fromDate = null;
		String toDate = null;
		try {
			sessionId = graphDetails.get(Constants.SESSION_ID).toString();
			serviceToken = graphDetails.get(Constants.SERVICE_TOKEN).toString();
			type = graphDetails.get("type").toString();
			fromDate = graphDetails.get("fromDate").toString();
			toDate = graphDetails.get("toDate").toString();
			Map<String, Integer> paginationData = (Map<String, Integer>) graphDetails.get(Constants.PAGINATION);
			int page = paginationData.get("page");
			int count = paginationData.get(Constants.COUNT);
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			Map<String, Object> graphDetailsList = dashBoardDetailsService.getGraphDetails(type,page,count,fromDate,toDate);
			resultMap.put("graphDetailsList", graphDetailsList.get("graphDetails"));
			resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.page_count));
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
		} catch (Exception e) {
			logger.error("Exception for  getGraphDetails() in  DashBoardDetailsController: metadata.json"
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return resultMap;

	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getFileGraphDetails")
	public JSONObject getFileGraphDetails(@RequestBody JSONObject fileGraphDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		String type = null;
		String fromDate = null;
		String toDate = null;
		try {
			sessionId = fileGraphDetails.get(Constants.SESSION_ID).toString();
			serviceToken = fileGraphDetails.get(Constants.SERVICE_TOKEN).toString();
			type = fileGraphDetails.get("type").toString();
			fromDate = fileGraphDetails.get("fromDate").toString();
			toDate = fileGraphDetails.get("toDate").toString();
			Map<String, Integer> paginationData = (Map<String, Integer>) fileGraphDetails.get(Constants.PAGINATION);
			int page = paginationData.get("page");
			int count = paginationData.get(Constants.COUNT);
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			Map<String, Object> graphDetailsList = dashBoardDetailsService.getFileGraphDetails(type,page,count,fromDate,toDate);
			resultMap.put(Constants.FILE_GRAPH_DETAILS_LIST, graphDetailsList.get("fileGraphDetails"));
			resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.page_count));
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
		} catch (Exception e) {
			logger.error("Exception for  getFileGraphDetails() in  DashBoardDetailsController: metadata.json"
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return resultMap;

	}
	
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getFailureReasonDetails")
	public JSONObject getFailureReasonDetails(@RequestBody JSONObject failureReasonDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		String type = null;
		String fromDate = null;
		String toDate = null;
		try {
			sessionId = failureReasonDetails.get(Constants.SESSION_ID).toString();
			serviceToken = failureReasonDetails.get(Constants.SERVICE_TOKEN).toString();
			type = failureReasonDetails.get("type").toString();
			fromDate = failureReasonDetails.get("fromDate").toString();
			toDate = failureReasonDetails.get("toDate").toString();
			Map<String, Integer> paginationData = (Map<String, Integer>) failureReasonDetails.get(Constants.PAGINATION);
			int page = paginationData.get("page");
			int count = paginationData.get(Constants.COUNT);
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			Map<String, Object> graphDetailsList = dashBoardDetailsService.getFailureReasonDetails(type,page,count,fromDate,toDate);
			resultMap.put("failureReasonDetails", graphDetailsList.get("failureReasonDetails"));
			resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.page_count));
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
		} catch (Exception e) {
			logger.error("Exception for  getFailureReasonDetails() in  DashBoardDetailsController: metadata.json"
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return resultMap;

	}
	
	@SuppressWarnings({ "unchecked"})
	@PostMapping(value = "/getStatisticsDetails")
	public JSONObject getStatisticsDetails(@RequestBody JSONObject statisticsDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		String searchStatus = null;
		String fromDate = null;
		String toDate = null;

		List<FileTransferStatusDetailsEntity> fileTransferStatusDetailsEntity = null;
		List<FileTransferStatusDetailsModel> fileTransferStatusDetailModel = null;
		try {
			sessionId = statisticsDetails.get(Constants.SESSION_ID).toString();
			serviceToken = statisticsDetails.get(Constants.SERVICE_TOKEN).toString();
			searchStatus = statisticsDetails.get("searchStatus").toString();
			fromDate = statisticsDetails.get("fromDate").toString();
			toDate = statisticsDetails.get("toDate").toString();
			Map<String, Integer> paginationData = (Map<String, Integer>) statisticsDetails.get(Constants.PAGINATION);
			int page = paginationData.get("page");
			int count = paginationData.get(Constants.COUNT);
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			if(Constants.LOAD.equals(searchStatus))
			{
				Map<String, Object> graphDetailsList = dashBoardDetailsService.getStatisticsDetails(fromDate,toDate,page,count);
				resultMap.put(Constants.FILE_GRAPH_DETAILS_LIST, graphDetailsList.get(Constants.STATISTIC_DETAILS));
				resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.page_count));
				resultMap.put(Constants.STATUS, Constants.SUCCESS);
			}
			if(Constants.SEARCH.equals(searchStatus))
			{
				Map<String, Object> graphDetailsList = dashBoardDetailsService.getStatisticsDetails(fromDate,toDate,page,count);	
				fileTransferStatusDetailsEntity = (List<FileTransferStatusDetailsEntity>) graphDetailsList.get(Constants.STATISTIC_DETAILS);	
				resultMap.put(Constants.FILE_GRAPH_DETAILS_LIST, graphDetailsList.get(Constants.STATISTIC_DETAILS));
				resultMap.put(Constants.PAGE_COUNT, graphDetailsList.get(Constants.page_count));
				resultMap.put(Constants.STATUS, Constants.SUCCESS);
			}
			if(CommonUtil.isValidObject(fileTransferStatusDetailsEntity)){
				fileTransferStatusDetailModel = new ArrayList<FileTransferStatusDetailsModel>();
				for(FileTransferStatusDetailsEntity objEntity: fileTransferStatusDetailsEntity){
					fileTransferStatusDetailModel.add(fileTransferStatusDetailsDto.getStatisticsDetailsEntity(objEntity));
				}
			}
			//resultMap.put("fileGraphDetailsList",fileTransferStatusDetailModel);
		} catch (Exception e) {
			logger.error("Exception for  getStatisticsDetails() in  DashBoardDetailsController: metadata.json"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return resultMap;
	}
	
}
