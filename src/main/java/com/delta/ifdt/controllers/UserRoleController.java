package com.delta.ifdt.controllers;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.service.UserRoleService;

@RestController
public class UserRoleController {

	static final Logger logger = LoggerFactory.getLogger(UserRoleController.class);

	@Autowired
	UserRoleService userRoleService;

	/**
	 * This method will getUserRoleList
	 * 
	 * @param userListDetails
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(value = "/getUserRoleList")
	public JSONObject getUserRoleList(@RequestBody JSONObject userRoleListDetails) {
		String sessionId = null;
		String serviceToken = null;
		JSONObject resultMap = new JSONObject();
		try {
			sessionId = userRoleListDetails.get("sessionId").toString();
			serviceToken = userRoleListDetails.get("serviceToken").toString();
			resultMap.put("sessionId", sessionId);
			resultMap.put("serviceToken", serviceToken);
			List<UserRoleEntity> userRoleDetailsEntityList = userRoleService.getUserRoleList();
			resultMap.put("roleDetails", userRoleDetailsEntityList);
			resultMap.put("status", Constants.SUCCESS);
		} catch (Exception e) {
			resultMap.put("status", Constants.FAIL);
			logger.error("Exception in UserRoleController.getUserRoleList(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return resultMap;
	}

}
