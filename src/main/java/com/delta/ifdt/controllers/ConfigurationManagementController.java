package com.delta.ifdt.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


import org.springframework.web.bind.annotation.RestController;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.models.PropertiesTemplateModel;
import com.delta.ifdt.util.LoadPropertyFiles;


@RestController
public class ConfigurationManagementController {
	
	static final Logger logger = LoggerFactory.getLogger(ConfigurationManagementController.class);

	
	/**
	 * This method will get details from application.properties file
	 * 
	 * @param propertiesPathDetails
	 * @return JSONObject
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@PostMapping(value = "/getConfigDetails")
	public JSONObject getConfigDetails(@RequestBody JSONObject propertiesPathDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		Map<String, Object> objMap = new HashMap<>();
		Map<String, Object> objMap1 = new HashMap<>();
		Map<String, Object> objMap2 = new HashMap<>();
		List<Map<String, Object>> details = new ArrayList<>();
		try
		{
			sessionId = propertiesPathDetails.get(Constants.SESSION_ID).toString();
			serviceToken = propertiesPathDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			LoadPropertyFiles.getInstance().init();
			PropertiesTemplateModel propertiesTemplateModel = new PropertiesTemplateModel();
			List<PropertiesTemplateModel> propertiesTemplateModelList = new ArrayList<>();
			List<PropertiesTemplateModel> propertiesTemplateModelList1 = new ArrayList<>();
			List<PropertiesTemplateModel> propertiesTemplateModelList2 = new ArrayList<>();
			propertiesTemplateModel.setLabel(Constants.BITEFILE_PATH_IFE);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.BITEFILE_PATH_IFE));
			propertiesTemplateModelList.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.AXINOMFILE_PATH_IFE);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.AXINOMFILE_PATH_IFE));
			propertiesTemplateModelList.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.SYSTEMLOG_FILE_PATH_IFE);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.SYSTEMLOG_FILE_PATH_IFE));
			propertiesTemplateModelList.add(propertiesTemplateModel);
			objMap.put(Constants.LABEL_DETAILS, propertiesTemplateModelList);
			objMap.put(Constants.PARAMETER_TYPE, "Source Path");
			details.add(objMap);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.BITEFILE_PATH_DESTINATION);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.BITEFILE_PATH_DESTINATION));
			propertiesTemplateModelList1.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.AXINOMFILE_PATH_IFE_DESTINATION);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.AXINOMFILE_PATH_IFE_DESTINATION));
			propertiesTemplateModelList1.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION));
			propertiesTemplateModelList1.add(propertiesTemplateModel);
			objMap1.put(Constants.LABEL_DETAILS, propertiesTemplateModelList1);
			objMap1.put(Constants.PARAMETER_TYPE, "Destination Path");
			details.add(objMap1);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT);
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT));
			propertiesTemplateModelList2.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel("SIZE_LIMIT_OF_TAR_FILE");
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty("size_limit_of_tar_file"));
			propertiesTemplateModelList2.add(propertiesTemplateModel);
			propertiesTemplateModel = new PropertiesTemplateModel();
			propertiesTemplateModel.setLabel("CHUNK_SIZE_OF_EACH_CHUNK");
			propertiesTemplateModel.setValue(LoadPropertyFiles.getInstance().getProperty("chunk_size_of_each_chunk"));
			propertiesTemplateModelList2.add(propertiesTemplateModel);
			objMap2.put(Constants.LABEL_DETAILS, propertiesTemplateModelList2);
			objMap2.put(Constants.PARAMETER_TYPE, "Ground Server");
			details.add(objMap2);
			resultMap.put(Constants.PROPERTIES_PATH_DETAILS, details);
			resultMap.put(Constants.STATUS, Constants.SUCCESS);

		}
		catch (Exception e) {
			logger.error(
					"Exception in ConfigurationManagementController.getConfigDetails(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
		}
		return resultMap;
	}



	/**
	 * This method will set details to application.properties file
	 * 
	 * @param propertiesPathDetails
	 * @return JSONObject
	 */
	@SuppressWarnings({ "static-access", "unchecked" })
	@PostMapping(value = "/saveConfigDetails")
	public JSONObject editConfigDetails(@RequestBody JSONObject propertiesPathDetails) {
		JSONObject resultMap = new JSONObject();
		String sessionId = null;
		String serviceToken = null;
		try {
			sessionId = propertiesPathDetails.get(Constants.SESSION_ID).toString();
			serviceToken = propertiesPathDetails.get(Constants.SERVICE_TOKEN).toString();
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			String type = propertiesPathDetails.get("type").toString();
			if (Constants.BITE_FILE_PATH_IFE.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.BITEFILE_PATH_IFE,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.AXINOM_FILE_PATH_IFE.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.AXINOMFILE_PATH_IFE,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.SYSTEM_LOG_FILE_PATH_IFE.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.SYSTEMLOG_FILE_PATH_IFE,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.BITE_FILE_PATH_DESTINATION.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.BITEFILE_PATH_DESTINATION,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.AXINOM_FILE_PATH_IFE_DESTINATION.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.AXINOMFILE_PATH_IFE_DESTINATION,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.SYSTEM_LOG_FILE_PATH_IFE_DESTINATION.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.SYSTEMLOG_FILE_PATH_IFE_DESTINATION,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.GROUND_SERVER_CONNECT_CHECK_COUNT.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty(Constants.GROUNDSERVER_CONNECT_CHECK_COUNT,
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.SIZE_LIMIT_OF_TAR_FILE.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty("size_limit_of_tar_file",
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			} else if (Constants.CHUNK_SIZE_OF_EACH_CHUNK.equals(type)) {
				LoadPropertyFiles.getInstance().init();
				LoadPropertyFiles.getInstance().setAppCodeProperty("chunk_size_of_each_chunk",
						propertiesPathDetails.get(Constants.PROPERTIES_PATH_DETAILS).toString());
			}
			
			resultMap.put(Constants.STATUS, Constants.SUCCESS);
			resultMap.put(Constants.SESSION_ID, sessionId);
			resultMap.put(Constants.SERVICE_TOKEN, serviceToken);
			return resultMap;
		} catch (Exception e) {
			logger.error(
					"Exception in ConfigurationManagementController.editPropertiesDetails(): " + ExceptionUtils.getFullStackTrace(e));
			resultMap.put(Constants.STATUS, Constants.FAIL);
			return resultMap;
		}
	}


	

}
