package com.delta.ifdt.service;

import java.util.List;

import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;

public interface FileTransferStatusDetailsService {
	List<FileTransferStatusDetailsEntity> getFileTransferDetails();

	boolean manualOffloadTransfer(String pendrivePath);

	boolean flightTransferIfeToGround();
}