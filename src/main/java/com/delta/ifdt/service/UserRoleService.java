package com.delta.ifdt.service;

import java.util.List;

import com.delta.ifdt.entities.UserRoleEntity;

public interface UserRoleService {

	List<UserRoleEntity> getUserRoleList();

}
