package com.delta.ifdt.service;

import java.util.Map;

import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;

public interface DashBoardDetailsService {
	
	Map<String,Object> getDashBoardDetails(String fromDate,String toDate);

	Map<String, Object> getGraphDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getFileGraphDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page,int count);

	Map<String, Object> getFailureReasonDetails(String type, int page, int count, String fromDate, String toDate);

	Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel, int page,
			int count);

}
