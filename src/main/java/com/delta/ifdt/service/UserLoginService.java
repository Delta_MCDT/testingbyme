package com.delta.ifdt.service;

import java.util.Date;
import java.util.Map;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;

public interface UserLoginService {

	UserManagementModel getUserDetailsByUserName(String userName);

	boolean validUser(UserManagementModel userEntity, String string);

	boolean setLastLogin(String userName, Date loginDate);

	boolean changePassword(String userName, String newPassword, boolean b);

	UserManagementEntity getUserDetailsByEmailId(String emailId);

	boolean mailNewPassword(String userFullName, String userName, String newPassword, String emailId) throws Exception;

	String resetPassword(String userName, String newPassword);

	Map<String, Object> getMenuList();

	void executeDbData();

}
