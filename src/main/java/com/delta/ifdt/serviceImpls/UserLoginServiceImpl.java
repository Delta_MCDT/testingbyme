package com.delta.ifdt.serviceImpls;

import java.util.Date;
import java.util.Map;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.delta.ifdt.entities.UserManagementEntity;

import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserLoginRepository;
import com.delta.ifdt.service.UserLoginService;
import com.delta.ifdt.util.EmailUtil;
import com.delta.ifdt.util.PasswordCryption;

@Service
@Transactional
public class UserLoginServiceImpl implements UserLoginService{

	static final Logger logger = LoggerFactory.getLogger(UserLoginServiceImpl.class);
	
	@Autowired
	UserLoginRepository userLoginRepository;
	
	@Autowired 
	EmailUtil emailUtil;
	
	@Override
	public UserManagementModel getUserDetailsByUserName(String userName) {
		UserManagementModel user = null;
		logger.info("UserLoginServiceImpl.getUserDetailsByUserName()");
			try {
				user = userLoginRepository.getUserDetails(userName);
			} catch (Exception e) {
				logger.error("Exception UserLoginServiceImpl.getUserDetailsByUserName(): " + ExceptionUtils.getFullStackTrace(e));
			}
			return user;
		}

	@Override
	public boolean validUser(UserManagementModel userEntity, String string) {
		logger.info("UserLoginServiceImpl.validUser()");
		logger.info("UserLoginServiceImpl.validUser()"+string);
		
		try {
			if (userEntity != null && PasswordCryption.decryptPasswordUI(string)
					.equals(PasswordCryption.decrypt(userEntity.getPassword()))) {
				logger.info("UserLoginServiceImpl.validUser() entitypwd"+userEntity.getPassword());
				logger.info("UserLoginServiceImpl.validUser() true");
				return true;
			}
		} catch (Exception e) {
			logger.error("Exception UserLoginServiceImpl.validUser(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return false;
	}

	@Override
	public boolean setLastLogin(String userName, Date loginDate) {
		boolean status = false;
		logger.info("UserLoginServiceImpl.setLastLogin()");
		try {
			return userLoginRepository.setLastLogin(userName, loginDate);
		} catch (Exception e) {
			logger.error("Exception UserLoginServiceImpl.setLastLogin(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

	@Override
	public boolean changePassword(String userName, String newPassword, boolean b) {
		boolean status = false;
		logger.info("UserLoginServiceImpl.changePassword()");
		try {
			return userLoginRepository.changePassword(userName, newPassword, b);
		} catch (Exception e) {
			logger.error("Exception UserLoginServiceImpl.changePassword(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

	@Override
	public UserManagementEntity getUserDetailsByEmailId(String emailId) {
		UserManagementEntity userDetailsEntity = null;
		logger.info("UserLoginServiceImpl.getUserDetailsByEmailId()");
		try {
			userDetailsEntity = userLoginRepository.getUserDetailsByEmailId(emailId);
		} catch (Exception e) {
			logger.error("Exception UserLoginServiceImpl.getUserDetailsByEmailId(): "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return userDetailsEntity;
	}

	@Override
	public boolean mailNewPassword(String userFullName, String userName, String newPassword, String emailId)
		throws Exception {
		logger.info("UserLoginServiceImpl.mailNewPassword()");
			try {
				
					String[] toList = emailId.split(",");

					StringBuilder bodyText = new StringBuilder();
					bodyText.append("Dear " + userFullName + ",");
					bodyText.append("<br/><br/>");
					bodyText.append("Please find login details for your account with SMART:");
					bodyText.append("<br/>");
					bodyText.append("Username : " + userName);
					bodyText.append("<br/>");
					bodyText.append("Password : " + newPassword);
					bodyText.append("<br/><br/>");
					bodyText.append("Regards");
					bodyText.append("<br/>");
					bodyText.append("SMART Administrator");
					String subject = "Login details for SMART";
					if (emailUtil.sendEmail(toList, null, null, subject, bodyText.toString(), true)) {
						return true;
					}
			} catch (AddressException e) {
				throw e;
			} catch (AuthenticationFailedException ae) {
				throw ae;
			} catch (MessagingException me) {
				throw me;
			}
			return false;
		}

	@Override
	public String resetPassword(String userName, String newPassword) {
		logger.info("UserLoginServiceImpl.resetPassword()");
		try {
			changePassword(userName, PasswordCryption.encryptPasswordUI(newPassword), false);

		} catch (Exception e) {
			logger.error("Exception UserLoginServiceImpl.resetPassword(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return newPassword;
	}

	@Override
	public Map<String, Object> getMenuList() {
		Map<String, Object> objList = null;
		try {
			objList=userLoginRepository.getMenuList();
			
	}catch (Exception e) {
		logger.error("Exception  getStatisticsDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objList;
	}

	@Override
	public void executeDbData() {
		userLoginRepository.getRoleData();
		userLoginRepository.getUserData();
		userLoginRepository.getHeaderData();
		
	}

}
