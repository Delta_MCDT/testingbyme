package com.delta.ifdt.serviceImpls;

import java.io.File;
import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.service.FlightOpenCloseService;
import com.delta.ifdt.util.CommonUtil;

@Service
public class FlightOpenCloseServiceImpl implements FlightOpenCloseService{
	
	static final Logger logger = LoggerFactory.getLogger(FlightOpenCloseServiceImpl.class);
	
	@Autowired
	FlightOpenCloseRepository objFlightOpenCloseRepository;
	
	@Override
	public File[] flightOpenCloseDetailsSave(String messageQue) {

		
		logger.info("FileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave()");
		
		File[] files=null;
		logger.info("FlightOpenCloseServiceImpl.flightOpenCloseDetailsSave()");
		try {
			
			if(StringUtils.isNotEmpty(messageQue)  && Constants.FLIGHT_OPEN.equalsIgnoreCase(messageQue)) {
				
				FlightOpenCloseEntity objFlightOpenCloseEntity=new FlightOpenCloseEntity();
				
				objFlightOpenCloseEntity.setFlightOpenTime(new Date());
				objFlightOpenCloseEntity.setCloseOpenStatus(Constants.FLIGHT_OPEN);
				objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objFlightOpenCloseEntity);
				
			}else if(StringUtils.isNotEmpty(messageQue)  && Constants.FLIGHT_CLOSE.equalsIgnoreCase(messageQue)){
				FlightOpenCloseEntity objFlightOpenCloseEntity =objFlightOpenCloseRepository.getLastFlightOpenDetails();
				objFlightOpenCloseEntity.setFlightCloseTime(new Date());
				objFlightOpenCloseEntity.setCloseOpenStatus(Constants.FLIGHT_CLOSE);
				objFlightOpenCloseEntity.setTransferStatus(Constants.READY_TO_TRANSMIT);
				FlightOpenCloseEntity objStausFlightOpenCloseEntity=objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objFlightOpenCloseEntity);
				
				if(objStausFlightOpenCloseEntity!=null) {
					 files =CommonUtil.getFilesDateInterval("/home/user/Documents", objStausFlightOpenCloseEntity.getFlightOpenTime(), objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					
				}
				
				
			}
			
			
		} catch (Exception e) {
			
			logger.error(
					"Exception  flightOpenCloseDetailsSave() in  FlightOpenCloseServiceImpl:" + ExceptionUtils.getFullStackTrace(e));
		}
		return files;
	}

}
