package com.delta.ifdt.serviceImpls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.models.BarchartDataModel;
import com.delta.ifdt.models.FileTransferStatusDetailsModel;
import com.delta.ifdt.models.PichartRestOffloadModel;
import com.delta.ifdt.repository.DashBoardDetailsRepository;
import com.delta.ifdt.repositoryImpl.DashBoardDetailsRepositoryImpl;
import com.delta.ifdt.service.DashBoardDetailsService;

@Service
public class DashBoardDetailsServiceImpl implements DashBoardDetailsService{
	
	
	static final Logger logger = LoggerFactory.getLogger(DashBoardDetailsRepositoryImpl.class);

	
	@Autowired
	DashBoardDetailsRepository objDashBoardDetailsRepository;
	
	@Override
	public Map<String, Object> getDashBoardDetails(String fromDate, String toDate) {

		
		Map<String, Object> objFinalMap=new HashMap<>();
		logger.info("DashBoardDetailsServiceImpl.getDashBoardDetails()");
		try {
			List<FileTransferStatusDetailsEntity> objDashBoardDetails=objDashBoardDetailsRepository.getDetailsOfDashBoard( fromDate,  toDate);
			
			
			//MCDT/MANUAL OFFLOAD
			
		long restTransferDatacount=	objDashBoardDetails.parallelStream().filter(x->(Constants.WAY_OF_TRANSPER_REST.equals(x.getModeOfTransfer()) && Constants.ACKNOWLEDGED.equals(x.getStatus()))).count();
		long manvalTransferDatacount=	objDashBoardDetails.parallelStream().filter(x->(Constants.WAY_OF_TRANSPER_OFFLOAD.equals(x.getModeOfTransfer()) && Constants.ACKNOWLEDGED.equals(x.getStatus()))).count();
		long totTransferDatacount=	objDashBoardDetails.parallelStream().filter(x->(Constants.ACKNOWLEDGED.equals(x.getStatus()))).count();
		
		
		
		float percentagerestData = (float) ((Double.valueOf(restTransferDatacount)
				* 100) / totTransferDatacount);
		float percentageoffloadData = (float) ((Double.valueOf(manvalTransferDatacount)
				* 100) / totTransferDatacount);
		
		PichartRestOffloadModel objPichartRestOffloadModel=new PichartRestOffloadModel();
		objPichartRestOffloadModel.setRestTransferDatacount(restTransferDatacount);
		objPichartRestOffloadModel.setManvalTransferDatacount(manvalTransferDatacount);
		objPichartRestOffloadModel.setTotTransferDatacount(totTransferDatacount);
		objPichartRestOffloadModel.setPercentagerestData(percentagerestData);
		objPichartRestOffloadModel.setPercentageoffloadData(percentageoffloadData);
		
		
		//Files TransFered
		long biteFileTransferedCount=objDashBoardDetails.parallelStream().filter(x->(Constants.ACKNOWLEDGED.equals(x.getStatus())) && Constants.BITE_LOG.equals(x.getFileType())).count();
		long axionumFileTransferedCount=objDashBoardDetails.parallelStream().filter(x->(Constants.ACKNOWLEDGED.equals(x.getStatus())) && Constants.AXINOM_LOG.equals(x.getFileType())).count();
		long syslogFileTransferedCount=objDashBoardDetails.parallelStream().filter(x->(Constants.ACKNOWLEDGED.equals(x.getStatus())) && Constants.SYSTEM_LOG.equals(x.getFileType())).count();
		long itulogFileTransferedCount=objDashBoardDetails.parallelStream().filter(x->(Constants.ACKNOWLEDGED.equals(x.getStatus())) && Constants.ITU_LOG.equals(x.getFileType())).count();
		
		BarchartDataModel objModel=new BarchartDataModel();
		List<BarchartDataModel> objList=new ArrayList<>();
		Map<String, Object> filesTransFeredMap = new HashedMap<>();
		
		
		List<String> objFilesNames = new ArrayList<>();
		objFilesNames.add("BITE Files");
		objFilesNames.add("Axinom Files");
		objFilesNames.add("System Logs");
		objFilesNames.add("ITU Logs");
		
		List<String> objDataList = new ArrayList<>();
		
		objDataList.add(String.valueOf(biteFileTransferedCount));
		objDataList.add(String.valueOf(axionumFileTransferedCount));
		objDataList.add(String.valueOf(syslogFileTransferedCount));
		objDataList.add(String.valueOf(itulogFileTransferedCount));
		
		
		objModel.setBackgroundColor("#11172b");
		objModel.setHoverBackgroundColor("#03185d");
		objModel.setLabel("");
		objModel.setData(objDataList);
		objList.add(objModel);
		
		
		
		filesTransFeredMap.put("labels", objFilesNames);
		filesTransFeredMap.put("datasets", objList);
		
		
		//Reason For Failure Map
		
		//Files TransFered
				long didNotReachGroundServerCount=objDashBoardDetails.parallelStream().filter(x->(Constants.FILE_NOT_RECEIVED.equals(x.getFailureReasonForOffLoad()))).count();
				long checkSumFailureCount=objDashBoardDetails.parallelStream().filter(x->(Constants.CHECKSUM_FAILURE.equals(x.getFailureReasonForOffLoad()))).count();
				long extractionFailureCount=objDashBoardDetails.parallelStream().filter(x->(Constants.EXTRACTION_FAILURE.equals(x.getFailureReasonForOffLoad()))).count();
				
				BarchartDataModel objFailureModel=new BarchartDataModel();
				List<BarchartDataModel> objFailureList=new ArrayList<>();
				Map<String, Object> failurefileTransFeredMap = new HashedMap<>();
				
				
				List<String> objFailureReasonsNames = new ArrayList<>();
				objFailureReasonsNames.add("GAS NOT RCVD");
				objFailureReasonsNames.add("CS FAILURE");
				objFailureReasonsNames.add("EX FAILURE");
				
				List<String> objFailureDataList = new ArrayList<>();
				
				objFailureDataList.add(String.valueOf(didNotReachGroundServerCount));
				objFailureDataList.add(String.valueOf(checkSumFailureCount));
				objFailureDataList.add(String.valueOf(extractionFailureCount));
				
				
				objFailureModel.setBackgroundColor("#11172b");
				objFailureModel.setHoverBackgroundColor("#03185d");
				objFailureModel.setLabel("");
				objFailureModel.setData(objFailureDataList);
				objFailureList.add(objFailureModel);
				
				
				
				failurefileTransFeredMap.put("labels", objFailureReasonsNames);
				failurefileTransFeredMap.put("datasets", objFailureList);
		
		
		objFinalMap.put("fileTransferDetails", filesTransFeredMap);
		objFinalMap.put("mcdtManualDetails", objPichartRestOffloadModel);
		objFinalMap.put("reasonsForFailure", failurefileTransFeredMap);
		
		} catch (Exception e) {
			logger.error("Exception  getDashBoardDetails() in  DashBoardDetailsServiceImpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return objFinalMap;
	}

	@Override
	public Map<String, Object> getGraphDetails(String type,int page,int count, String fromDate, String toDate) {
		Map<String, Object> objGraphDetails = null;
		try {
			 objGraphDetails=objDashBoardDetailsRepository.getGraphDetails(type,page,count,fromDate,toDate);
			
	}catch (Exception e) {
		logger.error("Exception  getGraphDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objGraphDetails;
	}

	@Override
	public Map<String, Object> getFileGraphDetails(String type,int page,int count, String fromDate, String toDate) {
		Map<String, Object> objFileGraphDetails = null;
		try {
			objFileGraphDetails=objDashBoardDetailsRepository.getFileGraphDetails(type,page,count,fromDate,toDate);
			
	}catch (Exception e) {
		logger.error("Exception  getFileGraphDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objFileGraphDetails;
	}

	@Override
	public Map<String, Object> getStatisticsDetails(String fromDate, String toDate, int page,int count) {
		Map<String, Object> objGraphDetails = null;
		try {
			 objGraphDetails=objDashBoardDetailsRepository.getStatisticsDetails(fromDate,toDate,page,count);
			
	}catch (Exception e) {
		logger.error("Exception  getStatisticsDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objGraphDetails;
	}

	@Override
	public Map<String, Object> getFailureReasonDetails(String type,int page,int count,String fromDate, String toDate) {
		Map<String, Object> objFileGraphDetails = null;
		try {
			objFileGraphDetails=objDashBoardDetailsRepository.getFailureReasonDetails(type,page,count,fromDate,toDate);
			
	}catch (Exception e) {
		logger.error("Exception  getFailureReasonDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objFileGraphDetails;
	}

	@Override
	public Map<String, Object> getStatisticsDetails(FileTransferStatusDetailsModel fileTransferStatusDetailsModel,
			int page, int count) {
		Map<String, Object> objGraphDetails = null;
		try {
			 objGraphDetails=objDashBoardDetailsRepository.getStatisticsDetails(fileTransferStatusDetailsModel,page,count);
			
	}catch (Exception e) {
		logger.error("Exception  getStatisticsDetails() in  DashBoardDetailsServiceImpl:"
				+ ExceptionUtils.getFullStackTrace(e));
	}
	return objGraphDetails;
	}
}
