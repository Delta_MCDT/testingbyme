package com.delta.ifdt.serviceImpls;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.delta.ifdt.constants.Constants;
import com.delta.ifdt.entities.ChunksDetailsEntity;
import com.delta.ifdt.entities.FileTransferStatusDetailsEntity;
import com.delta.ifdt.entities.FlightOpenCloseEntity;
import com.delta.ifdt.models.FlightMetaDataModel;
import com.delta.ifdt.models.ManualOffload;
import com.delta.ifdt.models.StatusQueryModel;
import com.delta.ifdt.repository.FileTransferStatusDetailsRepository;
import com.delta.ifdt.repository.FlightOpenCloseRepository;
import com.delta.ifdt.service.FileTransferStatusDetailsService;
import com.delta.ifdt.util.CommonUtil;
import com.delta.ifdt.util.DateUtil;
import com.delta.ifdt.util.FileUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class FileTransferStatusDetailsServiceimpl implements FileTransferStatusDetailsService {

	static final Logger logger = LoggerFactory.getLogger(FileTransferStatusDetailsServiceimpl.class);

	@Autowired
	FileTransferStatusDetailsRepository fileTransferStatusDetailsRepository;

	@Autowired
	FlightOpenCloseRepository objFlightOpenCloseRepository;
	@Autowired
	Environment env;
	
	@Autowired
	CommonUtil commonUtil;

	/**
	 * This method will transmit files status Details
	 * 
	 * @param
	 * @return boolean
	 */
	@Override
	public boolean flightTransferIfeToGround() {
		logger.info("FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround() start");

		boolean status = false;
		try {

			String groundServerIP = env.getProperty("GROUND_SERVER_IP");
			Integer groundServerPORT = Integer.valueOf(env.getProperty("GROUND_SERVER_PORT"));
			String connCheckCount = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
			String connCheckSomeTime = env.getProperty("GROUND_SERVER_CONNECT_CHECK_SOMETIME");

			logger.info(
					"FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround()  Properties Details groundServerIP :"
							+ groundServerIP + "groundServerPORT : " + groundServerPORT + " connCheckCount : "
							+ connCheckCount + " connCheckSomeTime : " + connCheckSomeTime);

			boolean checking_Ground_IP = false;
			boolean checking_Ground_IP_OffLoad = false;

			List<FlightOpenCloseEntity> receivedInFlightOpenCloseEntityList = objFlightOpenCloseRepository
					.getFlightDetails_with_received_main_entity();

		
			if (receivedInFlightOpenCloseEntityList != null && !receivedInFlightOpenCloseEntityList.isEmpty()) {
				try {
					checking_Ground_IP_OffLoad = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

				} catch (Exception e) {

					logger.error(
							"Exception FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround()  in groundServer Port Checking: "
									+ ExceptionUtils.getFullStackTrace(e));
				}

				if (checking_Ground_IP_OffLoad) {
					List<FileTransferStatusDetailsEntity> receivedInFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
							.getFileTransferDetailsReceivedStatus();

					if (receivedInFileTransferStatusDetailsEntity != null
							&& !receivedInFileTransferStatusDetailsEntity.isEmpty()) {
						receivedStatusServerQuerying(receivedInFileTransferStatusDetailsEntity,
								receivedInFlightOpenCloseEntityList);
					}

				}
			}
			//itu file creation
			
			List<FlightOpenCloseEntity> objItuFliOpenCleEtyDetLst = objFlightOpenCloseRepository
					.getFltDesClsRdyToItuTraStatus();

			if (objItuFliOpenCleEtyDetLst != null && !objItuFliOpenCleEtyDetLst.isEmpty()) {

				List<FileTransferStatusDetailsEntity> objOldItuList=fileTransferStatusDetailsRepository.getFileTransferDetails_ItuCheck();

				for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objItuFliOpenCleEtyDetLst) {
					long count=0;
					if(objOldItuList!=null && !objOldItuList.isEmpty()) {
						count=objOldItuList.parallelStream().filter(X->X.getOpenCloseDetilsId().intValue()==objLocFlightOpenCloseEntity.getId().intValue()).count();
					}
                       if(count==0) {
					FileTransferStatusDetailsEntity objItuEntity = itufileTraFmIFESertoLocPth(
							objLocFlightOpenCloseEntity);
                       }

								}

			}

			

			List<FileTransferStatusDetailsEntity> objOldList = fileTransferStatusDetailsRepository
					.getFileTransferDetails();

			if (objOldList != null && objOldList.size() > 0) {

				try {
					checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

				} catch (Exception e) {

					logger.error(
							"Exception FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround()  in groundServer Port Checking: "
									+ ExceptionUtils.getFullStackTrace(e));
				}

				logger.debug(
						"FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround() checking_Ground_IP Status"
								+ checking_Ground_IP);

				if (!checking_Ground_IP) {

					if (StringUtils.isNotEmpty(connCheckCount)) {
						int conncount = Integer.parseInt(connCheckCount);

						for (int intial = 1; intial <= conncount; intial++) {
							try {
								checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

							} catch (Exception e) {

								logger.error(
										"Exception FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround()  in groundServer Port Checking: "
												+ ExceptionUtils.getFullStackTrace(e));
							}
							if (!checking_Ground_IP) {
								logger.info(
										"FileTransferStatusDetailsServiceimpl.flightTransferIfeToGround()  Grond Server Conncection Checking Count "
												+ intial);
								continue;
							} else {
								break;
							}

						}
					}

					if (!checking_Ground_IP) {
						if (StringUtils.isNotEmpty(connCheckSomeTime)) {
							logger.info(
									"FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Grond Server Conncection Checking After This Many minutes "
											+ connCheckSomeTime);
							Thread.sleep(TimeUnit.MINUTES.toMillis(Integer.parseInt(connCheckCount)));
							flightTransferIfeToGround();
						}
					}

				}
			} else {
				checking_Ground_IP = true;
			}
			if (checking_Ground_IP) {
				logger.info(
						"FileTransferStatusDetailsServiceimpl.flightOpenCloseDetailsSave()  checking_Ground_IP Status"
								+ checking_Ground_IP);

				// get the data from FILE_TRANSFER_STATUS_DETAILS Table status is
				// ReadyToTransfer

				List<FlightOpenCloseEntity> objfileMainDetailsList = objFlightOpenCloseRepository
						.getFlightDetailsCloseAndReadyToTransper();

				// get the data from FlightOpenclose Table status is
				// received

				// query For OffLoad Status Details

				List<FileTransferStatusDetailsEntity> objOffLoadList = fileTransferStatusDetailsRepository
						.getFileTransferDetailsOffLoadStatus();

				if (objOffLoadList != null && objOffLoadList.size() > 0) {

					// Offloaded query
					boolean groundServerQueryStatus = groundServerQuery(objOffLoadList, objfileMainDetailsList);

					// for updated list from db
					objfileMainDetailsList = objFlightOpenCloseRepository.getFlightDetailsCloseAndReadyToTransper();
					objOffLoadList = fileTransferStatusDetailsRepository.getFileTransferDetailsOffLoadStatus();

				}

				if (objfileMainDetailsList != null && objfileMainDetailsList.size() > 0) {

					for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objfileMainDetailsList) {
						objOldList = fileTransferStatusDetailsRepository.getFileTransferDetails();

						if (objOldList != null && objOldList.size() > 0) {

							List<FileTransferStatusDetailsEntity> objTransperDetailsOldList = objOldList
									.stream().filter(x -> x.getOpenCloseDetilsId()
											.intValue() == objLocFlightOpenCloseEntity.getId().intValue())
									.collect(Collectors.toList());

							if (objTransperDetailsOldList != null && objTransperDetailsOldList.size() > 0) {
								boolean oldFileTransPerDetailsStatus = getStatusOfFileTransferDetailsOld(
										objTransperDetailsOldList, objLocFlightOpenCloseEntity);

								if (oldFileTransPerDetailsStatus) {
									FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = objLocFlightOpenCloseEntity;

									FileTransferStatusDetailsEntity objItuEntity = objTransperDetailsOldList.stream()
											.filter(X -> X.getFileType().equalsIgnoreCase(Constants.ITU_LOG))
											.collect(Collectors.toList()).stream().findAny().orElse(null);
									if (objItuEntity != null) {
										objLocFlightOpenCloseEntityStatusSet.setItuFileStatus(Constants.RECEIVED);
									}
									objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.RECEIVED);
									objFlightOpenCloseRepository
											.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);
								}
							}

						} else {
							// get the data from IFE To Local and creating tar files and update the db table
							List<FileTransferStatusDetailsEntity> objListTransferFiles = fileTransferFromIFEServertoLocalPath(
									objLocFlightOpenCloseEntity);

							if (objListTransferFiles != null && objListTransferFiles.size() > 0) {
								// send the tar files from local to ground server
								boolean newFileTransPerDetailsStatus = getStatusOfFileTransferDetailsNewFiles(
										objListTransferFiles, objLocFlightOpenCloseEntity);

								if (newFileTransPerDetailsStatus) {
									FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = objLocFlightOpenCloseEntity;
									if (Constants.READY_TO_TRANSMIT.equalsIgnoreCase(
											objLocFlightOpenCloseEntityStatusSet.getItuFileStatus())) {
										objLocFlightOpenCloseEntityStatusSet.setItuFileStatus(Constants.RECEIVED);
									}
									objLocFlightOpenCloseEntityStatusSet.setTransferStatus(Constants.RECEIVED);
									// update the transfer status
									objFlightOpenCloseRepository
											.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);

								}

							}

						}

					}

				}

			
			}
			status = true;

		} catch (Exception e) {
			status = false;
			logger.error("Exception for flightOpenCloseDetailsSave() in  FileTransferStatusDetailsServiceimpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

	/**
	 * This method will Query For Offload
	 * 
	 * @param finalOffloadedQueryList
	 * @return String
	 */
	public String restQueryForOffload(List<String> finalOffloadedQueryList) {

		// boolean status = false;
		logger.info("restQueryForOffload.flightOpenCloseDetailsSave()  finalOffloadedQuery" + finalOffloadedQueryList);
		String offloadedResponse = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);

			ManualOffload manualOffload = new ManualOffload();
			manualOffload.setFiles(finalOffloadedQueryList.toArray(new String[0]));

			HttpEntity<ManualOffload> requestEntity = new HttpEntity<>(manualOffload, headers);

			String offloadedURL = env.getProperty("OFFLOADED_SERVER_URL");

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);

			RestTemplate restTemplate =fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.postForEntity(offloadedURL, requestEntity, String.class);
			int statusCode = response.getStatusCodeValue();

			logger.debug("FileTransferStatusDetailsServiceimpl.restQueryForOffload()  statusCode" + statusCode);

			if (200 == statusCode) {
				offloadedResponse = response.getBody();

			}

		} catch (Exception e) {
			logger.error("Exception for restQueryForOffload() in  FileTransferStatusDetailsServiceimpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return offloadedResponse;
	}

	/**
	 * This method will Update system logs in DB
	 * 
	 * @param objLocFileTransferStatusDetailsEntity
	 * @return FileTransferStatusDetailsEntity
	 */
	public FileTransferStatusDetailsEntity sysItuLogsUpdationInDB(
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity) {

		boolean systemUpstatus = false;
		FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = null;
		logger.info("FileTransferStatusDetailsServiceimpl.sysLogsUpdationInDB()  objLocFileTransferStatusDetailsEntity"
				+ objLocFileTransferStatusDetailsEntity);

		try {

			File sysTarFilename = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
			long Tar_fileSize = (long) sysTarFilename.length();
			long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
			long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
			long sizeLimit = sizelimitMb * (1000 * 1000);
			long chunk_size = chunkfilemb * (1000 * 1000);

			if (Tar_fileSize > sizeLimit) {

				Map<String, Object> chunkDetails = commonUtil
						.Chunking_System_logs(objLocFileTransferStatusDetailsEntity.getTarFilePath(), chunk_size);
				int totChunkCount = (Integer) chunkDetails.get("totCount");
				Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get("pathDetails");

				if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

					objLocFileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

					objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
							.saveFileTransferStatusDetails(objLocFileTransferStatusDetailsEntity);

					for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
						String chunk_name = objMap.getValue();
						ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
						objChunksDetailsEntity.setChunkCount(objMap.getKey());
						objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
						objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
						objChunksDetailsEntity
								.setFileTransferStausDetailsEntityId(objFileTransferStatusDetailsEntity.getId());
						objChunksDetailsEntity.setChunkName(
								chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
						String chunkTarFile = objMap.getValue();
						MessageDigest md = MessageDigest.getInstance("MD5");
						String chunk_checkSum = commonUtil.computeFileChecksum(md, chunkTarFile);
						objChunksDetailsEntity.setChunk_Checksum(chunk_checkSum);
						objChunksDetailsEntity = fileTransferStatusDetailsRepository
								.saveChunkFileDetails(objChunksDetailsEntity);

					}
					systemUpstatus = true;

				}

			} else {
				objLocFileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
				objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
				fileTransferStatusDetailsRepository
						.saveFileTransferStatusDetails(objLocFileTransferStatusDetailsEntity);
				systemUpstatus = true;
			}

		} catch (Exception e) {

			systemUpstatus = false;
			logger.error("Exception  sysLogsUpdationInDB() in  FileTransferStatusDetailsServiceimpl:"
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return objFileTransferStatusDetailsEntity;
	}

	/**
	 * This method will transfer file from IFEServer to Localpath
	 * 
	 * @param objStausFlightOpenCloseEntity
	 * @return FileTransferStatusDetailsEntity
	 */
	public List<FileTransferStatusDetailsEntity> fileTransferFromIFEServertoLocalPath(
			FlightOpenCloseEntity objStausFlightOpenCloseEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()  objLocFileTransferStatusDetailsEntity"
						+ objStausFlightOpenCloseEntity);
		boolean status = false;
		List<FileTransferStatusDetailsEntity> objListDetils = new ArrayList<>();
		try {

			String biteFilePathSource = env.getProperty("BITE_FILE_PATH_IFE");
			String axinomFilePathSource = env.getProperty("AXINOM_FILE_PATH_IFE");
			String systemLogFilePathSource = env.getProperty("SYSTEM_LOG_FILE_PATH_IFE");
			String ituLogFilePathSource = env.getProperty("ITU_LOG_FILE_PATH_IFE");

			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			StringBuilder bitedestpaths = new StringBuilder();
			StringBuilder axinomdestpaths = new StringBuilder();
			StringBuilder syslogdestpaths = new StringBuilder();
			StringBuilder itulogdestpaths = new StringBuilder();

			String biteFilePathDest = bitedestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("BITE_FILE_PATH_DESTINATION")).toString();
			String axinomFilePathDest = axinomdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).toString();
			String systemLogFilePathDest = syslogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			String ituLogFilePathDest = itulogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			Date date = new Date();

			/* creating the Bite tar File */
			if (StringUtils.isNotEmpty(biteFilePathSource)) {

				File biteFileSource = new File(biteFilePathSource);
				if (biteFileSource.exists()) {

					String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
					StringBuilder folderDate = new StringBuilder();
					biteFilePathDest = folderDate.append(biteFilePathDest).append(Constants.BITE_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory"
									+ biteFilePathDest);
					File biteFileDestination = new File(biteFilePathDest);

					if (!biteFileDestination.exists()) {
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory ");
						biteFileDestination.mkdirs();
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory "
										+ biteFileDestination + " Creted Successfuly");
					}

					/* To create a tar file with the file type and date */
					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(biteFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatess(biteFilePathSource, biteFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							biteFilePathDest);

					boolean statusOfTarFile = CommonUtil.createTarFile(biteFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bitelog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathDest
								.substring(biteFilePathDest.lastIndexOf('/') + 1, biteFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}
				}

			}
			/* creating the Axinom tar File */

			if (StringUtils.isNotEmpty(axinomFilePathSource)) {

				File axinomFileSource = new File(axinomFilePathSource);

				if (axinomFileSource.exists()) {

					String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
					StringBuilder folderDate = new StringBuilder();
					axinomFilePathDest = folderDate.append(axinomFilePathDest).append(Constants.AXINOM_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_Folder_in_local_directory"
									+ axinomFilePathDest);

					File axinomFileDestination = new File(axinomFilePathDest);

					if (!axinomFileDestination.exists()) {
						axinomFileDestination.mkdirs();
					}

					/* To create a tar file with the file type and date */
					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(axinomFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, axinomFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							axinomFilePathDest);
					boolean statusOfTarFile = CommonUtil.createTarFile(axinomFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(axinomFilePathDest
								.substring(axinomFilePathDest.lastIndexOf('/') + 1, axinomFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}

				}

			}
			/* creating the Syslogs tar File */

			if (StringUtils.isNotEmpty(systemLogFilePathSource)) {

				File systemLogFileSource = new File(systemLogFilePathSource);

				if (systemLogFileSource.exists()) {
					String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
					StringBuilder folderDate = new StringBuilder();
					systemLogFilePathDest = folderDate.append(systemLogFilePathDest).append(Constants.SYSTEM_LOG)
							.append("_").append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(
									DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime()))
							.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ systemLogFilePathDest);

					File systemLogFileDestination = new File(systemLogFilePathDest);

					if (!systemLogFileDestination.exists()) {
						systemLogFileDestination.mkdirs();
					}

					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(systemLogFilePathDest).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatess(systemLogFilePathSource, systemLogFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							systemLogFilePathDest);

					boolean statusOfTarFile = CommonUtil.createTarFile(systemLogFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Systemlog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(systemLogFilePathDest
								.substring(systemLogFilePathDest.lastIndexOf('/') + 1, systemLogFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						// system logs for again rest call
						File tarFile = new File(destFolder);
						long tarFileSize = (long) tarFile.length();
						long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
						long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
						long size_limit = sizelimitMb * (1000 * 1000);
						long chunk_size = chunkfilemb * (1000 * 1000);

						if (tarFileSize > size_limit) {

							logger.info(
									"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Tar_fileSize is greater than size_limit "
											+ tarFileSize);
							Map<String, Object> chunkDetails = commonUtil.Chunking_System_logs(destFolder, chunk_size);
							int totChunkCount = (Integer) chunkDetails.get("totCount");
							Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails
									.get("pathDetails");

							if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

								fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

								FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
										.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
								objListDetils.add(objFileTransferStatusDetailsEntity);

								for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
									String chunk_name = objMap.getValue();
									ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
									objChunksDetailsEntity.setChunkCount(objMap.getKey());
									objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
									objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
									objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
											objFileTransferStatusDetailsEntity.getId());
									objChunksDetailsEntity.setChunkName(
											chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
									String chunkTarFile = objMap.getValue();

									String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
									objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
									objChunksDetailsEntity = fileTransferStatusDetailsRepository
											.saveChunkFileDetails(objChunksDetailsEntity);

								}

							}

						} else {
							fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
							FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
									.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
							objListDetils.add(objFileTransferStatusDetailsEntity);
						}

					}

				}

			}

			if (StringUtils.isNotEmpty(ituLogFilePathSource)
					&& Constants.READY_TO_TRANSMIT.equalsIgnoreCase(objStausFlightOpenCloseEntity.getItuFileStatus())) {

				File ituLogFileSource = new File(ituLogFilePathSource+objStausFlightOpenCloseEntity.getItuFolderName());

				if (ituLogFileSource.exists()) {
					StringBuilder folderDate = new StringBuilder();
					ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(new Date()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ ituLogFilePathDest);

					File ituLogFileDestination = new File(ituLogFilePathDest);

					if (!ituLogFileDestination.exists()) {
						ituLogFileDestination.mkdirs();
					}

					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(ituLogFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() ItuLog_Folder_in_local_directory"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatessItu(ituLogFileSource.toString(), ituLogFilePathDest,
							objStausFlightOpenCloseEntity.getFlightCloseTime(),
							objStausFlightOpenCloseEntity.getNextFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity, ituLogFilePathDest);

					boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() Systemlog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
								.substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						// system logs for again rest call
						File tarFile = new File(destFolder);
						long tarFileSize = (long) tarFile.length();
						long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
						long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
						long size_limit = sizelimitMb * (1000 * 1000);
						long chunk_size = chunkfilemb * (1000 * 1000);

						if (tarFileSize > size_limit) {

							logger.info(
									"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() Tar_fileSize is greater than size_limit "
											+ tarFileSize);
							Map<String, Object> chunkDetails = commonUtil.Chunking_System_logs(destFolder, chunk_size);
							int totChunkCount = (Integer) chunkDetails.get("totCount");
							Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails
									.get("pathDetails");

							if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

								fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

								FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
										.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

								for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
									String chunk_name = objMap.getValue();
									ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
									objChunksDetailsEntity.setChunkCount(objMap.getKey());
									objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
									objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
									objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
											objFileTransferStatusDetailsEntity.getId());
									objChunksDetailsEntity.setChunkName(
											chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
									String chunkTarFile = objMap.getValue();

									String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
									objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
									objChunksDetailsEntity = fileTransferStatusDetailsRepository
											.saveChunkFileDetails(objChunksDetailsEntity);

								}

							}

						} else {
							fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
							FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
									.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						}

					}

				}

			}

		} catch (Exception e) {
			objListDetils = null;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()  : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return objListDetils;
	}

	/**
	 * This method will transfer file from IFEServer to Localpath
	 * 
	 * @param objItuStausFlightOpenCloseEntity
	 * @return FileTransferStatusDetailsEntity
	 */
	public FileTransferStatusDetailsEntity itufileTraFmIFESertoLocPth(
			FlightOpenCloseEntity objItuStausFlightOpenCloseEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth()  objItuStausFlightOpenCloseEntity"
						+ objItuStausFlightOpenCloseEntity);
		FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = null;
		try {
			String ituLogFilePathSource = env.getProperty("ITU_LOG_FILE_PATH_IFE");

			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			
			StringBuilder itulogdestpaths = new StringBuilder();

			

			String ituLogFilePathDest = itulogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			Date date = new Date();
			/* creating the itulogs tar File */

			if (StringUtils.isNotEmpty(ituLogFilePathDest)) {

				File ituLogFileSource = new File(ituLogFilePathSource+objItuStausFlightOpenCloseEntity.getItuFolderName());

				if (ituLogFileSource.exists()) {
					StringBuilder folderDate = new StringBuilder();
					ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG).append("_")
							.append(objItuStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objItuStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ ituLogFilePathDest);

					File ituLogFileDestination = new File(ituLogFilePathDest);

					if (!ituLogFileDestination.exists()) {
						ituLogFileDestination.mkdirs();
					}

					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(ituLogFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() ItuLog_Folder_in_local_directory"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatessItu(ituLogFileSource.toString(), ituLogFilePathDest,
							objItuStausFlightOpenCloseEntity.getFlightCloseTime(),
							objItuStausFlightOpenCloseEntity.getNextFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					metadataJsonFileCreationAndTransfer(objItuStausFlightOpenCloseEntity, ituLogFilePathDest);

					boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() Systemlog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
								.substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objItuStausFlightOpenCloseEntity.getId());

						// system logs for again rest call
						File tarFile = new File(destFolder);
						long tarFileSize = (long) tarFile.length();
						long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
						long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
						long size_limit = sizelimitMb * (1000 * 1000);
						long chunk_size = chunkfilemb * (1000 * 1000);

						if (tarFileSize > size_limit) {

							logger.info(
									"FileTransferStatusDetailsServiceimpl.itufileTraFmIFESertoLocPth() Tar_fileSize is greater than size_limit "
											+ tarFileSize);
							Map<String, Object> chunkDetails = commonUtil.Chunking_System_logs(destFolder, chunk_size);
							int totChunkCount = (Integer) chunkDetails.get("totCount");
							Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails
									.get("pathDetails");

							if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

								fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

								objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
										.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

								for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
									String chunk_name = objMap.getValue();
									ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
									objChunksDetailsEntity.setChunkCount(objMap.getKey());
									objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
									objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
									objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
											objFileTransferStatusDetailsEntity.getId());
									objChunksDetailsEntity.setChunkName(
											chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
									String chunkTarFile = objMap.getValue();

									String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
									objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
									objChunksDetailsEntity = fileTransferStatusDetailsRepository
											.saveChunkFileDetails(objChunksDetailsEntity);

								}

							}

						} else {
							fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
							 objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
									.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						}

					}

				}

			}

		} catch (Exception e) {
			objFileTransferStatusDetailsEntity = null;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()  : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return objFileTransferStatusDetailsEntity;
	}

	/**
	 * This method will return File Transfer Details
	 * 
	 * @param
	 * @return FileTransferStatusDetailsEntity
	 */
	@Override
	public List<FileTransferStatusDetailsEntity> getFileTransferDetails() {

		logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() ");
		List<FileTransferStatusDetailsEntity> objList = null;
		boolean fileTransferStatus = false;
		boolean chunk_fileTransferStatus = false;
		logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()");

		try {
			String groundServerIP = env.getProperty("GROUND_SERVER_IP");
			Integer groundServerPORT = Integer.valueOf(env.getProperty("GROUND_SERVER_PORT"));

			objList = fileTransferStatusDetailsRepository.getFileTransferDetails();

			if (objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */

					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					boolean checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

					logger.debug("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() checking_Ground_IP"
							+ checking_Ground_IP);

					if (checking_Ground_IP) {

						if (!Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
									objLocFileTransferStatusDetailsEntity);

							if (fileTransferStatus) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updationStatus = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updationStatus) {

									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
								}

							}
						} else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

							boolean statusChunkTransfer = false;

							List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
									.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

							if (objListChunks != null && objListChunks.size() > 0) {

								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
											objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
									if (chunk_fileTransferStatus) {
										objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
										fileTransferStatusDetailsRepository
												.saveChunkFileDetails(objChunksDetailsEntity);
									} else {
										statusChunkTransfer = true;
									}

								}

								if (!statusChunkTransfer) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									// chunk files delete
									for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

										FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

									}

									// parent files delete
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);

								}

							}

						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getFileTransferDetails() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return objList;
	}

	/*
	 * This method(tarTransfer_to_ground_server_Bite_Axinom_Files) used for both the
	 * bite and axinom log files to send ground server
	 */

	/**
	 * This method will tarTransfer_to_ground_server_Bite_Axinom_Files used for both
	 * the bite and axinom log files to send ground server
	 * 
	 * @param objLocFileTransferStatusDetailsEntity
	 * @return boolean
	 */
	public boolean tarTransfer_to_ground_server_Bite_Axinom_Files(
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_Bite_Axinom_Files() bite,axinom to send rest api "
						+ objLocFileTransferStatusDetailsEntity);
		boolean status = false;

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

			body.add("file", new FileSystemResource(objLocFileTransferStatusDetailsEntity.getTarFilePath()));
			body.add("chunk_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
			body.add("file_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
			body.add("current_part", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
			body.add("total_parts", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
			body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);

			logger.info(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_Bite_Axinom_Files() TarFilePath, checksum,total_chunks,current_chunk "
							+ objLocFileTransferStatusDetailsEntity.getTarFilePath() + ","
							+ objLocFileTransferStatusDetailsEntity.getChecksum() + ","
							+ objLocFileTransferStatusDetailsEntity.getChunkSumCount() + ","
							+ objLocFileTransferStatusDetailsEntity.getChunkSumCount());

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

			String serverUrl = env.getProperty("GROUND_SERVER_URL");

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);
			requestFactory.setConnectTimeout(10000);
			requestFactory.setReadTimeout(120000);

			RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);

			ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);

			int statusCode = response.getStatusCodeValue();

			logger.info(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_Bite_Axinom_Files() statusCode"
							+ statusCode);

			String responseBodyOfreq = response.getBody();

			JsonObject jsonObject = new JsonParser().parse(responseBodyOfreq).getAsJsonObject();

			if (jsonObject != null) {
				if (200 == statusCode
						&& jsonObject.get("status").getAsString().contains(Constants.SUCCESS_STATUS_CODE)) {
					status = true;

				} else {
					status = false;
				}
			}
		}

		catch (ResourceAccessException e) {
			if (e.getMessage().contains("Read timed out")) {

				/*ground_Server_rest_apiQuery(objLocFileTransferStatusDetailsEntity.getTarFilename());*/
				// tarTransfer_to_ground_server_Bite_Axinom_Files(objLocFileTransferStatusDetailsEntity);
			}
		}

		catch (Exception e) {
			status = false;
			logger.error(
					"Exception FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_Bite_Axinom_Files() : "
							+ ExceptionUtils.getFullStackTrace(e));

		}

		return status;

	}

	/**
	 * This method will Transfer System logs files to ground server without chunks
	 * 
	 * @param objLocFileTransferStatusDetailsEntity
	 * @return boolean
	 */
	public boolean tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
			FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks() system_files_without_chunks"
						+ objLocFileTransferStatusDetailsEntity);
		boolean status = false;

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

			body.add("file", new FileSystemResource(objLocFileTransferStatusDetailsEntity.getTarFilePath()));
			body.add("chunk_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
			body.add("file_checksum", objLocFileTransferStatusDetailsEntity.getChecksum());
			body.add("current_part", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
			body.add("total_parts", objLocFileTransferStatusDetailsEntity.getChunkSumCount());
			body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);

			logger.debug(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks() TarFilePath, checksum,total_chunks,current_chunk "
							+ objLocFileTransferStatusDetailsEntity.getTarFilePath() + ","
							+ objLocFileTransferStatusDetailsEntity.getChecksum() + ","
							+ objLocFileTransferStatusDetailsEntity.getChunkSumCount() + ","
							+ objLocFileTransferStatusDetailsEntity.getChunkSumCount());

			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

			String serverUrl = env.getProperty("GROUND_SERVER_URL");

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);

			RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
			int statusCode = response.getStatusCodeValue();

			logger.debug(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks() statusCode"
							+ statusCode);

			String responseBodyOfreq = response.getBody();
			
			JsonObject jsonObject = new JsonParser().parse(responseBodyOfreq).getAsJsonObject();

			if (jsonObject != null) {
				if (200 == statusCode
						&& jsonObject.get("status").getAsString().contains(Constants.SUCCESS_STATUS_CODE)) {
					status = true;
				} else {
					status = false;
				}
			}
		} catch (Exception e) {
			status = false;
			logger.error(
					"Exception FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks() : "
							+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;

	}

	// This method is used to transfer the server logs to the ground server

	/**
	 * This method will transfer the server logs to the ground server
	 * 
	 * @param objChunksDetailsEntity,
	 *            objFileTransferStatusDetailsEntity
	 * @return boolean
	 */
	public boolean tarTransfer_to_ground_server_SystemItu_Log_Files(ChunksDetailsEntity objChunksDetailsEntity,
			FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItu_Log_Files() system_logfiles_with_chunks"
						+ objFileTransferStatusDetailsEntity);
		boolean status = false;

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

			/*
			 * body.add("file", new
			 * FileSystemResource(objChunksDetailsEntity.getChunkTarFilePath()));
			 * body.add("checksum", objChunksDetailsEntity.getChunk_Checksum());
			 * body.add("total_chunks",
			 * objFileTransferStatusDetailsEntity.getChunkSumCount());
			 * body.add("current_chunk", objChunksDetailsEntity.getChunkCount());
			 */

			body.add("file", new FileSystemResource(objChunksDetailsEntity.getChunkTarFilePath()));
			body.add("chunk_checksum", objChunksDetailsEntity.getChunk_Checksum());
			body.add("file_checksum", objFileTransferStatusDetailsEntity.getChecksum());
			body.add("current_part", objChunksDetailsEntity.getChunkCount());
			body.add("total_parts", objFileTransferStatusDetailsEntity.getChunkSumCount());
			body.add("offload_type", Constants.OFFLOAD_TYPE_AUTO);
			logger.debug(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItu_Log_Files() chunk_TarFilePath, chunk_checksum,total_chunks,current_chunk "
							+ objChunksDetailsEntity.getChunkTarFilePath() + ","
							+ objChunksDetailsEntity.getChunk_Checksum() + ","
							+ objFileTransferStatusDetailsEntity.getChunkSumCount() + ","
							+ objChunksDetailsEntity.getChunkCount());
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

			String serverUrl = env.getProperty("GROUND_SERVER_URL");

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);

			RestTemplate restTemplate = fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
			int statusCode = response.getStatusCodeValue();
			logger.debug(
					"FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItu_Log_Files() statusCode"
							+ statusCode);

			String responseBodyOfreq = response.getBody();

			JsonObject jsonObject = new JsonParser().parse(responseBodyOfreq).getAsJsonObject();

			if (jsonObject != null) {
				if (200 == statusCode
						&& jsonObject.get("status").getAsString().contains(Constants.SUCCESS_STATUS_CODE)) {
					status = true;
				} else {
					status = false;
					
				}
			}
		} catch (Exception e) {
			status = false;
			logger.error(
					"Exception FileTransferStatusDetailsServiceimpl.tarTransfer_to_ground_server_SystemItu_Log_Files() : "
							+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;

	}

	/**
	 * This method will return the status of File Transfer Details
	 * 
	 * @param objFileTransferStatusDetailsEntityList
	 * @return boolean
	 */
	public boolean getStatusOfFileTransferDetails(
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList) {

		logger.info("FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetails() system_logfiles_with_chunks"
				+ objFileTransferStatusDetailsEntityList);
		List<FileTransferStatusDetailsEntity> objList = null;
		boolean fileTransferStatus = false;
		boolean chunkFileTransferStatus = false;
		boolean finalTransferStatus = false;

		try {
			String groundServerIP = env.getProperty("GROUND_SERVER_IP");
			Integer groundServerPORT = Integer.valueOf(env.getProperty("GROUND_SERVER_PORT"));

			objList = objFileTransferStatusDetailsEntityList;

			if (objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */

					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					boolean checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

					logger.debug("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() checking_Ground_IP"
							+ checking_Ground_IP);

					if (checking_Ground_IP) {

						if (!Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
									objLocFileTransferStatusDetailsEntity);
							if (!fileTransferStatus) {

								String connCheckCount = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
								if (StringUtils.isNotEmpty(connCheckCount)) {
									int conncount = Integer.parseInt(connCheckCount);

									for (int intial = 1; intial <= conncount; intial++) {
										fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
												objLocFileTransferStatusDetailsEntity);
										if (!fileTransferStatus) {
											continue;
										} else {
											break;
										}

									}
								}

							}

							if (fileTransferStatus) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updation_status) {

									/*
									 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
									 * biteFilePathDest.length())
									 */
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
								}

							}
						} else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

							boolean statusChunkTransfer = false;

							List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
									.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

							if (objListChunks != null && objListChunks.size() > 0) {

								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									chunkFileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
											objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
									if (chunkFileTransferStatus) {
										objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
										fileTransferStatusDetailsRepository
												.saveChunkFileDetails(objChunksDetailsEntity);
									} else {
										statusChunkTransfer = true;
									}

								}

								if (!statusChunkTransfer) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									// chunk files delete
									for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

										FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

									}

									// parent files delete
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);

								}

							}

						}
					}
					finalTransferStatus = true;

				}
			}
		} catch (Exception e) {
			finalTransferStatus = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetails() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return finalTransferStatus;
	}

	/*
	 * This method is used to send the old files which are having Ready to Transmit
	 * status and we can send through based on the File type
	 */
	/**
	 * This method will send the old files which are having Ready to Transmit status
	 * 
	 * @param objFileTransferStatusDetailsEntityList,
	 *            objLocFlightOpenCloseEntity
	 * @return boolean
	 */
	public boolean getStatusOfFileTransferDetailsOld(
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
			FlightOpenCloseEntity objLocFlightOpenCloseEntity) {

		logger.info("FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld()");

		List<FileTransferStatusDetailsEntity> objList = null;
		boolean fileTransferStatus = false;
		boolean chunk_fileTransferStatus = false;
		boolean finalTransferStatus = false;

		boolean biteTransferStatus = true;
		boolean axionumTransferStatus = true;
		boolean systransferstatus = true;
		boolean itutransferstatus = true;

		try {
			String groundServerIP = env.getProperty("GROUND_SERVER_IP");
			Integer groundServerPORT = Integer.valueOf(env.getProperty("GROUND_SERVER_PORT"));

			objList = objFileTransferStatusDetailsEntityList;

			if (objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */

					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					boolean checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

					logger.debug("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() checking_Ground_IP"
							+ checking_Ground_IP);

					if (checking_Ground_IP) {

						if (Constants.BITE_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							biteTransferStatus = false;
							fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
									objLocFileTransferStatusDetailsEntity);

							if (fileTransferStatus) {
								objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updation_status) {

									/*
									 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
									 * biteFilePathDest.length())
									 */
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
									biteTransferStatus = true;

								}

							}
						} else if (Constants.AXINOM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							axionumTransferStatus = false;
							fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
									objLocFileTransferStatusDetailsEntity);

							if (fileTransferStatus) {
								objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updation_status) {

									/*
									 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
									 * biteFilePathDest.length())
									 */
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
									axionumTransferStatus = true;
								}

							}
						} else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							systransferstatus = false;
							// gfh
							if (Constants.WAY_OF_TRANSPER_OFFLOAD
									.equals(objLocFileTransferStatusDetailsEntity.getModeOfTransfer())) {
								FileTransferStatusDetailsEntity objnewDetais = sysItuLogsUpdationInDB(
										objLocFileTransferStatusDetailsEntity);
								if (objnewDetais != null) {
									// some problem will occurs
									objLocFileTransferStatusDetailsEntity = objnewDetais;
								}
							}
							boolean statusChunkTransfer = false;

							List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
									.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

							if (objListChunks != null && objListChunks.size() > 0) {

								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
											objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
									if (chunk_fileTransferStatus) {
										objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
										fileTransferStatusDetailsRepository
												.saveChunkFileDetails(objChunksDetailsEntity);
									} else {
										statusChunkTransfer = true;
									}

								}

								if (!statusChunkTransfer) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									// chunk files delete
									for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

										FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

									}

									// parent files delete
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
									systransferstatus = true;
								}

							}
							// with out chunks sys log
							else {

								fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
										objLocFileTransferStatusDetailsEntity);

								// checking connection repetedly based upon count
								if (!fileTransferStatus) {

									String connCheckCount = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
									if (StringUtils.isNotEmpty(connCheckCount)) {
										int conncount = Integer.parseInt(connCheckCount);

										for (int intial = 1; intial <= conncount; intial++) {
											fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
													objLocFileTransferStatusDetailsEntity);
											if (!fileTransferStatus) {
												continue;
											} else {
												break;
											}

										}
									}

								}

								if (fileTransferStatus) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									if (updation_status) {

										/*
										 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
										 * biteFilePathDest.length())
										 */
										String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
												.getTarFilePath();
										String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
												.getTarFilePath()
												.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
														objLocFileTransferStatusDetailsEntity.getOriginalFilename());
										FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
										FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
										systransferstatus = true;
									}

								}

							}

						} else if (Constants.ITU_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
							itutransferstatus = false;
							// gfh
							if (Constants.WAY_OF_TRANSPER_OFFLOAD
									.equals(objLocFileTransferStatusDetailsEntity.getModeOfTransfer())) {
								FileTransferStatusDetailsEntity objnewDetais = sysItuLogsUpdationInDB(
										objLocFileTransferStatusDetailsEntity);
								if (objnewDetais != null) {
									// some problem will occurs
									objLocFileTransferStatusDetailsEntity = objnewDetais;
								}
							}
							boolean statusChunkTransfer = false;

							List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
									.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

							if (objListChunks != null && objListChunks.size() > 0) {

								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
											objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
									if (chunk_fileTransferStatus) {
										objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
										fileTransferStatusDetailsRepository
												.saveChunkFileDetails(objChunksDetailsEntity);
									} else {
										statusChunkTransfer = true;
									}

								}

								if (!statusChunkTransfer) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									// chunk files delete
									for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

										FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

									}

									// parent files delete
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
									itutransferstatus = true;
								}

							}
							// with out chunks Itu log
							else {

								fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
										objLocFileTransferStatusDetailsEntity);

								// checking connection repetedly based upon count
								if (!fileTransferStatus) {

									String connCheckCount = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
									if (StringUtils.isNotEmpty(connCheckCount)) {
										int conncount = Integer.parseInt(connCheckCount);

										for (int intial = 1; intial <= conncount; intial++) {
											fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
													objLocFileTransferStatusDetailsEntity);
											if (!fileTransferStatus) {
												continue;
											} else {
												break;
											}

										}
									}

								}

								if (fileTransferStatus) {

									objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
									boolean updation_status = fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
									if (updation_status) {

										/*
										 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
										 * biteFilePathDest.length())
										 */
										String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
												.getTarFilePath();
										String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
												.getTarFilePath()
												.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
														objLocFileTransferStatusDetailsEntity.getOriginalFilename());
										FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
										FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
										itutransferstatus = true;
									}

								}

							}

						}
					}

				}
				if (biteTransferStatus && axionumTransferStatus && systransferstatus && itutransferstatus) {
					finalTransferStatus = true;
				}

			}
		} catch (Exception e) {
			finalTransferStatus = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return finalTransferStatus;

	}

	/**
	 * This method will return Status Of File Transfer Details
	 * 
	 * @param objFileTransferStatusDetailsEntityList,
	 *            objLocFlightOpenCloseEntity, offloadPath
	 * @return boolean
	 */
	public boolean getStatusOfFileTransferDetailsOld_manual(
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
			FlightOpenCloseEntity objLocFlightOpenCloseEntity, String offloadPath) {

		logger.info("FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld_manual()");
		List<FileTransferStatusDetailsEntity> objList = null;

		boolean finalTransferStatus = false;
		logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()");

		try {

			objList = objFileTransferStatusDetailsEntityList;

			if (objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */

					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					if (Constants.BITE_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

						/*
						 * FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.
						 * getTarFilePath(), Constants.OFFLOAD_LOCAL_PATH);
						 */

						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offloadPath);
						FileUtils.copyFileToDirectory(srcFile, destDir);

					
						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);

					} else if (Constants.AXINOM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

						/*
						 * FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.
						 * getTarFilePath(), Constants.OFFLOAD_LOCAL_PATH);
						 */

						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offloadPath);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						
						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);

					}else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

						/*
						 * FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.
						 * getTarFilePath(), Constants.OFFLOAD_LOCAL_PATH);
						 */

						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offloadPath);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						// here need to write old chunks db and paths also
						List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
								.removeChunkFileTransferDetails_manual(objLocFileTransferStatusDetailsEntity.getId());

						if (objListChunks != null && objListChunks.size() > 0) {

							for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

								FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

								fileTransferStatusDetailsRepository
										.deleteFileTransferDetails(objChunksDetailsEntity.getId());

							}
						}

						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						objLocFileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);

					}else if (Constants.ITU_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

						/*
						 * FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.
						 * getTarFilePath(), Constants.OFFLOAD_LOCAL_PATH);
						 */

						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offloadPath);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						// here need to write old chunks db and paths also
						List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
								.removeChunkFileTransferDetails_manual(objLocFileTransferStatusDetailsEntity.getId());

						if (objListChunks != null && objListChunks.size() > 0) {

							for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

								FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

								fileTransferStatusDetailsRepository
										.deleteFileTransferDetails(objChunksDetailsEntity.getId());

							}
						}

						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						objLocFileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);

					}

					finalTransferStatus = true;

				}
			}
		} catch (Exception e) {
			finalTransferStatus = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsOld_manual() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return finalTransferStatus;
	}

	/**
	 * This method will return Status Of File Transfer Details
	 * 
	 * @param objFileTransferStatusDetailsEntityList,
	 *            objLocFlightOpenCloseEntity
	 * @return boolean
	 */
	public boolean getStatusOfFileTransferDetailsNewFiles(
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
			FlightOpenCloseEntity objLocFlightOpenCloseEntity) {

		List<FileTransferStatusDetailsEntity> objList = null;
		boolean fileTransferStatus = false;
		boolean chunk_fileTransferStatus = false;
		boolean finalTransferStatus = false;

		logger.info("FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles()");

		try {
			String connCheckCount = env.getProperty("GROUND_SERVER_CONNECT_CHECK_COUNT");
			String connCheckSomeTime = env.getProperty("GROUND_SERVER_CONNECT_CHECK_SOMETIME");
			String groundServerIP = env.getProperty("GROUND_SERVER_IP");
			Integer groundServerPORT = Integer.valueOf(env.getProperty("GROUND_SERVER_PORT"));

			objList = objFileTransferStatusDetailsEntityList;

			// Checkin ground Server Ip All Ways
			boolean checking_Ground_IP = false;

			try {
				checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

			} catch (Exception e) {

				logger.error(
						"Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles()  in groundServer Port Checking: "
								+ ExceptionUtils.getFullStackTrace(e));
			}

			logger.debug("FileTransferStatusDetailsServiceimpl.getFileTransferDetails() checking_Ground_IP Status"
					+ checking_Ground_IP);

			if (!checking_Ground_IP) {

				if (StringUtils.isNotEmpty(connCheckCount)) {
					int conncount = Integer.parseInt(connCheckCount);

					for (int intial = 1; intial <= conncount; intial++) {
						try {
							checking_Ground_IP = commonUtil.isIPandportreachable(groundServerIP, groundServerPORT);

						} catch (Exception e) {

							logger.error(
									"Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles()  in groundServer Port Checking: "
											+ ExceptionUtils.getFullStackTrace(e));
						}
						if (!checking_Ground_IP) {
							logger.info(
									"FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Grond Server Conncection Checking Count "
											+ intial);
							continue;
						} else {
							break;
						}

					}
				}

				if (!checking_Ground_IP) {
					if (StringUtils.isNotEmpty(connCheckSomeTime)) {
						logger.info(
								"FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Grond Server Conncection Checking After This Many minutes "
										+ connCheckSomeTime);
						Thread.sleep(TimeUnit.MINUTES.toMillis(Integer.parseInt(connCheckCount)));
						flightTransferIfeToGround();
						// it wont go next implementaion
						objList = null;

					}
				}

			}

			if (checking_Ground_IP && objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */

					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					if (Constants.BITE_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
						fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
								objLocFileTransferStatusDetailsEntity);

						// checking connection repetedly based upon count
						if (!fileTransferStatus) {

							if (StringUtils.isNotEmpty(connCheckCount)) {
								int conncount = Integer.parseInt(connCheckCount);

								for (int intial = 1; intial <= conncount; intial++) {
									fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
											objLocFileTransferStatusDetailsEntity);
									if (!fileTransferStatus) {
										continue;
									} else {
										break;
									}

								}
							}

						}

						if (fileTransferStatus) {

							objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
							boolean updation_status = fileTransferStatusDetailsRepository
									.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
							if (updation_status) {

								String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
								String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
										.getTarFilePath()
										.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
												objLocFileTransferStatusDetailsEntity.getOriginalFilename());
								FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
								FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
							}

						}
					} else if (Constants.AXINOM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
						fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
								objLocFileTransferStatusDetailsEntity);

						// checking connection repetedly based upon count
						if (!fileTransferStatus) {

							if (StringUtils.isNotEmpty(connCheckCount)) {
								int conncount = Integer.parseInt(connCheckCount);

								for (int intial = 1; intial <= conncount; intial++) {
									fileTransferStatus = tarTransfer_to_ground_server_Bite_Axinom_Files(
											objLocFileTransferStatusDetailsEntity);
									if (!fileTransferStatus) {
										continue;
									} else {
										break;
									}

								}
							}

						}

						if (fileTransferStatus) {

							objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
							boolean updation_status = fileTransferStatusDetailsRepository
									.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
							if (updation_status) {

								/*
								 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
								 * biteFilePathDest.length())
								 */
								String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
								String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
										.getTarFilePath()
										.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
												objLocFileTransferStatusDetailsEntity.getOriginalFilename());
								FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
								FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
							}

						}
					} else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {

						boolean statusChunkTransfer = false;

						List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
								.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

						if (objListChunks != null && objListChunks.size() > 0) {

							for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

								chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
										objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
								// checking connection repetedly based upon count
								if (!chunk_fileTransferStatus) {

									if (StringUtils.isNotEmpty(connCheckCount)) {
										int conncount = Integer.parseInt(connCheckCount);

										for (int intial = 1; intial <= conncount; intial++) {
											chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
													objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
											if (!chunk_fileTransferStatus) {
												continue;
											} else {
												break;
											}

										}
									}

								}
								if (chunk_fileTransferStatus) {
									objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
									fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
								} else {
									statusChunkTransfer = true;
								}

							}

							if (!statusChunkTransfer) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								// chunk files delete
								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

								}

								// parent files delete
								String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
								String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
										.getTarFilePath()
										.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
												objLocFileTransferStatusDetailsEntity.getOriginalFilename());
								FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
								FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);

							}

						} else if (1 == objLocFileTransferStatusDetailsEntity.getChunkSumCount().intValue()) {
							fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
									objLocFileTransferStatusDetailsEntity);

							// checking connection repetedly based upon count
							if (!fileTransferStatus) {
								if (StringUtils.isNotEmpty(connCheckCount)) {
									int conncount = Integer.parseInt(connCheckCount);

									for (int intial = 1; intial <= conncount; intial++) {
										fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
												objLocFileTransferStatusDetailsEntity);
										if (!fileTransferStatus) {
											continue;
										} else {
											break;
										}

									}
								}

							}

							if (fileTransferStatus) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updation_status) {

									/*
									 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
									 * biteFilePathDest.length())
									 */
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
								}

							}

						}

					} else if (Constants.ITU_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())
							&& Constants.READY_TO_TRANSMIT
									.equalsIgnoreCase(objLocFlightOpenCloseEntity.getItuFileStatus())) {

						boolean statusChunkTransfer = false;

						List<ChunksDetailsEntity> objListChunks = fileTransferStatusDetailsRepository
								.getChunkFileTransferDetails(objLocFileTransferStatusDetailsEntity.getId());

						if (objListChunks != null && objListChunks.size() > 0) {

							for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

								chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
										objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
								// checking connection repetedly based upon count
								if (!chunk_fileTransferStatus) {

									if (StringUtils.isNotEmpty(connCheckCount)) {
										int conncount = Integer.parseInt(connCheckCount);

										for (int intial = 1; intial <= conncount; intial++) {
											chunk_fileTransferStatus = tarTransfer_to_ground_server_SystemItu_Log_Files(
													objChunksDetailsEntity, objLocFileTransferStatusDetailsEntity);
											if (!chunk_fileTransferStatus) {
												continue;
											} else {
												break;
											}

										}
									}

								}
								if (chunk_fileTransferStatus) {
									objChunksDetailsEntity.setChunkStatus(Constants.RECEIVED);
									fileTransferStatusDetailsRepository.saveChunkFileDetails(objChunksDetailsEntity);
								} else {
									statusChunkTransfer = true;
								}

							}

							if (!statusChunkTransfer) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								// chunk files delete
								for (ChunksDetailsEntity objChunksDetailsEntity : objListChunks) {

									FileUtil.deleteFileOrFolder(objChunksDetailsEntity.getChunkTarFilePath());

								}

								// parent files delete
								String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
								String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
										.getTarFilePath()
										.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
												objLocFileTransferStatusDetailsEntity.getOriginalFilename());
								FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
								FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);

							}

						} else if (1 == objLocFileTransferStatusDetailsEntity.getChunkSumCount().intValue()) {
							fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
									objLocFileTransferStatusDetailsEntity);

							// checking connection repetedly based upon count
							if (!fileTransferStatus) {
								if (StringUtils.isNotEmpty(connCheckCount)) {
									int conncount = Integer.parseInt(connCheckCount);

									for (int intial = 1; intial <= conncount; intial++) {
										fileTransferStatus = tarTransfer_to_ground_server_SystemItulogs_Files_WithoutChunks(
												objLocFileTransferStatusDetailsEntity);
										if (!fileTransferStatus) {
											continue;
										} else {
											break;
										}

									}
								}

							}

							if (fileTransferStatus) {

								objLocFileTransferStatusDetailsEntity.setStatus(Constants.RECEIVED);
								boolean updation_status = fileTransferStatusDetailsRepository
										.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
								if (updation_status) {

									/*
									 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
									 * biteFilePathDest.length())
									 */
									String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath();
									String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity
											.getTarFilePath()
											.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
													objLocFileTransferStatusDetailsEntity.getOriginalFilename());
									FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
									FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
								}

							}

						}

					}

				}

				finalTransferStatus = fileTransferStatusDetailsRepository
						.getFileTransferDetailsRestStatusRecived(objLocFlightOpenCloseEntity);
			}
		} catch (Exception e) {
			finalTransferStatus = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return finalTransferStatus;
	}

	
	/* This method is for manual offload transfer */

	/**
	 * This This method is for manual offload transfer
	 * 
	 * @param offload_path
	 * @return boolean
	 */
	@Override
	public boolean manualOffloadTransfer(String offload_path) {

		boolean status = false;
		logger.info("FileTransferStatusDetailsServiceimpl.manualOffloadTransfer()");

		try {

			
			List<FlightOpenCloseEntity> objItuFliOpenCleEtyDetLst = objFlightOpenCloseRepository
					.getFltDesClsRdyToItuTraStatusManual();

			if (objItuFliOpenCleEtyDetLst != null && !objItuFliOpenCleEtyDetLst.isEmpty()) {
				List<FileTransferStatusDetailsEntity> objOldItuList=fileTransferStatusDetailsRepository.getFileTransferDetails_ItuCheck();

				for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objItuFliOpenCleEtyDetLst) {
					long count=0;
					if(objOldItuList!=null && !objOldItuList.isEmpty()) {
						count=objOldItuList.parallelStream().filter(X->X.getOpenCloseDetilsId().intValue()==objLocFlightOpenCloseEntity.getId().intValue()).count();
					}
                       if(count==0) {
					FileTransferStatusDetailsEntity objItuEntity = itufileTraFmIFESertoLocPth(
							objLocFlightOpenCloseEntity);
                       }

								}

			}

			// get the details for ReadyToTransMit Deatils From FLIGHT_OPEN_CLOSE_DETAILS
			// Table

			
			List<FlightOpenCloseEntity> objfileMainDetailsList = objFlightOpenCloseRepository
					.getFlightDetailsCloseAndReadyToTransper_manual();
			if (objfileMainDetailsList != null && objfileMainDetailsList.size() > 0) {
				// get the details for ReadyToTransMit Deatils From FILE_TRANSFER_STATUS_DETAILS
				// Table
				
				
				
				List<FileTransferStatusDetailsEntity> objOldList = fileTransferStatusDetailsRepository
						.getFileTransferDetails_manual();

				for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objfileMainDetailsList) {
					
				/*	objOldList = fileTransferStatusDetailsRepository
							.getFileTransferDetails_manual();*/

					if (objOldList != null && objOldList.size() > 0) {

						List<FileTransferStatusDetailsEntity> objTransperDetailsOldList = objOldList.stream()
								.filter(x -> x.getOpenCloseDetilsId().intValue() == objLocFlightOpenCloseEntity.getId()
										.intValue())
								.collect(Collectors.toList());

						if (objTransperDetailsOldList != null && objTransperDetailsOldList.size() > 0) {
							boolean oldFileTransPerDetailsStatus = getStatusOfFileTransferDetailsOld_manual(
									objTransperDetailsOldList, objLocFlightOpenCloseEntity, offload_path);

							if (oldFileTransPerDetailsStatus) {

								// FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet =
								// objLocFlightOpenCloseEntity;
								objLocFlightOpenCloseEntity.setWayOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
								objFlightOpenCloseRepository.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntity);
							}
						}else {

							List<FileTransferStatusDetailsEntity> objListTransferFiles = fileTransferFromIFEServertoLocalPathManval(
									objLocFlightOpenCloseEntity);

							if (objListTransferFiles != null && objListTransferFiles.size() > 0) {
								boolean newFileTransPerDetailsStatusOffload = getStatusOfFileTransferDetailsNewFiles_manual(
										objListTransferFiles, objLocFlightOpenCloseEntity, offload_path);

								if (newFileTransPerDetailsStatusOffload) {

									FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = objLocFlightOpenCloseEntity;
									objLocFlightOpenCloseEntityStatusSet
											.setWayOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
									objFlightOpenCloseRepository
											.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);
								}

							}

						}

					} else {

						List<FileTransferStatusDetailsEntity> objListTransferFiles = fileTransferFromIFEServertoLocalPathManval(
								objLocFlightOpenCloseEntity);

						if (objListTransferFiles != null && objListTransferFiles.size() > 0) {
							boolean newFileTransPerDetailsStatusOffload = getStatusOfFileTransferDetailsNewFiles_manual(
									objListTransferFiles, objLocFlightOpenCloseEntity, offload_path);

							if (newFileTransPerDetailsStatusOffload) {

								FlightOpenCloseEntity objLocFlightOpenCloseEntityStatusSet = objLocFlightOpenCloseEntity;
								objLocFlightOpenCloseEntityStatusSet
										.setWayOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
								objFlightOpenCloseRepository
										.saveFlightOpenCloseDetails(objLocFlightOpenCloseEntityStatusSet);
							}

						}

					}

				}

			}

			status = true;
		} catch (Exception e) {
			status = true;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.manualOffloadTransfer() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;

	}

	/**
	 * This This method will Transfer Files from IFEServer to LocalPathManval
	 * 
	 * @param objStausFlightOpenCloseEntity
	 * @return FileTransferStatusDetailsEntity
	 */
	public List<FileTransferStatusDetailsEntity> fileTransferFromIFEServertoLocalPathManval(
			FlightOpenCloseEntity objStausFlightOpenCloseEntity) {

		logger.info("FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathManval()");
		List<FileTransferStatusDetailsEntity> objListDetils = new ArrayList<>();

		try {
			String biteFilePathSource = env.getProperty("BITE_FILE_PATH_IFE");
			String axinomFilePathSource = env.getProperty("AXINOM_FILE_PATH_IFE");
			String systemLogFilePathSource = env.getProperty("SYSTEM_LOG_FILE_PATH_IFE");
			String ituLogFilePathSource = env.getProperty("ITU_LOG_FILE_PATH_IFE");
			
			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			StringBuilder bitedestpaths = new StringBuilder();
			StringBuilder axinomdestpaths = new StringBuilder();
			StringBuilder syslogdestpaths = new StringBuilder();
			StringBuilder itulogdestpaths = new StringBuilder();

			String biteFilePathDest = bitedestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("BITE_FILE_PATH_DESTINATION")).toString();
			String axinomFilePathDest = axinomdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).toString();
			String systemLogFilePathDest = syslogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			String ituLogFilePathDest = itulogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			/*String biteFilePathDest = env.getProperty("BITE_FILE_PATH_DESTINATION");
			String axinomFilePathDest = env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION");
			String systemLogFilePathDest = env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION");
			String ituLogFilePathDest = env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION");*/
		
			/* creating the Bite tar File */
			if (StringUtils.isNotEmpty(biteFilePathSource)) {

				File biteFileSource = new File(biteFilePathSource);
				if (biteFileSource.exists()) {

					StringBuilder folderDate = new StringBuilder();
					biteFilePathDest = folderDate.append(biteFilePathDest).append(Constants.BITE_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory"
									+ biteFilePathDest);
					File biteFileDestination = new File(biteFilePathDest);

					if (!biteFileDestination.exists()) {
						biteFileDestination.mkdirs();
					}

					/* To create a tar file with the file type and date */
					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(biteFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
									+ destFolder);

					FileUtil.copyFastFileToLocationbetweenDatess(biteFilePathSource, biteFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);

					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							biteFilePathDest);
					boolean statusOfTarFile = CommonUtil.createTarFile(biteFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.debug(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bitelog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathDest
								.substring(biteFilePathDest.lastIndexOf('/') + 1, biteFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}
				}

			}
			/* creating the Axinom tar File */

			if (StringUtils.isNotEmpty(axinomFilePathSource)) {

				File axinomFileSource = new File(axinomFilePathSource);

				if (axinomFileSource.exists()) {

					StringBuilder folderDate = new StringBuilder();
					axinomFilePathDest = folderDate.append(axinomFilePathDest).append(Constants.AXINOM_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_Folder_in_local_directory"
									+ axinomFilePathDest);

					File axinomFileDestination = new File(axinomFilePathDest);

					if (!axinomFileDestination.exists()) {
						axinomFileDestination.mkdirs();
					}

					/* To create a tar file with the file type and date */
					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(axinomFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
									+ destFolder);
					// need to change

					FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, axinomFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);

					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							axinomFilePathDest);
					boolean statusOfTarFile = CommonUtil.createTarFile(axinomFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.debug(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(axinomFilePathDest
								.substring(axinomFilePathDest.lastIndexOf('/') + 1, axinomFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}

				}

			}

			if (StringUtils.isNotEmpty(systemLogFilePathSource)) {

				File systemLogFileSource = new File(systemLogFilePathSource);

				if (systemLogFileSource.exists()) {
					StringBuilder folderDate = new StringBuilder();
					systemLogFilePathDest = folderDate.append(systemLogFilePathDest).append(Constants.SYSTEM_LOG)
							.append("_").append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ systemLogFilePathDest);

					File systemLogFileDestination = new File(systemLogFilePathDest);

					if (!systemLogFileDestination.exists()) {
						systemLogFileDestination.mkdirs();
					}

					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(systemLogFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() SystemLog_Folder_in_local_directory"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatess(systemLogFilePathSource, systemLogFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							systemLogFilePathDest);
					boolean statusOfTarFile = CommonUtil.createTarFile(systemLogFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.debug(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Systemlog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(systemLogFilePathDest
								.substring(systemLogFilePathDest.lastIndexOf('/') + 1, systemLogFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}

				}

			}
			
			if (StringUtils.isNotEmpty(ituLogFilePathSource)&& Constants.READY_TO_TRANSMIT.equalsIgnoreCase(objStausFlightOpenCloseEntity.getItuFileStatus())) {
				File ituLogFileSource = new File(ituLogFilePathSource);

				if (ituLogFileSource.exists()) {
					StringBuilder folderDate = new StringBuilder();
					ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG)
							.append("_").append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime())).toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() ItuLog_Folder_in_local_directory"
									+ ituLogFilePathDest);

					File ituLogFileDestination = new File(ituLogFilePathDest);

					if (!ituLogFileDestination.exists()) {
						ituLogFileDestination.mkdirs();
					}

					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(ituLogFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() ItuLog_Folder_in_local_directory"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatessItu(ituLogFilePathSource, ituLogFilePathDest,
							objStausFlightOpenCloseEntity.getFlightCloseTime(),
							objStausFlightOpenCloseEntity.getNextFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							ituLogFilePathDest);
					boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.debug(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Itulog_checkSum"
										+ checkSum);
						FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
								.substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
						objListDetils.add(objFileTransferStatusDetailsEntity);

					}

				}

			}

			/*
			 * status=fileTransferStatusDetailsRepository.deleteFileTransferDetails(reqID);
			 */
		} catch (Exception e) {
			objListDetils = null;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		finally {
			// getFileTransferDetails();
		}

		return objListDetils;
	}

	/**
	 * This This method will returns File Transfer Details NewFiles_manual
	 * 
	 * @param objFileTransferStatusDetailsEntityList,
	 *            objLocFlightOpenCloseEntity, offload_path
	 * @return boolean
	 */
	public boolean getStatusOfFileTransferDetailsNewFiles_manual(
			List<FileTransferStatusDetailsEntity> objFileTransferStatusDetailsEntityList,
			FlightOpenCloseEntity objLocFlightOpenCloseEntity, String offload_path) {

		List<FileTransferStatusDetailsEntity> objList = null;

		boolean finalTransferStatus = false;

		logger.info("FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles_manual() start..");

		try {

			objList = objFileTransferStatusDetailsEntityList;

			if (objList != null && objList.size() > 0) {

				for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objList) {

					/* Checking the Ground server IP and Port */
					logger.info("FileTransferStatusDetailsServiceimpl.getFileTransferDetails()  Date"
							+ objLocFileTransferStatusDetailsEntity.getTarFileDate());

					// need to change

					if (Constants.BITE_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
						/*FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.getTarFilePath(),
								offload_path);*/
						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offload_path);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
					/*	if (updation_status) {

							
							 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
							 * biteFilePathDest.length())
							 
							String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
							String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath()
									.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
											objLocFileTransferStatusDetailsEntity.getOriginalFilename());
							FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
							FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
						}*/

					} else if (Constants.AXINOM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
						/*FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.getTarFilePath(),
								offload_path);*/
						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offload_path);
						FileUtils.copyFileToDirectory(srcFile, destDir);


						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
					/*	if (updation_status) {

							
							 * biteFilePathDest.substring(biteFilePathDest.lastIndexOf('/') + 1,
							 * biteFilePathDest.length())
							 
							String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
							String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath()
									.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
											objLocFileTransferStatusDetailsEntity.getOriginalFilename());
							FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
							FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
						}*/

					} else if (Constants.SYSTEM_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
					/*	FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.getTarFilePath(),
								offload_path);*/

						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offload_path);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
					/*	if (updation_status) {

							String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
							String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath()
									.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
											objLocFileTransferStatusDetailsEntity.getOriginalFilename());
							FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
							FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
						}*/
					}else if (Constants.ITU_LOG.equals(objLocFileTransferStatusDetailsEntity.getFileType())) {
						/*FileUtil.copyFastFileToLocation(objLocFileTransferStatusDetailsEntity.getTarFilePath(),
								offload_path);*/

						
						File srcFile = new File(objLocFileTransferStatusDetailsEntity.getTarFilePath());
						File destDir = new File(offload_path);
						FileUtils.copyFileToDirectory(srcFile, destDir);

						objLocFileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_OFFLOAD);

						boolean updation_status = fileTransferStatusDetailsRepository
								.saveFileTransferDetails(objLocFileTransferStatusDetailsEntity);
					/*	if (updation_status) {

							String getTarFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath();
							String sourceFolderFilenameToDelete = objLocFileTransferStatusDetailsEntity.getTarFilePath()
									.replaceAll(objLocFileTransferStatusDetailsEntity.getTarFilename(),
											objLocFileTransferStatusDetailsEntity.getOriginalFilename());
							FileUtil.deleteFileOrFolder(getTarFilenameToDelete);
							FileUtil.deleteFileOrFolder(sourceFolderFilenameToDelete);
						}*/
					}

					finalTransferStatus = true;

				}
			}
		} catch (Exception e) {
			finalTransferStatus = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.getStatusOfFileTransferDetailsNewFiles() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return finalTransferStatus;
	}

	/**
	 * This This method will Transfer metadataJson File
	 * 
	 * @param objStausFlightOpenCloseEntity,
	 *            destinationPathFolder
	 * @return boolean
	 */
	public boolean metadataJsonFileCreationAndTransfer(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
			String destinationPathFolder) {

		ObjectMapper Obj = new ObjectMapper();
		StringBuilder objStringBuilder = new StringBuilder();
		logger.info("FileTransferStatusDetailsServiceimpl.metadataJsonFileCreationAndTransfer()");

		boolean status = false;
		try {
			FlightMetaDataModel objFlightMetaDataModel = new FlightMetaDataModel();
			objFlightMetaDataModel.setAircraftsubtype("IFDT");
			objFlightMetaDataModel.setAircrafttype(objStausFlightOpenCloseEntity.getAircraftType());
			objFlightMetaDataModel.setAirline(objStausFlightOpenCloseEntity.getAirLineName());
			objFlightMetaDataModel.setArrivalairport(objStausFlightOpenCloseEntity.getArrivalAirport());
			objFlightMetaDataModel.setDepartureairport(objStausFlightOpenCloseEntity.getDepartureAirport());
			//objFlightMetaDataModel.setDeparturetime(objStausFlightOpenCloseEntity.getDepartureTime().toString());
			objFlightMetaDataModel.setFlightnumber(objStausFlightOpenCloseEntity.getFlightNumber());
			objFlightMetaDataModel.setTailnumber(objStausFlightOpenCloseEntity.getTailNumber());
			objFlightMetaDataModel.setArrivaltime(objStausFlightOpenCloseEntity.getArrivalTime().toString());

			String jsonStr = Obj.writeValueAsString(objFlightMetaDataModel);
			objStringBuilder.append(destinationPathFolder);
			objStringBuilder.append(File.separator);
			objStringBuilder.append(Constants.METADATA_FILE);
			CommonUtil.writeDataToFile(jsonStr, objStringBuilder.toString());
			status = true;

		} catch (Exception e) {
			logger.error("Exception FileTransferStatusDetailsServiceimpl.metadataJsonFileCreationAndTransfer() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;

	}

	/**
	 * This This method is used for groundServerQuery
	 * 
	 * @param objOffLoadList,
	 *            objfileMainDetailsList
	 * @return boolean
	 */
	public boolean groundServerQuery(List<FileTransferStatusDetailsEntity> objOffLoadList,
			List<FlightOpenCloseEntity> objfileMainDetailsList) {
		boolean status = false;
		StringBuilder sboffloadedURL = new StringBuilder();
		logger.info("FileTransferStatusDetailsServiceimpl.groundServerQuery()");
		List<String> objListOffLoadFileNames = new ArrayList<>();
		try {

			Set<Integer> offLoadDetailsIds = objOffLoadList.stream().map(X -> X.getOpenCloseDetilsId())
					.collect(Collectors.toSet());

			for (FileTransferStatusDetailsEntity objLocFileTransferStatusDetailsEntity : objOffLoadList) {

				objListOffLoadFileNames.add(objLocFileTransferStatusDetailsEntity.getTarFilename());

			}

			String offloadedServerIP = env.getProperty("OFFLOADED_SERVER_IP");
			Integer offloadedServerURL = Integer.valueOf(env.getProperty("OFFLOADED_SERVER_PORT"));
			boolean checkingOffloadedIP = commonUtil.isIPandportreachable(offloadedServerIP, offloadedServerURL);
			if (checkingOffloadedIP) {

				String listOfFilenames = restQueryForOffload(objListOffLoadFileNames);

				if (StringUtils.isNotEmpty(listOfFilenames)) {

					HashMap<String, StatusQueryModel> mapStatusQueryModel = getStatusQueryModel(listOfFilenames);

					if (mapStatusQueryModel != null && mapStatusQueryModel.size() > 0) {
						for (Map.Entry<String, StatusQueryModel> objLocalMap : mapStatusQueryModel.entrySet()) {
							String key = objLocalMap.getKey();
							StatusQueryModel objLocStatusQueryModel = objLocalMap.getValue();

							if (objLocStatusQueryModel != null) {

								if (StringUtils.isNotEmpty(objLocStatusQueryModel.getChecksumStatus())
										&& (StringUtils.isNotEmpty(objLocStatusQueryModel.getExtractionStatus()))) {

									if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())
											&& "true".equalsIgnoreCase(objLocStatusQueryModel.getExtractionStatus())) {

										FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
												.stream().filter(X -> key.equals(X.getTarFilename())).findFirst()
												.orElse(null);
										if (objFileTransferStatusDetailsEntity != null) {
											if(1<objFileTransferStatusDetailsEntity.getChunkSumCount() && (Constants.SYSTEM_LOG.equalsIgnoreCase(
													objFileTransferStatusDetailsEntity.getFileType()) || Constants.ITU_LOG.equalsIgnoreCase(
													objFileTransferStatusDetailsEntity.getFileType()))) {
											fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(objFileTransferStatusDetailsEntity.getId());
										}

											objFileTransferStatusDetailsEntity.setStatus(Constants.ACKNOWLEDGED);
											fileTransferStatusDetailsRepository
													.saveFileTransferDetails(objFileTransferStatusDetailsEntity);

										}

									} else if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())
											&& "false".equalsIgnoreCase(objLocStatusQueryModel.getExtractionStatus())) {

										FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
												.stream().filter(X -> key.equals(X.getTarFilename())).findFirst()
												.orElse(null);
										if (objFileTransferStatusDetailsEntity != null) {

											objFileTransferStatusDetailsEntity
													.setFailureReasonForOffLoad(Constants.EXTRACTION_FAILURE);
											fileTransferStatusDetailsRepository
													.saveFileTransferDetails(objFileTransferStatusDetailsEntity);

										}

									} else {
										FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
												.stream().filter(x -> key.equals(x.getTarFilename())).findFirst()
												.orElse(null);
										if (objFileTransferStatusDetailsEntity != null) {

											objFileTransferStatusDetailsEntity
													.setFailureReasonForOffLoad(Constants.CHECKSUM_FAILURE);
											fileTransferStatusDetailsRepository
													.saveFileTransferDetails(objFileTransferStatusDetailsEntity);

										}

									}

								} else if (StringUtils.isNotEmpty(objLocStatusQueryModel.getChecksumStatus())
										&& StringUtils.isEmpty(objLocStatusQueryModel.getExtractionStatus())) {
									if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())) {
										FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
												.stream().filter(X -> key.equals(X.getTarFilename())).findFirst()
												.orElse(null);
										if (objFileTransferStatusDetailsEntity != null) {

											objFileTransferStatusDetailsEntity
													.setFailureReasonForOffLoad(Constants.EXTRACTION_FAILURE);
											fileTransferStatusDetailsRepository
													.saveFileTransferDetails(objFileTransferStatusDetailsEntity);

										}

									}

								}

							} else {
								FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
										.stream().filter(X -> key.equals(X.getTarFilename())).findFirst().orElse(null);
								if (objFileTransferStatusDetailsEntity != null) {
									objFileTransferStatusDetailsEntity
											.setFailureReasonForOffLoad(Constants.FILE_NOT_RECEIVED);
									fileTransferStatusDetailsRepository
											.saveFileTransferDetails(objFileTransferStatusDetailsEntity);
								}
							}

							// status change in first table

							if (offLoadDetailsIds != null && offLoadDetailsIds.size() > 0)

								for (Integer offLoadId : offLoadDetailsIds) {
									FlightOpenCloseEntity objFlightOpenCloseEntityStatus = objfileMainDetailsList
											.stream().filter(X -> offLoadId.intValue() == X.getId().intValue())
											.findFirst().orElse(null);

									boolean statusOFFileTransfered = fileTransferStatusDetailsRepository
											.getFileTransferDetailsRestStatus(objFlightOpenCloseEntityStatus);

									if (statusOFFileTransfered) {
										objFlightOpenCloseEntityStatus.setItuFileStatus(Constants.ACKNOWLEDGED);
										objFlightOpenCloseEntityStatus.setTransferStatus(Constants.ACKNOWLEDGED);
										objFlightOpenCloseRepository
												.saveFlightOpenCloseDetails(objFlightOpenCloseEntityStatus);
									}

								}

						}
					}
				}

			}
			status = true;

		} catch (Exception e) {
			logger.error("Exception FileTransferStatusDetailsServiceimpl.groundServerQuery() : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	private HashMap<String, StatusQueryModel> getStatusQueryModel(String listOfFilenames)
			throws IOException, JsonParseException, JsonMappingException {

		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		HashMap<String, StatusQueryModel> mapStatusQueryModel = mapper.readValue(listOfFilenames,
				new com.fasterxml.jackson.core.type.TypeReference<HashMap<String, StatusQueryModel>>() {
				});
		return mapStatusQueryModel;
	}

	public boolean receivedStatusServerQuerying(
			List<FileTransferStatusDetailsEntity> objOffLoadList, List<FlightOpenCloseEntity> objfileMainDetailsList) {
		
		boolean status=false;

		try {

			Set<Integer> offLoadDetailsIds = objOffLoadList.stream().map(X -> X.getOpenCloseDetilsId())
					.collect(Collectors.toSet());
			for (FlightOpenCloseEntity objLocFlightOpenCloseEntity : objfileMainDetailsList) {
				if (objOffLoadList != null && objOffLoadList.size() > 0) {

					List<FileTransferStatusDetailsEntity> objTransperDetailsOldList = objOffLoadList.stream().filter(
							x -> x.getOpenCloseDetilsId().intValue() == objLocFlightOpenCloseEntity.getId().intValue())
							.collect(Collectors.toList());
					if (objTransperDetailsOldList != null && !objTransperDetailsOldList.isEmpty()) {

						List<String> listOffilenames = objTransperDetailsOldList.stream().map(Y -> Y.getTarFilename())
								.collect(Collectors.toList());

						String listOfFilenames = restQueryForOffload(listOffilenames);

						if (StringUtils.isNotEmpty(listOfFilenames)) {

							HashMap<String, StatusQueryModel> mapStatusQueryModel = getStatusQueryModel(
									listOfFilenames);

							if (mapStatusQueryModel != null && mapStatusQueryModel.size() > 0) {
								for (Map.Entry<String, StatusQueryModel> objLocalMap : mapStatusQueryModel.entrySet()) {
									String key = objLocalMap.getKey();
									StatusQueryModel objLocStatusQueryModel = objLocalMap.getValue();

									if (objLocStatusQueryModel != null) {

										if (StringUtils.isNotEmpty(objLocStatusQueryModel.getChecksumStatus())
												&& (StringUtils
														.isNotEmpty(objLocStatusQueryModel.getExtractionStatus()))) {

											if ("true".equalsIgnoreCase(objLocStatusQueryModel.getChecksumStatus())
													&& "true".equalsIgnoreCase(
															objLocStatusQueryModel.getExtractionStatus())) {

												FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
														.stream().filter(X -> key.equals(X.getTarFilename()))
														.findFirst().orElse(null);
												if (objFileTransferStatusDetailsEntity != null) {

													if(1<objFileTransferStatusDetailsEntity.getChunkSumCount() && (Constants.SYSTEM_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType()) || Constants.ITU_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType()))) {
														fileTransferStatusDetailsRepository.updateFileTransferDetailsChunks(objFileTransferStatusDetailsEntity.getId());
													}
													objFileTransferStatusDetailsEntity
													.setStatus(Constants.ACKNOWLEDGED);
											fileTransferStatusDetailsRepository.saveFileTransferDetails(
													objFileTransferStatusDetailsEntity);

												}

											} else {

												// tar file creation

												FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
														.stream().filter(X -> key.equals(X.getTarFilename()))
														.findFirst().orElse(null);

												if (objFileTransferStatusDetailsEntity != null
														&& Constants.BITE_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType())) {
													fileTransferFromIFEServertoLocalPathBITE(
															objLocFlightOpenCloseEntity,
															objFileTransferStatusDetailsEntity);

												} else if (objFileTransferStatusDetailsEntity != null
														&& Constants.AXINOM_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType())) {

													fileTransferFromIFEServertoLocalPathAXINOM(
															objLocFlightOpenCloseEntity,
															objFileTransferStatusDetailsEntity);

												} else if (objFileTransferStatusDetailsEntity != null
														&& Constants.SYSTEM_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType())) {
													fileTransferFromIFEServertoLocalPathSYSTEM(
															objLocFlightOpenCloseEntity,
															objFileTransferStatusDetailsEntity);

												} else if (objFileTransferStatusDetailsEntity != null
														&& Constants.ITU_LOG.equalsIgnoreCase(
																objFileTransferStatusDetailsEntity.getFileType())) {
													fileTransferFromIFEServertoLocalPathITU(objLocFlightOpenCloseEntity,
															objFileTransferStatusDetailsEntity);

												}

											}

										}

									} else {
										FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = objOffLoadList
												.stream().filter(X -> key.equals(X.getTarFilename())).findFirst()
												.orElse(null);
										if (objFileTransferStatusDetailsEntity != null) {

											if (objFileTransferStatusDetailsEntity != null
													&& Constants.BITE_LOG.equalsIgnoreCase(
															objFileTransferStatusDetailsEntity.getFileType())) {
												fileTransferFromIFEServertoLocalPathBITE(objLocFlightOpenCloseEntity,
														objFileTransferStatusDetailsEntity);

											} else if (objFileTransferStatusDetailsEntity != null
													&& Constants.AXINOM_LOG.equalsIgnoreCase(
															objFileTransferStatusDetailsEntity.getFileType())) {

												fileTransferFromIFEServertoLocalPathAXINOM(objLocFlightOpenCloseEntity,
														objFileTransferStatusDetailsEntity);

											} else if (objFileTransferStatusDetailsEntity != null
													&& Constants.SYSTEM_LOG.equalsIgnoreCase(
															objFileTransferStatusDetailsEntity.getFileType())) {
												fileTransferFromIFEServertoLocalPathSYSTEM(objLocFlightOpenCloseEntity,
														objFileTransferStatusDetailsEntity);

											} else if (objFileTransferStatusDetailsEntity != null
													&& Constants.ITU_LOG.equalsIgnoreCase(
															objFileTransferStatusDetailsEntity.getFileType())) {
												fileTransferFromIFEServertoLocalPathITU(objLocFlightOpenCloseEntity,
														objFileTransferStatusDetailsEntity);

											}

										}
									}

									// status change in first table

									if (offLoadDetailsIds != null && offLoadDetailsIds.size() > 0)

										for (Integer offLoadId : offLoadDetailsIds) {
											FlightOpenCloseEntity objFlightOpenCloseEntityStatus = objfileMainDetailsList
													.stream().filter(X -> offLoadId.intValue() == X.getId().intValue())
													.findFirst().orElse(null);

											boolean statusOFFileTransfered = fileTransferStatusDetailsRepository
													.getFleTraferDetRestStatusCheck(objFlightOpenCloseEntityStatus);

											if (statusOFFileTransfered) {
												objFlightOpenCloseEntityStatus.setItuFileStatus(Constants.ACKNOWLEDGED);
												objFlightOpenCloseEntityStatus.setTransferStatus(Constants.ACKNOWLEDGED);
												objFlightOpenCloseRepository
														.saveFlightOpenCloseDetails(objFlightOpenCloseEntityStatus);
											}

										}

								}
							}
						}
					}

				}
			}
			 status=true;
		} catch (Exception e) {
			 status=false;
			 logger.error("Exception FileTransferStatusDetailsServiceimpl.receivedStatusServerQuerying() : "
						+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	public void receivedStatusOffloadQuerying(List<String> receivedFilenamesOfOneEntity) {
		logger.info(
				"restQueryForOffload.flightOpenCloseDetailsSave()  finalOffloadedQuery" + receivedFilenamesOfOneEntity);
		String offloadedResponse = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);

			ManualOffload manualOffload = new ManualOffload();
			manualOffload.setFiles(receivedFilenamesOfOneEntity.toArray(new String[0]));

			HttpEntity<ManualOffload> requestEntity = new HttpEntity<>(manualOffload, headers);

			String offloadedURL = env.getProperty("OFFLOADED_SERVER_URL");

			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setConnectionManager(connectionManager).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			requestFactory.setHttpClient(httpClient);

			RestTemplate restTemplate =fileTransferStatusDetailsRepository.createRestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.postForEntity(offloadedURL, requestEntity, String.class);
			int statusCode = response.getStatusCodeValue();

			logger.debug("FileTransferStatusDetailsServiceimpl.restQueryForOffload()  statusCode" + statusCode);

			if (200 == statusCode) {
				offloadedResponse = response.getBody();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean fileTransferFromIFEServertoLocalPathBITE(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
			FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()  objLocFileTransferStatusDetailsEntity"
						+ objStausFlightOpenCloseEntity);
		boolean status = false;
		try {

			String biteFilePathSource = env.getProperty("BITE_FILE_PATH_IFE");

			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			StringBuilder bitedestpaths = new StringBuilder();

			String biteFilePathDest = bitedestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("BITE_FILE_PATH_DESTINATION")).toString();

			Date date = new Date();

			/* creating the Bite tar File */
			if (StringUtils.isNotEmpty(biteFilePathSource)) {

				File biteFileSource = new File(biteFilePathSource);
				if (biteFileSource.exists()) {

					String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
					StringBuilder folderDate = new StringBuilder();
					biteFilePathDest = folderDate.append(biteFilePathDest).append(Constants.BITE_LOG).append("_")
							.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
							.append(String.valueOf(
									DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime()))
							.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory"
									+ biteFilePathDest);
					File biteFileDestination = new File(biteFilePathDest);

					if (!biteFileDestination.exists()) {
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory ");
						biteFileDestination.mkdirs();
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bite_Folder_in_local_directory "
										+ biteFileDestination + " Creted Successfuly");
					}

					/* To create a tar file with the file type and date */
					StringBuilder tarOutputBuilder = new StringBuilder();
					tarOutputBuilder.append(biteFileDestination).append(".tar.gz");
					String destFolder = tarOutputBuilder.toString();

					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
									+ destFolder);
					FileUtil.copyFastFileToLocationbetweenDatess(biteFilePathSource, biteFilePathDest,
							objStausFlightOpenCloseEntity.getFlightOpenTime(),
							objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
					boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
							biteFilePathDest);

					boolean statusOfTarFile = CommonUtil.createTarFile(biteFilePathDest, destFolder);
					if (statusOfTarFile) {
						MessageDigest md = MessageDigest.getInstance("MD5");
						String checkSum = commonUtil.computeFileChecksum(md, destFolder);
						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Bitelog_checkSum"
										+ checkSum);
						// FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity = new
						// FileTransferStatusDetailsEntity();
						// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
						// biteFilePathSource.length()-2))+1);
						fileTransferStatusDetailsEntity.setChecksum(checkSum);
						fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
						fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathDest
								.substring(biteFilePathDest.lastIndexOf('/') + 1, biteFilePathDest.length()));
						fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
						fileTransferStatusDetailsEntity.setTarFileDate(new Date());
						fileTransferStatusDetailsEntity.setTarFilename(
								destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
						fileTransferStatusDetailsEntity.setFileType(Constants.BITE_LOG);
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
						fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

						if (objFileTransferStatusDetailsEntity != null) {
							status = true;
						}

					}
				}

			}

		} catch (Exception e) {
			status = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathBITE()  : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	public boolean fileTransferFromIFEServertoLocalPathAXINOM(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
			FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath()  objLocFileTransferStatusDetailsEntity"
						+ objStausFlightOpenCloseEntity);
		boolean status = false;

		try {
			String axinomFilePathSource = env.getProperty("AXINOM_FILE_PATH_IFE");
			StringBuilder axinomdestpaths = new StringBuilder();
			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			String axinomFilePathDest = axinomdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("AXINOM_FILE_PATH_IFE_DESTINATION")).toString();

			File axinomFileSource = new File(axinomFilePathSource);
			Date date = new Date();

			if (axinomFileSource.exists()) {

				String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
				StringBuilder folderDate = new StringBuilder();
				axinomFilePathDest = folderDate.append(axinomFilePathDest).append(Constants.AXINOM_LOG).append("_")
						.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
						.append(String
								.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime()))
						.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_Folder_in_local_directory"
								+ axinomFilePathDest);

				File axinomFileDestination = new File(axinomFilePathDest);

				if (!axinomFileDestination.exists()) {
					axinomFileDestination.mkdirs();
				}

				/* To create a tar file with the file type and date */
				StringBuilder tarOutputBuilder = new StringBuilder();
				tarOutputBuilder.append(axinomFileDestination).append(".tar.gz");
				String destFolder = tarOutputBuilder.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() destination_tar"
								+ destFolder);
				FileUtil.copyFastFileToLocationbetweenDatess(axinomFilePathSource, axinomFilePathDest,
						objStausFlightOpenCloseEntity.getFlightOpenTime(),
						objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
				boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
						axinomFilePathDest);
				boolean statusOfTarFile = CommonUtil.createTarFile(axinomFilePathDest, destFolder);
				if (statusOfTarFile) {
					MessageDigest md = MessageDigest.getInstance("MD5");
					String checkSum = commonUtil.computeFileChecksum(md, destFolder);
					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPath() Axinom_checkSum"
									+ checkSum);

					// fileTransferStatusDetailsEntity.setOriginalFilename(biteFilePathSource.substring(0,biteFilePathSource.lastIndexOf("/",
					// biteFilePathSource.length()-2))+1);
					fileTransferStatusDetailsEntity.setChecksum(checkSum);
					fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
					fileTransferStatusDetailsEntity.setOriginalFilename(axinomFilePathDest
							.substring(axinomFilePathDest.lastIndexOf('/') + 1, axinomFilePathDest.length()));
					fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
					fileTransferStatusDetailsEntity.setTarFileDate(new Date());
					fileTransferStatusDetailsEntity
							.setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
					fileTransferStatusDetailsEntity.setFileType(Constants.AXINOM_LOG);
					fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
					fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
					fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

					FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
							.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

				}

			}

		} catch (Exception e) {
			status = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathBITE()  : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	public boolean fileTransferFromIFEServertoLocalPathSYSTEM(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
			FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM()  objLocFileTransferStatusDetailsEntity"
						+ objStausFlightOpenCloseEntity);
		boolean status = false;

		try {

			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			String systemLogFilePathSource = env.getProperty("SYSTEM_LOG_FILE_PATH_IFE");
			StringBuilder syslogdestpaths = new StringBuilder();
			String systemLogFilePathDest = syslogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("SYSTEM_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			File systemLogFileSource = new File(systemLogFilePathSource);
			Date date = new Date();
			if (systemLogFileSource.exists()) {
				String makeAFolderWithDate = CommonUtil.dateToString(date, Constants.YYYY_MM_DD_HH_MM);
				StringBuilder folderDate = new StringBuilder();
				systemLogFilePathDest = folderDate.append(systemLogFilePathDest).append(Constants.SYSTEM_LOG)
						.append("_").append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
						.append(String
								.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime()))
						.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM() SystemLog_Folder_in_local_directory"
								+ systemLogFilePathDest);

				File systemLogFileDestination = new File(systemLogFilePathDest);

				if (!systemLogFileDestination.exists()) {
					systemLogFileDestination.mkdirs();
				}

				StringBuilder tarOutputBuilder = new StringBuilder();
				tarOutputBuilder.append(systemLogFileDestination).append(".tar.gz");
				String destFolder = tarOutputBuilder.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM() SystemLog_Folder_in_local_directory"
								+ destFolder);
				FileUtil.copyFastFileToLocationbetweenDatess(systemLogFilePathSource, systemLogFilePathDest,
						objStausFlightOpenCloseEntity.getFlightOpenTime(),
						objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
				boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
						systemLogFilePathDest);

				boolean statusOfTarFile = CommonUtil.createTarFile(systemLogFilePathDest, destFolder);
				if (statusOfTarFile) {
					MessageDigest md = MessageDigest.getInstance("MD5");
					String checkSum = commonUtil.computeFileChecksum(md, destFolder);
					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM() Systemlog_checkSum"
									+ checkSum);

					fileTransferStatusDetailsEntity.setChecksum(checkSum);
					fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
					fileTransferStatusDetailsEntity.setOriginalFilename(systemLogFilePathDest
							.substring(systemLogFilePathDest.lastIndexOf('/') + 1, systemLogFilePathDest.length()));
					fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
					fileTransferStatusDetailsEntity.setTarFileDate(new Date());
					fileTransferStatusDetailsEntity
							.setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
					fileTransferStatusDetailsEntity.setFileType(Constants.SYSTEM_LOG);
					fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
					fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

					// system logs for again rest call
					File tarFile = new File(destFolder);
					long tarFileSize = (long) tarFile.length();
					long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
					long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
					long size_limit = sizelimitMb * (1000 * 1000);
					long chunk_size = chunkfilemb * (1000 * 1000);

					if (tarFileSize > size_limit) {

						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM() Tar_fileSize is greater than size_limit "
										+ tarFileSize);
						Map<String, Object> chunkDetails = commonUtil.Chunking_System_logs(destFolder, chunk_size);
						int totChunkCount = (Integer) chunkDetails.get("totCount");
						Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get("pathDetails");

						if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

							fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

							FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
									.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
							fileTransferStatusDetailsRepository
									.deleteFileTransferDetailsChunks(fileTransferStatusDetailsEntity.getId());

							for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
								String chunk_name = objMap.getValue();
								ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
								objChunksDetailsEntity.setChunkCount(objMap.getKey());
								objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
								objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
								objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
										objFileTransferStatusDetailsEntity.getId());
								objChunksDetailsEntity.setChunkName(
										chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
								String chunkTarFile = objMap.getValue();

								String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
								objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
								objChunksDetailsEntity = fileTransferStatusDetailsRepository
										.saveChunkFileDetails(objChunksDetailsEntity);

							}

						}

					} else {
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

					}

				}

			}

		}

		catch (Exception e) {
			status = false;
			logger.error(
					"Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathSYSTEM()  : "
							+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}

	public boolean fileTransferFromIFEServertoLocalPathITU(FlightOpenCloseEntity objStausFlightOpenCloseEntity,
			FileTransferStatusDetailsEntity fileTransferStatusDetailsEntity) {

		logger.info(
				"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU()  objLocFileTransferStatusDetailsEntity"
						+ objStausFlightOpenCloseEntity);
		boolean status = false;

		try {

			String toGetCurrentWorkingDir = CommonUtil.toGetCurrentDir();
			String ituLogFilePathSource = env.getProperty("ITU_LOG_FILE_PATH_IFE");
			StringBuilder itulogdestpaths = new StringBuilder();
			String ituLogFilePathDest = itulogdestpaths.append(toGetCurrentWorkingDir)
					.append(env.getProperty("ITU_LOG_FILE_PATH_IFE_DESTINATION")).toString();

			File ituLogFileSource = new File(ituLogFilePathSource+objStausFlightOpenCloseEntity.getItuFolderName());
			Date date = new Date();
			if (ituLogFileSource.exists()) {
				StringBuilder folderDate = new StringBuilder();
				ituLogFilePathDest = folderDate.append(ituLogFilePathDest).append(Constants.ITU_LOG).append("_")
						.append(objStausFlightOpenCloseEntity.getFlightNumber()).append("_")
						.append(String
								.valueOf(DateUtil.getDate(objStausFlightOpenCloseEntity.getDepartureTime()).getTime()))
						.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() ItuLog_Folder_in_local_directory"
								+ ituLogFilePathDest);

				File systemLogFileDestination = new File(ituLogFilePathDest);

				if (!systemLogFileDestination.exists()) {
					systemLogFileDestination.mkdirs();
				}

				StringBuilder tarOutputBuilder = new StringBuilder();
				tarOutputBuilder.append(systemLogFileDestination).append(".tar.gz");
				String destFolder = tarOutputBuilder.toString();

				logger.info(
						"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() ItuLog_Folder_in_local_directory"
								+ destFolder);
				FileUtil.copyFastFileToLocationbetweenDatess(ituLogFilePathSource+objStausFlightOpenCloseEntity.getItuFolderName(), ituLogFilePathDest,
						objStausFlightOpenCloseEntity.getFlightOpenTime(),
						objStausFlightOpenCloseEntity.getFlightCloseTime(), Constants.YYYY_MM_DD_HH_MM_SS);
				boolean metaDataStatus = metadataJsonFileCreationAndTransfer(objStausFlightOpenCloseEntity,
						ituLogFilePathDest);

				boolean statusOfTarFile = CommonUtil.createTarFile(ituLogFilePathDest, destFolder);
				if (statusOfTarFile) {
					MessageDigest md = MessageDigest.getInstance("MD5");
					String checkSum = commonUtil.computeFileChecksum(md, destFolder);
					logger.info(
							"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() Itulog_checkSum"
									+ checkSum);

					fileTransferStatusDetailsEntity.setChecksum(checkSum);
					fileTransferStatusDetailsEntity.setStatus(Constants.READY_TO_TRANSMIT);
					fileTransferStatusDetailsEntity.setOriginalFilename(ituLogFilePathDest
							.substring(ituLogFilePathDest.lastIndexOf('/') + 1, ituLogFilePathDest.length()));
					fileTransferStatusDetailsEntity.setTarFilePath(destFolder);
					fileTransferStatusDetailsEntity.setTarFileDate(new Date());
					fileTransferStatusDetailsEntity
							.setTarFilename(destFolder.substring(destFolder.lastIndexOf('/') + 1, destFolder.length()));
					fileTransferStatusDetailsEntity.setFileType(Constants.ITU_LOG);
					fileTransferStatusDetailsEntity.setModeOfTransfer(Constants.WAY_OF_TRANSPER_REST);
					fileTransferStatusDetailsEntity.setOpenCloseDetilsId(objStausFlightOpenCloseEntity.getId());

					// system logs for again rest call
					File tarFile = new File(destFolder);
					long tarFileSize = (long) tarFile.length();
					long sizelimitMb = Long.valueOf(env.getProperty("size_limit_of_tar_file").trim());
					long chunkfilemb = Long.valueOf(env.getProperty("chunk_size_of_each_chunk").trim());
					long size_limit = sizelimitMb * (1000 * 1000);
					long chunk_size = chunkfilemb * (1000 * 1000);

					if (tarFileSize > size_limit) {

						logger.info(
								"FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU() Tar_fileSize is greater than size_limit "
										+ tarFileSize);
						Map<String, Object> chunkDetails = commonUtil.Chunking_System_logs(destFolder, chunk_size);
						int totChunkCount = (Integer) chunkDetails.get("totCount");
						Map<Integer, String> objMappathDetails = (Map<Integer, String>) chunkDetails.get("pathDetails");

						if (objMappathDetails != null && totChunkCount == objMappathDetails.size()) {

							fileTransferStatusDetailsEntity.setChunkSumCount(totChunkCount);

							FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
									.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);
							fileTransferStatusDetailsRepository
									.deleteFileTransferDetailsChunks(fileTransferStatusDetailsEntity.getId());

							for (Map.Entry<Integer, String> objMap : objMappathDetails.entrySet()) {
								String chunk_name = objMap.getValue();
								ChunksDetailsEntity objChunksDetailsEntity = new ChunksDetailsEntity();
								objChunksDetailsEntity.setChunkCount(objMap.getKey());
								objChunksDetailsEntity.setChunkStatus(Constants.READY_TO_TRANSMIT);
								objChunksDetailsEntity.setChunkTarFilePath(objMap.getValue());
								objChunksDetailsEntity.setFileTransferStausDetailsEntityId(
										objFileTransferStatusDetailsEntity.getId());
								objChunksDetailsEntity.setChunkName(
										chunk_name.substring(chunk_name.lastIndexOf('/') + 1, chunk_name.length()));
								String chunkTarFile = objMap.getValue();

								String chunkCheckSum = commonUtil.computeFileChecksum(md, chunkTarFile);
								objChunksDetailsEntity.setChunk_Checksum(chunkCheckSum);
								objChunksDetailsEntity = fileTransferStatusDetailsRepository
										.saveChunkFileDetails(objChunksDetailsEntity);

							}

						}

					} else {
						fileTransferStatusDetailsEntity.setChunkSumCount(Constants.CHECKSUM_COUNT);
						FileTransferStatusDetailsEntity objFileTransferStatusDetailsEntity = fileTransferStatusDetailsRepository
								.saveFileTransferStatusDetails(fileTransferStatusDetailsEntity);

					}

				}

			}

		}

		catch (Exception e) {
			status = false;
			logger.error("Exception FileTransferStatusDetailsServiceimpl.fileTransferFromIFEServertoLocalPathITU()  : "
					+ ExceptionUtils.getFullStackTrace(e));
		}

		return status;
	}
	
	
	

}