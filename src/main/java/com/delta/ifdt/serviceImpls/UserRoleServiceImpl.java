package com.delta.ifdt.serviceImpls;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.ifdt.entities.UserRoleEntity;
import com.delta.ifdt.repository.UserRoleRepository;
import com.delta.ifdt.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	static final Logger logger = LoggerFactory.getLogger(UserRoleServiceImpl.class);

	@Autowired
	UserRoleRepository userRoleRepository;

	/**
	 * This method will a getUserRoleList
	 * 
	 * @return List<UserRoleEntity>
	 */
	@Override
	public List<UserRoleEntity> getUserRoleList() {
		List<UserRoleEntity> objList = null;
		logger.info("UserRoleServiceImpl.getUserRoleList()");
		try {
			objList = userRoleRepository.getUserRoleList();
		} catch (Exception e) {
			logger.error("Exception in UserRoleServiceImpl.getUserRoleList(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return objList;
	}

}
