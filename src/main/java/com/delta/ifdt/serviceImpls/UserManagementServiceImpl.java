package com.delta.ifdt.serviceImpls;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.delta.ifdt.entities.UserManagementEntity;
import com.delta.ifdt.models.UserManagementModel;
import com.delta.ifdt.repository.UserManagementRepository;
import com.delta.ifdt.service.UserManagementService;
import com.delta.ifdt.util.PasswordCryption;

@Service
public class UserManagementServiceImpl implements UserManagementService {

	private static final Logger logger = LoggerFactory.getLogger(UserManagementServiceImpl.class);

	@Autowired
	UserManagementRepository userManagementRepository;

	/**
	 * This method will a check duplicateUser
	 * 
	 * @param userDetails
	 * @return boolean
	 */
	@Override
	public boolean duplicateUser(UserManagementModel userDetails) {
		boolean status = false;
		logger.info("UserManagementServiceImpl.duplicateUser()");
		try {
			status = userManagementRepository.duplicateUser(userDetails);
		} catch (Exception e) {
			logger.error("Exception UserManagementServiceImpl.duplicateUser(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

	/**
	 * This method will createUser
	 * 
	 * @param userEntity
	 * @return boolean
	 */
	@Override
	public boolean createUser(UserManagementEntity userEntity) {
		boolean status = false;
		logger.info("UserManagementServiceImpl.createUser()");
		try {
			String encryptpassword = PasswordCryption
					.encrypt(PasswordCryption.decryptPasswordUI(userEntity.getPassword()));
			userEntity.setPassword(encryptpassword);
			status = userManagementRepository.createUser(userEntity);
		} catch (Exception e) {
			logger.error("Exception UserManagementServiceImpl.createUser(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

	/**
	 * This method will getUserList
	 * 
	 * @return List<UserManagementModel>
	 */
	@Override
	public List<UserManagementModel> getUserList() {
		List<UserManagementModel> userManagementModel = null;
		logger.info("UserManagementServiceImpl.getUserList()");
		try {
			List<UserManagementModel> userList = userManagementRepository.getUserList();
			if (userList != null && userList.size() > 0) {
				userManagementModel = new ArrayList<>();
				for (UserManagementModel userDetailsModel : userList) {
					String encryptpassword = PasswordCryption.decrypt(userDetailsModel.getPassword());
					userDetailsModel.setPassword(encryptpassword);
					userDetailsModel.setConfirmPassword(encryptpassword);
					userManagementModel.add(userDetailsModel);
				}
			}

		} catch (Exception e) {
			logger.error("Exception UserManagementServiceImpl.getUserList(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return userManagementModel;
	}

	/**
	 * This method will deleteUser
	 * 
	 * @param valueOf
	 * @return boolean
	 */
	@Override
	public boolean deleteUser(Integer valueOf) {
		boolean status = false;
		logger.info("UserManagementServiceImpl.deleteUser()");
		try {
			status = userManagementRepository.deleteUser(valueOf);
		} catch (Exception e) {
			logger.error("Exception UserManagementServiceImpl.deleteUser(): " + ExceptionUtils.getFullStackTrace(e));
		}
		return status;
	}

}
